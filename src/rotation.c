/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include <glib.h>
#include <GL/gl.h>
#include "node.h"
#include "rotation.h"

static void rotation_compute_extents (CmNode  *node,
				      Extents *extents);
static void rotation_render          (CmNode  *node,
				      CmState *state);

G_DEFINE_TYPE (CmRotation, cm_rotation, CM_TYPE_NODE);

static void
cm_rotation_finalize (GObject *object)
{
    CmRotation *rotation = CM_ROTATION (object);
    
    G_OBJECT_CLASS (cm_rotation_parent_class)->finalize (object);

    cm_node_disown_child (CM_NODE (object), &rotation->child);
}

static void
cm_rotation_class_init (CmRotationClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = CM_NODE_CLASS (class);
    
    object_class->finalize = cm_rotation_finalize;

    node_class->render = rotation_render;
    node_class->compute_extents = rotation_compute_extents;
}

static void
cm_rotation_init (CmRotation *rotation)
{
    
}

static void
rotation_compute_extents (CmNode *node,
			  Extents *extents)
{
    extents->x = 0.0;
    extents->y = 0.0;
    extents->z = 0.0;
    extents->width = 1.0;
    extents->height = 1.0;
    extents->depth = 1.0;
}

static void
rotation_render (CmNode *node,
		 CmState *state)
{
    CmRotation *rotation = (CmRotation *)node;

    glPushMatrix();

    glRotatef (rotation->angle,
	       rotation->x,
	       rotation->y,
	       rotation->z);
    
    cm_node_render (rotation->child, state);

    glPopMatrix();
}

CmRotation *
cm_rotation_new (CmNode *child)
{
    CmRotation *rotation;

    g_return_val_if_fail (CM_IS_NODE (child), NULL);
    
    rotation = g_object_new (CM_TYPE_ROTATION, NULL);
    
    rotation->angle = 0.0;
    rotation->x = 0.0;
    rotation->y = 0.0;
    rotation->z = 1.0;

    cm_node_own_child (CM_NODE (rotation), &rotation->child, child);
    
    return rotation;
}

void
cm_rotation_set_rotation (CmRotation *rotation,
			  gdouble     angle,
			  gdouble     x,
			  gdouble     y,
			  gdouble     z)
{
    g_return_if_fail (CM_IS_ROTATION (rotation));
    
    rotation->angle = angle;
    rotation->x = x;
    rotation->y = y;
    rotation->z = z;

    cm_node_queue_repaint (CM_NODE (rotation));
}
