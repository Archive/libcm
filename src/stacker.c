/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include <glib.h>
#include <GL/gl.h>
#include "node.h"
#include "stacker.h"
#include "state.h"

static void stacker_compute_extents (CmNode  *node,
				     Extents *extents);
static void stacker_render          (CmNode  *node,
				     CmState *state);

G_DEFINE_TYPE (CmStacker, cm_stacker, CM_TYPE_NODE);

static void
cm_stacker_finalize (GObject *object)
{
    CmStacker *stacker = CM_STACKER (object);
    GList *list;

    for (list = stacker->children; list != NULL; list = list->next)
    {
	if (list->data)
	    cm_node_disown_child (CM_NODE (object), (CmNode **)&(list->data));
    }
    
    G_OBJECT_CLASS (cm_stacker_parent_class)->finalize (object);
}

static void
cm_stacker_class_init (CmStackerClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = CM_NODE_CLASS (class);
    
    object_class->finalize = cm_stacker_finalize;
    
    node_class->render = stacker_render;
    node_class->compute_extents = stacker_compute_extents;
}

static void
cm_stacker_init (CmStacker *stacker)
{
    
}

static void
stacker_compute_extents (CmNode *node,
			 Extents *extents)
{
    extents->x = 0.0;
    extents->y = 0.0;
    extents->z = 0.0;
    extents->width = 1.0;
    extents->height = 1.0;
    extents->depth = 1.0;
}

static void
render_child (CmNode *child,
	      CmState *state)
{
    WsRegion *covered; 

    cm_node_render (child, state);

    covered = cm_node_get_covered_region (child);

    if (covered)
    {
	cm_state_add_covered_region (state, covered);
	ws_region_destroy (covered);
    }
}

static void
render_children (GList *children, CmState *state)
{
    if (children)
    {
	WsRegion *covered;
	CmNode *child = children->data;

	if (child)
	{
	    cm_state_push_covered_region (state);
	    
	    covered = cm_node_get_covered_region (child);
	    
	    if (covered)
	    {
		cm_state_add_covered_region (state, covered);
		ws_region_destroy (covered);
	    }
	}
	
	render_children (children->next, state);
	
	if (child)
	{
	    cm_state_pop_covered_region (state);
	    
	    cm_node_render (child, state);
	}
    }
}

static void
stacker_render (CmNode *node,
		CmState *state)
{
    CmStacker *stacker = (CmStacker *)node;
    GList *list;
#if 0
    GLboolean depth_mask;
    gdouble depth;
#endif
    
    glPushMatrix();
    
    /* Our children are supposed to be coplanar, but the depth buffer
     * has finite precision, so we get bleeding.
     *
     * The fix is to do two-pass rendering
     *
     *   - First render everything back-to-front with glDepthMask (FALSE).
     *     This will make sure the color buffer is correct
     *
     *   - Then draw everything with glDepthMask (TRUE) and
     *     glColorMask (0, 0, 0, 0). This will make sure the depth buffer
     *     is correct.
     *
     * The big problem with this is that a chain of n stackers will
     * take time O(2^n) to render:
     *
     *        T(n) = 2 * T(n - 1)
     *
     * The solution to this is to have the state maintain the enabled bits
     * of the depth and color buffer. If turned off n times, it needs to
     * turned on n times before the buffer will be used.
     *
     * Then the code will look like this:
     *
     *        cm_state_disable_depth_buffer (state);
     *
     *	      if (cm_state_color_buffer_enabled())
     *			render_children();
     *
     *        cm_state_enable_depth_buffer (state);
     *
     *        cm_state_disable_color_buffer (state)
     *
     *        if (cm_state_depth_buffer_enabled())
     *			render_children();
     *
     *        cm_state_enable_color_buffer (state);
     *
     * The final problem is that there are other kinds of buffers than
     * depth and color. Such as stencil etc. If those are used, what will
     * happen when stuff is rendered twice? Maybe nothing.
     * 
     */
    
    /* So, disable depth buffer writes and render the children
     * to the color buffer ...
     */
    
#if 0
    stacker->children = g_list_reverse (stacker->children);
#endif

    if (cm_state_color_buffer_update_enabled (state))
    {
	cm_state_disable_depth_buffer_update (state);

	render_children (stacker->children, state);
	
	cm_state_enable_depth_buffer_update (state);
    }

    if (cm_state_depth_buffer_update_enabled (state))
    {
	cm_state_disable_color_buffer_update (state);

	render_children (stacker->children, state);

	cm_state_enable_color_buffer_update (state);
    }
    
#if 0
    cm_state_push_covered_region (state);

#if 0
    g_print ("begin\n");
#endif
    
    if (cm_state_color_buffer_update_enabled (state))
    {
	cm_state_disable_depth_buffer_update (state);

	for (list = stacker->children; list; list = list->next)
	{
	    CmNode *child = list->data;

	    if (child)
		render_child (child, state);
	}
	cm_state_enable_depth_buffer_update (state);
    }

    cm_state_pop_covered_region (state);
#endif
    
#if 0
    /* ... then disable the color buffer and render the children
     * again to the depth buffer
     */

    cm_state_push_covered_region (state);
    
    if (cm_state_depth_buffer_update_enabled (state))
    {
	cm_state_disable_color_buffer_update (state);
	for (list = stacker->children; list; list = list->next)
	{
	    CmNode *child = list->data;

	    if (child)
		render_child (child, state);
	}
	cm_state_enable_color_buffer_update (state);
    }

#if 0
    g_print ("end\n");
#endif
    
    cm_state_pop_covered_region (state);
#endif
    
#if 0
    stacker->children = g_list_reverse (stacker->children);
#endif

    glPopMatrix();
}

CmStacker *
cm_stacker_new (void)
{
    CmStacker *stacker = g_object_new (CM_TYPE_STACKER, NULL);

    /* Children are stored in top to bottom order */
    
    return stacker;
}

void
cm_stacker_add_child (CmStacker *stacker,
		      CmNode *child)
{
    stacker->children = g_list_prepend (stacker->children, NULL);

    cm_node_own_child (CM_NODE (stacker),
		       (CmNode **)&(stacker->children->data), child);
}

void
cm_stacker_remove_child (CmStacker *stacker,
			 CmNode *child)
{
    GList *child_link = g_list_find (stacker->children, child);

    if (!child_link)
	return;
    
    cm_node_disown_child (CM_NODE (stacker), (CmNode **)&(child_link->data));
}

/* FIXME: there has to be a simpler way to do this
 * Note: node_disown/own_child() need the children to stay
 * in the same links at all times
 */

static void
restack_internally (CmStacker *stacker,
		    GList     *child_link,
		    GList     *before_this_link)
{
    GList *last;
    int len_before;

    len_before = g_list_length (stacker->children);
    
    if (!child_link)
	return;

    if (child_link == before_this_link)
	return;

    if (child_link->next == before_this_link)
	return;
    
    last = g_list_last (stacker->children);

#if 0
    g_print ("child %p last %p before %p\n", child_link, before_this_link, last);
    g_print ("child next/prev %p %p\n", child_link->next, child_link->prev);
    
#endif
    if (child_link->next)
	child_link->next->prev = child_link->prev;

    if (child_link->prev)
	child_link->prev->next = child_link->next;
    else
	stacker->children = child_link->next;
    
    if (before_this_link)
    {
	child_link->prev = before_this_link->prev;
	child_link->next = before_this_link;
    }
    else
    {
	if (child_link != last)
	{
	    child_link->prev = last;
	    child_link->next = NULL;
	}
    }

    if (child_link->next)
	child_link->next->prev = child_link;

    if (child_link->prev)
	child_link->prev->next = child_link;

    if (!child_link->prev)
	stacker->children = child_link;

    g_assert (child_link->next != child_link);
    g_assert (child_link->prev != child_link);
    g_assert (len_before == g_list_length (stacker->children));
}

void
cm_stacker_restack_child (CmStacker *stacker,
			  CmNode *child,
			  CmNode *above_this)
{
    GList *child_link;
    GList *above_link;

    child_link = g_list_find (stacker->children, child);
    
    g_return_if_fail (child_link != NULL);
    
    above_link = g_list_find (stacker->children, above_this);

    if (child_link->next == above_link)
	return;
    
    restack_internally (stacker, child_link, above_link);

}

void
cm_stacker_lower_child (CmStacker *stacker,
			CmNode *child)
{
    GList *child_link = g_list_find (stacker->children, child);

    g_return_if_fail (child_link != NULL);

    if (!child_link->next)
	return;
    
    restack_internally (stacker, child_link, NULL);
}

void
cm_stacker_raise_child (CmStacker *stacker,
			CmNode *child)
{
    GList *child_link = g_list_find (stacker->children, child);

    g_return_if_fail (child_link != NULL);

    if (child_link == stacker->children)
	return;
    
    restack_internally (stacker, child_link, stacker->children);
}
