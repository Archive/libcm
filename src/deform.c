/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include <math.h>
#include <string.h>

#include "node.h"
#include "deform.h"
#include "state.h"
#include "drawable-node.h"

static void cm_deform_render (CmNode *node, CmState *state);

G_DEFINE_TYPE (CmDeform, cm_deform, CM_TYPE_NODE);

static void
cm_deform_finalize (GObject *object)
{
    CmDeform *deform = CM_DEFORM (object);
    
    cm_node_disown_child (CM_NODE (deform), &deform->child);
    
    G_OBJECT_CLASS (cm_deform_parent_class)->finalize (object);
}

static void
cm_deform_class_init (CmDeformClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = CM_NODE_CLASS (class);
    
    object_class->finalize = cm_deform_finalize;
    node_class->render = cm_deform_render;
}

static void
cm_deform_init (CmDeform *deform)
{
    
}

static void
deform_vertex_func (gdouble *x,
		    gdouble *y,
		    gdouble *z,
		    gdouble  u,
		    gdouble  v,
		    gpointer data)
{
    CmDeform *deform = data;

    if (deform->func)
    {
	int ix = *x;
	int iy = *y;
	int dx, dy;

	deform->func (ix, iy,
		      deform->x, deform->y, deform->width, deform->height,
		      &dx, &dy, deform->data);

	*x = dx;
	*y = dy;

    }
}

static void
cm_deform_render (CmNode *node,
		  CmState *state)
{
    CmDeform *deform = CM_DEFORM (node);

    cm_state_push_vertex_func (state, deform_vertex_func, deform);

    cm_node_render (deform->child, state);

    cm_state_pop_vertex_func (state);
}

CmDeform *
cm_deform_new        (CmNode *child,
		      int x,
		      int y,
		      int width,
		      int height)
{
    CmDeform *deform = g_object_new (CM_TYPE_DEFORM, NULL);

    cm_node_own_child (CM_NODE (deform), &deform->child, child);

    deform->x = x;
    deform->y = y;
    deform->width = width;
    deform->height = height;

    return deform;
}

static void
new_patch_deform (int u_in, int v_in,
		  int x, int y, int w, int h,
		  int *deformed_x, int *deformed_y,
		  void *p)
{
    CmDeform *deform = p;
    double coeffs_u[4], coeffs_v[4];
    double patch_x, patch_y;
    double u, v;
    int i, j;

    u_in -= x;
    v_in -= y;
    
    u = (double) u_in / w;
    v = (double) v_in / h;
    
    coeffs_u[0] = (1 - u) * (1 - u) * (1 - u);
    coeffs_u[1] = 3 * u * (1 - u) * (1 - u);
    coeffs_u[2] = 3 * u * u * (1 - u);
    coeffs_u[3] = u * u * u;
    
    coeffs_v[0] = (1 - v) * (1 - v) * (1 - v);
    coeffs_v[1] = 3 * v * (1 - v) * (1 - v);
    coeffs_v[2] = 3 * v * v * (1 - v);
    coeffs_v[3] = v * v * v;
    
    patch_x = 0;
    patch_y = 0;
    
    for (i = 0; i < 4; i++)
    {
	for (j = 0; j < 4; j++)
	{
	    patch_x += coeffs_u[i] * coeffs_v[j] * deform->patch_points[j][i].x;
	    patch_y += coeffs_u[i] * coeffs_v[j] * deform->patch_points[j][i].y;
	}
    }
    
    *deformed_x = patch_x + 0.5;
    *deformed_y = patch_y + 0.5;
}

static void
rectangle_deform (int u, int v,
		  int x, int y, int width, int height,
		  int *deformed_x, int *deformed_y,
		  void *p)
{
    CmDeform *deform = p;
    
    *deformed_x = deform->rectangle.x + deform->rectangle.width * ((u - x) / (double)width);
    *deformed_y = deform->rectangle.y + deform->rectangle.height * ((v - y) / (double)height);
}

void
cm_deform_set_rectangle (CmDeform *deform,
			 WsRectangle *rect)
{
    if (!rect)
    {
	cm_deform_set_deform (deform, NULL, NULL);
	return;
    }

    deform->rectangle = *rect;
    cm_deform_set_deform (deform, rectangle_deform, deform);
}

void
cm_deform_set_patch (CmDeform	   *deform,
		     CmPoint	    points[][4])
{
    g_return_if_fail (CM_IS_DEFORM (deform));
    
    cm_deform_set_deform (deform, new_patch_deform, deform);
    
    memcpy (deform->patch_points, points, sizeof (deform->patch_points));
    
    cm_node_queue_repaint (CM_NODE (deform));
}

void
cm_deform_set_deform (CmDeform     *deform,
		      CmDeformFunc  func,
		      gpointer      data)
{
    g_return_if_fail (CM_IS_DEFORM (deform));

    deform->func = func;
    deform->data = data;

    cm_node_queue_repaint (CM_NODE (deform));
}

void
cm_deform_set_geometry (CmDeform   *deform,
			int x, int y, int width, int height)
{
    deform->x = x;
    deform->y = y;
    deform->width = width;
    deform->height = height;
    
    cm_node_queue_repaint (CM_NODE (deform));
}

/* Sample deforms */
void
cm_identity_deform (int u, int v,
		    int x, int y, int width, int height,
		    int *deformed_x, int *deformed_y,
		    void *p)
{
    *deformed_x = u;
    *deformed_y = v;
}

void
cm_wavy_deform (int u, int v,
		int x, int y, int width, int height,
		int *deformed_x, int *deformed_y,
		void *p)
{
    *deformed_x = x + u + cos((x * 4 + u + y * 4 + v) / 64.0) * 8;
    *deformed_y = y + v + sin((x * 4 + u + y * 4 + v) / 64.0) * 8;
}
