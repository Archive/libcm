/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "ws.h"
#include "wsint.h"

#include <stdlib.h> /* tthurman -- debugging only */
#include <X11/X.h>
#include <X11/Xutil.h>
#include <X11/extensions/Xcomposite.h>
#include <X11/extensions/Xdamage.h>

G_DEFINE_TYPE (WsScreen, ws_screen, G_TYPE_OBJECT);

static void
ws_screen_finalize (GObject *object)
{
    G_OBJECT_CLASS (ws_screen_parent_class)->finalize (object);
}

static void
ws_screen_class_init (WsScreenClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    object_class->finalize = ws_screen_finalize;
}

static void
ws_screen_init (WsScreen *screen)
{
    
}

static gboolean
visual_info_from_window (WsScreen *screen,
			 WsWindow *window,
			 XVisualInfo *visinfo)
{
    XWindowAttributes attrs;
    WsFormat format;
    Display *xdisplay = WS_RESOURCE_XDISPLAY (window);
    Window xwindow = WS_RESOURCE_XID (window);

    if (!XGetWindowAttributes (xdisplay, xwindow, &attrs))
	return FALSE;

    visinfo->visual = attrs.visual;
    visinfo->visualid = XVisualIDFromVisual (attrs.visual);
    visinfo->screen = ws_screen_get_number (screen);
    visinfo->depth = attrs.depth;
    visinfo->class = attrs.visual->class;

    format = ws_drawable_get_format (WS_DRAWABLE (window));

    ws_format_get_masks (format,
			 &visinfo->red_mask,
			 &visinfo->green_mask,
			 &visinfo->blue_mask);

    return TRUE;
}
    
static GLXContext
create_context (WsScreen *screen,
		XVisualInfo *visinfo)
{
    int i;
    Display *xdisplay;
    Bool go_direct;

    if (screen->context)
	return screen->context;
    
    xdisplay = screen->display->xdisplay;
    
    g_print ("depth: %d\n", visinfo->depth);

    go_direct = (getenv("LIBCM_DIRECT") != NULL);

    if (go_direct)
      g_warning ("Using DIRECT rendering because LIBCM_DIRECT is defined");
    else
      g_warning ("Using INDIRECT rendering because LIBCM_DIRECT is not defined");

    if (getenv ("LIBCM_TFP") != NULL)
      {
        g_warning ("Using TFP rendering because LIBCM_TFP is defined");
        enable_tfp ();
      }
    else
      g_warning ("Using non-TFP rendering because LIBCM_TFP is not defined");

   screen->context = glXCreateContext (xdisplay, visinfo, NULL, go_direct);
    
    /* FIXME: report an error instead of asserting */
    g_assert (visinfo);
    
    /* FIXME: report an error instead of asserting */
    g_assert (screen->context);
    
    return screen->context;
}

static void
make_output_only (WsWindow *window)
{
    WsServerRegion *region = ws_server_region_new (WS_RESOURCE (window)->display);
    
    ws_window_set_input_shape (window, region);
    
    g_object_unref (G_OBJECT (region));
}

static void
create_gl_window (WsScreen *screen)
{
    int attrib[] = { GLX_RGBA,
		     GLX_DOUBLEBUFFER,
		     GLX_RED_SIZE, 0,
		     GLX_GREEN_SIZE, 0,
		     GLX_BLUE_SIZE, 0,
		     GLX_DEPTH_SIZE, 4,
		     None };
    XVisualInfo *visinfo;
    Display *xdisplay = screen->display->xdisplay;
    XSetWindowAttributes attr;
    Window xwindow;
    Window xroot;
    GLXContext context;

    visinfo = glXChooseVisual (
	xdisplay, ws_screen_get_number (screen), attrib);

    context = create_context (screen, visinfo);

    xroot = WS_RESOURCE_XID (ws_screen_get_root_window (screen));
    
    attr.colormap = XCreateColormap (
	xdisplay, xroot,
	visinfo->visual, AllocNone);
    
    xwindow = XCreateWindow (xdisplay, xroot,
			     0, 0,
			     screen->xscreen->width,
			     screen->xscreen->height,
			     0,
			     visinfo->depth, InputOutput, visinfo->visual,
			     CWColormap, &attr);
    
    glXMakeCurrent (xdisplay, xwindow, context);
    
    XSelectInput (xdisplay, xwindow, ExposureMask | PointerMotionMask);
    
    screen->gl_window = _ws_window_ensure (screen->display,
					   xwindow,
					   FALSE);

    make_output_only (screen->gl_window);
}    

#if 0
static void
paint_window (Display *dpy, Window window, unsigned long pixel)
{
    XGCValues gc_values;
    GC gc;

    gc_values.foreground = pixel;
    gc = XCreateGC (dpy, window, GCForeground, &gc_values);

    XFillRectangle (dpy, window, gc, 0, 0, 32000, 32000);

    XFreeGC (dpy, gc);
}
#endif

static void
create_overlay_window (WsScreen *screen)
{
    Display *xdisplay = screen->display->xdisplay;
    Window overlay_window, subwindow, xroot;
    GLXContext context;
    XVisualInfo *visinfo;
    XSetWindowAttributes attr;
    int attrib[] = { GLX_RGBA,
		     GLX_DOUBLEBUFFER,
		     GLX_RED_SIZE, 0,
		     GLX_GREEN_SIZE, 0,
		     GLX_BLUE_SIZE, 0,
		     GLX_DEPTH_SIZE, 4,
		     None };

    xdisplay = screen->display->xdisplay;
    xroot = WS_RESOURCE_XID (ws_screen_get_root_window (screen));
    
    overlay_window = XCompositeGetOverlayWindow (xdisplay, xroot);

    /* Create as foreign window, since we don't want to have it destroyed */
    screen->overlay_window = _ws_window_ensure (screen->display, overlay_window, TRUE);

    visinfo = glXChooseVisual (
	xdisplay, ws_screen_get_number (screen), attrib);

    context = create_context (screen, visinfo);

    attr.colormap = XCreateColormap (
	xdisplay, xroot,
	visinfo->visual, AllocNone);
    
    subwindow = XCreateWindow (xdisplay, overlay_window,
			       0, 0,
			       screen->xscreen->width,
			       screen->xscreen->height,
			       0,
			       visinfo->depth, InputOutput, visinfo->visual,
			       CWColormap, &attr);

    screen->gl_window = _ws_window_ensure (screen->display, subwindow, FALSE);

    ws_window_map (screen->gl_window);
    
    glXMakeCurrent (xdisplay, subwindow, context);

    make_output_only (screen->overlay_window);
    make_output_only (screen->gl_window);
    
    /* FIXME set up the shape etc. */
}

WsWindow *
ws_screen_get_gl_window (WsScreen *screen)
{
#if 0
    if (!screen->gl_window)
	create_gl_window (screen);

    return screen->gl_window;
#endif

    if (!screen->gl_window)
	create_overlay_window (screen);

    g_assert (screen->overlay_window);
    
    return screen->gl_window;
}    

void
ws_screen_release_gl_window (WsScreen *screen)
{
    g_object_unref (screen->gl_window);
    XCompositeReleaseOverlayWindow (
	screen->display->xdisplay, WS_RESOURCE_XID (screen->overlay_window));

    screen->gl_window = NULL;
    screen->overlay_window = NULL;
}

WsScreen *
_ws_screen_new (WsDisplay *display,
		Screen    *xscreen)
{
    WsScreen *screen = g_object_new (WS_TYPE_SCREEN, NULL);
    
    screen->display = display;
    screen->xscreen = xscreen;
    
    return screen;
}

WsWindow *
ws_screen_get_root_window (WsScreen *screen)
{
    Window root;
    
    g_return_val_if_fail (WS_IS_SCREEN (screen), NULL);
    
    root = RootWindowOfScreen (screen->xscreen);
    
    return _ws_window_ensure (screen->display, root, TRUE);
}

int
ws_screen_get_number (WsScreen *screen)
{
    int i;
    
    g_return_val_if_fail (screen != NULL, -1);
    
    for (i = 0; i < screen->display->n_screens; ++i)
	if (screen->display->screens[i] == screen)
	    return i;
    
    return -1;
}

void
ws_screen_graphics_sync (WsScreen *screen)
{
    WsWindow *root = ws_screen_get_root_window (screen);
    XImage *tmp_image;
    
    tmp_image = XGetImage (WS_RESOURCE_XDISPLAY (screen),
			   WS_RESOURCE_XID (root),
			   0, 0, 1, 1, (unsigned)-1, ZPixmap);
    
    XDestroyImage (tmp_image);
}

gint
ws_screen_get_width (WsScreen *screen)
{
    return screen->xscreen->width;
}

gint
ws_screen_get_height (WsScreen *screen)
{
    return screen->xscreen->height;
}

