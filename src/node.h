/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#ifndef _RENDER_H
#define _RENDER_H

#include <glib.h>
#include <glib-object.h>

#include "ws.h"

#include <GL/gl.h>

/* A RenderNode will assume that it has a coordinate system
 * with properties such that
 *
 *	- (0, 0, 0) is in the bottom left corner of the screen
 *	- (0, 1, 0) is in the top left corner of the screen
 *      - (1, 0, 0) has the same distance to (0, 0, 0) as (0, 1, 0)
 *      - (0, 0, 1) has (visually) the same distance to (0, 0, 0)
 *
 *
 * A RenderNode has a 'render' method that draws the thing.
 *
 * Possibly later: A RenderNode has an 'extents' method that returns a
 * bounding box.
 */

/* Example of possibly useful RenderNodes:
 * 
 * 
 * Cube: Has six children that are rendered on the six faces of the cube
 *	Cube can be spun around axes.
 *		cube_rotate_{x,y,z} ()
 *
 * Sphere: Renders its child(s 2D projection) on the surface of a sphere:
 *
 *	implementation:
 *		compute extents of child
 *		split extent into number of small boxes
 *		for each box
 *			push
 *			set scissor/clip to box
 *			set projection so that drawing ends
 *				up on a square on the cube
 *			(* render)
 *			pop()
 *
 * Canvas: Renders a number of stacked 2D children.
 *
 * Distortion: Splits child into a number of rectangles,
 *
 * Window: renders a window.
 *
 * Other useful ones: Translator, Rotator, Box (puts more children aside each other).
 *
 */

/*
 *	Silly animation:
 *		Grid:	  Renders its (n * m) children in an (n x m) grid
 *		Clip:     Takes one child and clips out a given retangle (or box).
 *					(Note: use ClipPlane)
 *
 *              Add cubes to grid, each cube has a Clip of the desktop.
 *
 */ 

/* We'll need the concept of 'extent'. A node must report its extent -
 * otherwise, things like the cube won't know how much of its children
 * to render on its faces.
 *
 *      Extents {
 *
 *		double x,
 *		double y,
 *		double z,
 *		double width,
 *		double height,
 *		double depth
 *	};
 *
 * all the numbers can legally be 0, meaning that nothing is drawn.
 *
 * if only one dimension is 0, the extents is 2D. Does GL deal correctly
 * with clipping a 3D drawing to a 2D plane? We are going to need this
 * for a lot of things.
 *
 */

#define CM_TYPE_NODE            (cm_node_get_type ())
#define CM_NODE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CM_TYPE_NODE, CmNode))
#define CM_NODE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CM_TYPE_NODE, CmNodeClass))
#define CM_IS_NODE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CM_TYPE_NODE))
#define CM_IS_NODE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CM_TYPE_NODE))
#define CM_NODE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CM_TYPE_NODE, CmNodeClass))

typedef struct _CmState CmState;
typedef struct _CmNode CmNode;
typedef struct _CmNodeClass CmNodeClass;
typedef struct Extents Extents;
typedef struct _CmTexInfo CmTexInfo;

struct Extents
{
    double x;
    double y;
    double z;
    double width;
    double height;
    double depth;
};

struct _CmTexInfo
{
    GLuint	texture;

    double	u1;
    double	v1;
    double	u2;	
    double	v2;

    int		x1;
    int		y1;
    int		x2;
    int		y2;
};

struct _CmNode
{
    GObject	 parent_instance;

    GList       *parents;
};

struct _CmNodeClass
{
    GObjectClass parent_class;

    void (* render) (CmNode *node,
		     CmState *state);
    void (* compute_extents) (CmNode      *node,
			      Extents     *extents);
    WsRegion * (* get_covered_region) (CmNode *node);
};

GType cm_node_get_type (void);

void cm_node_get_extents (CmNode    *node,
		       Extents *extents);
void cm_node_render (CmNode *node,
		     CmState *state);
void cm_node_queue_repaint (CmNode *node);
gdouble cm_node_get_time (CmNode *node);
void cm_node_own_child (CmNode *node,
			CmNode **child_location,
			CmNode *child);
void cm_node_disown_child (CmNode *node,
			   CmNode **child_location);
WsRegion *cm_node_get_covered_region (CmNode *node);
gboolean cm_node_is_toplevel (CmNode *node);

#endif /* _RENDER_H */
