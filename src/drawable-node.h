/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "ws.h"
#include "node.h"
#include "grid.h"
#include "pixtexture.h"
#include "deform.h"
#include "shadow.h"
#include "texenv.h"
#include "explosion.h"
#include "stacker.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#define CM_TYPE_DRAWABLE_NODE            (cm_drawable_node_get_type ())
#define CM_DRAWABLE_NODE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CM_TYPE_DRAWABLE_NODE, CmDrawableNode))
#define CM_DRAWABLE_NODE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CM_TYPE_DRAWABLE_NODE, CmDrawableNodeClass))
#define CM_IS_DRAWABLE_NODE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CM_TYPE_DRAWABLE_NODE))
#define CM_IS_DRAWABLE_NODE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CM_TYPE_DRAWABLE_NODE))
#define CM_DRAWABLE_NODE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CM_TYPE_DRAWABLE_NODE, CmDrawableNodeClass))
	    
typedef struct _CmDrawableNode CmDrawableNode;
typedef struct _CmDrawableNodeClass CmDrawableNodeClass;

struct _CmDrawableNode
{
    CmNode			parent_instance;
    
    WsDrawable *		drawable;
    CmTexEnv *			tex_env;
    CmPixTexture *		pix_texture;
    CmDeform *			deform;
    CmGrid *			grid;
    CmShadow *			shadow;
    CmExplosion *		explosion;
    CmStacker *			stacker;
    
    gboolean			viewable;
    
    GTimer *			timer;
    WsRectangle			geometry;
    WsRegion *			shape;		/* in window coordinates */
    
    double			alpha;
    double			explosion_level;

    gboolean			deformed;
};

struct _CmDrawableNodeClass
{
    CmNodeClass parent_class;
};

GType cm_drawable_node_get_type (void);

CmDrawableNode *cm_drawable_node_new          (WsDrawable     *drawable,
					       WsRectangle    *geometry);
void cm_drawable_node_set_shape (CmDrawableNode *node,
				 WsRegion *shape);
void cm_drawable_node_set_geometry (CmDrawableNode *node,
				    WsRectangle    *geometry);
void cm_drawable_node_set_updates (CmDrawableNode *node,
				   gboolean updates);
/* This function must be called whenever the named pixmap becomes stale
 * (and you want to make it non-stale. Ie., after map/unmap and configure
 * notify. 
 *
 * (DrawableNode doesn't do this automatically, since you might want to
 *  do stuff with the stale pixmap, such as fading it out).
 */
void cm_drawable_node_update_pixmap (CmDrawableNode *node);

/* FIXME: This function probably belongs at the CmNode level as
 * cm_node_set_visible()
 */
void            cm_drawable_node_set_viewable (CmDrawableNode *node,
					       gboolean        viewable);
void		cm_drawable_node_set_explosion_level (CmDrawableNode *node,
						      gdouble	       explosion_level);
/* FIMXE: these function should go away - they just delegate to node->deform */
void            cm_drawable_node_set_patch    (CmDrawableNode *node,
					       CmPoint         points[][4]);
void		cm_drawable_node_unset_patch (CmDrawableNode *node);
void            cm_drawable_node_set_alpha    (CmDrawableNode *node,
					       double          alpha);
void		cm_drawable_node_get_clipbox  (CmDrawableNode *node,
					       WsRectangle    *clipbox);
void		cm_drawable_node_set_scale_rect (CmDrawableNode *node,
						 WsRectangle  *rect);
gboolean	cm_drawable_node_get_viewable (CmDrawableNode *node);
