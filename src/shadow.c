/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include <math.h>
#include "shadow.h"
#include "state.h"

G_DEFINE_TYPE (CmShadow, cm_shadow, CM_TYPE_NODE);

static void cm_shadow_render (CmNode *node, CmState *state);

static void
cm_shadow_finalize (GObject *object)
{
    G_OBJECT_CLASS (cm_shadow_parent_class)->finalize (object);
}

static void
cm_shadow_class_init (CmShadowClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = CM_NODE_CLASS (class);
    
    object_class->finalize = cm_shadow_finalize;
    node_class->render = cm_shadow_render;
}

static void
cm_shadow_init (CmShadow *shadow)
{
    
}

#define SHADOW_WIDTH 64

static void
bind_shadow_texture (void)
{
    static unsigned char texture[4 * SHADOW_WIDTH * SHADOW_WIDTH];
    static gboolean initialized;
    static GLuint texture_name;
    int i, j, dx, dy;
    double d, alpha;
    const int xofs = 4, yofs = 4;
    const int fuzz = 3 * SHADOW_WIDTH / 5;
    
    if (!initialized)
    {
	initialized = TRUE;
	glGenTextures (1, &texture_name);
	for (i = 0; i < SHADOW_WIDTH * 2; i++)
	{
	    for (j = 0; j < SHADOW_WIDTH * 2; j++)
	    {
		if (i < (xofs + fuzz))
		    dx = (xofs + fuzz) - i;
		else if (i < (xofs + 2 * SHADOW_WIDTH - fuzz))
		    dx = 0;
		else
		    dx = i - (xofs + 2 * SHADOW_WIDTH - fuzz);
		
		if (j < (yofs + fuzz))
		    dy = (yofs + fuzz) - j;
		else if (j < (yofs + 2 * SHADOW_WIDTH - fuzz))
		    dy = 0;
		else
		    dy = j - (yofs + 2 * SHADOW_WIDTH - fuzz);
		
		d = sqrt (dx * dx + dy * dy);
		alpha = pow (0.9, d * 2) * 90;
		
		texture[i + j * SHADOW_WIDTH * 2] = alpha;
	    }	  
	}
	
	glBindTexture (GL_TEXTURE_RECTANGLE_ARB, texture_name);
	glTexImage2D (GL_TEXTURE_RECTANGLE_ARB, 0, GL_ALPHA,
		      SHADOW_WIDTH * 2, SHADOW_WIDTH * 2, 0,
		      GL_ALPHA, GL_BYTE, texture);
    }
    
    glBindTexture (GL_TEXTURE_RECTANGLE_ARB, texture_name);
}

/* FIXME: wobble the vertices */

static void
emit_vertex (CmState *state,
	     gdouble x,
	     gdouble y)
{
    cm_state_set_vertex_state (state, &x, &y, NULL,
			       0, 0);
    glVertex3f (x, y, 0);
}

static void
emit_quad (CmShadow *shadow,
	   CmState *state,
	   int u0, int v0, int u1, int v1,
	   double x0, double y0,
	   double x1, double y1)
{
    x0 += shadow->x;
    y0 += shadow->y;
    x1 += shadow->x;
    y1 += shadow->y;
    
    glTexCoord2i (u0, v0);
    emit_vertex (state, x0, y0);
    glTexCoord2i (u1, v0);
    emit_vertex (state, x1, y0);
    glTexCoord2i (u1, v1);
    emit_vertex (state, x1, y1);
    glTexCoord2i (u0, v1);
    emit_vertex (state, x0, y1);
}

#define SHADOW_ALPHA 0.9	/* should probably be a parameter */

static void
cm_shadow_render (CmNode *node, CmState *state)
{
    const int tile_size = 32;
    CmShadow *shadow = CM_SHADOW (node);
    int w = shadow->width;
    int h = shadow->height;
    int top, bottom, left, right;
    int u0, u1, v0, v1;

    if (w > 2 * tile_size) {
	left = tile_size;
	right = tile_size;
    } else {
	left = w / 2;
	right = (w + 1) / 2;
    }

    if (h > 2 * tile_size) {
	top = tile_size;
	bottom = tile_size;
    } else {
	top = h / 2;
	bottom = (h + 1) / 2;
    }

    glMatrixMode (GL_MODELVIEW_MATRIX);
    
    glPushAttrib (GL_CURRENT_BIT);
    glPushMatrix();

    glColor4f (0.0, 0.0, 0.0, SHADOW_ALPHA);
    glEnable (GL_TEXTURE_RECTANGLE_ARB);
    bind_shadow_texture();
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable (GL_BLEND);
    
    glBegin (GL_QUADS);
    
    /* Top left corner quads. */
    emit_quad (shadow, state, 0, 0, tile_size, tile_size,
	       -tile_size, -tile_size, 0, 0);
    emit_quad (shadow, state, tile_size, 0, 2 * tile_size, tile_size,
	       0, -tile_size, left, 0);
    emit_quad (shadow, state, 0, tile_size, tile_size, 2 * tile_size,
	       -tile_size, 0, 0, top);
#if 0
    emit_quad (shadow, state, tile_size, tile_size,
	       2 * tile_size, 2 * tile_size,
	       0, 0, left, top);
#endif
    
    /* Top right corner quads. */
    emit_quad (shadow, state, 2 * tile_size, 0, 3 * tile_size, tile_size,
	       w - right, -tile_size, w, 0);
    emit_quad (shadow, state, 3 * tile_size, 0, 4 * tile_size, tile_size,
	       w, -tile_size, w + tile_size, 0);
#if 0
    emit_quad (shadow, state, 2 * tile_size, tile_size,
	       3 * tile_size, 2 * tile_size,
	       w - right, 0, w, top);
#endif
    emit_quad (shadow, state, 3 * tile_size, tile_size,
	       4 * tile_size, 2 * tile_size,
	       w, 0, w + tile_size, top);
    
    for (u0 = tile_size; u0 < w - tile_size; u0 += tile_size)
    {
	v0 = -tile_size;
	v1 = 0;
	u1 = MIN(u0 + tile_size, w - tile_size);
	
	emit_quad (shadow, state, 2 * tile_size, 0, 2 * tile_size, tile_size,
		  u0, v0, u1, v1);
	
	v0 = h;
	v1 = h + tile_size;
	emit_quad (shadow, state, 2 * tile_size, 3 * tile_size, 2 * tile_size, 4 * tile_size,
		  u0, v0, u1, v1);
    }
    
    
    for (v0 = tile_size; v0 < h - tile_size; v0 += tile_size)
    {
	u0 = -tile_size;
	u1 = 0;
	v1 = MIN(v0 + tile_size, h - tile_size);
	
	emit_quad (shadow, state, 0, 2 * tile_size, tile_size, 2 * tile_size,
		  u0, v0, u1, v1);
	
	u0 = w;
	u1 = w + tile_size;
	
	emit_quad (shadow, state, 3 * tile_size, 2 * tile_size, 4 * tile_size, 2 * tile_size,
		  u0, v0, u1, v1);
    }
    
    /* Bottom left corner quads. */
    emit_quad (shadow, state, 0, 2 * tile_size,
	       tile_size, 3 * tile_size,
	       -tile_size, h - bottom, 0, h);
#if 0
    emit_quad (shadow, state, tile_size, 2 * tile_size,
	       2 * tile_size, 3 * tile_size,
	       0, h - bottom, left, h);
#endif
    emit_quad (shadow, state, 0, 3 * tile_size,
	       tile_size, 4 * tile_size,
	       -tile_size, h, 0, h + tile_size);
    emit_quad (shadow, state, tile_size, 3 * tile_size,
	       2 * tile_size, 4 * tile_size,
	       0, h, left, h + tile_size);

    /* Bottom right corner quads. */
#if 0
    emit_quad (shadow, state, 2 * tile_size, 2 * tile_size,
	       3 * tile_size, 3 * tile_size,
	       w - right, h - bottom, w, h);
#endif
    emit_quad (shadow, state, 3 * tile_size, 2 * tile_size,
	       4 * tile_size, 3 * tile_size,
	       w, h - bottom, w + tile_size, h);
    emit_quad (shadow, state, 2 * tile_size, 3 * tile_size,
	       3 * tile_size, 4 * tile_size,
	       w - right, h, w, h + tile_size);
    emit_quad (shadow, state, 3 * tile_size, 3 * tile_size,
	       4 * tile_size, 4 * tile_size,
	       w, h, w + tile_size, h + tile_size);
    
    glEnd ();
    glPopMatrix();
    glPopAttrib();
}

void
cm_shadow_set_geometry (CmShadow *shadow,
			int x, int y,
			int width, int height)
{
    shadow->x = x;
    shadow->y = y;
    shadow->width = width;
    shadow->height = height;

    cm_node_queue_repaint (CM_NODE (shadow));
}

CmShadow *
cm_shadow_new (int x, int y,
	    int width, int height)
{
    CmShadow *shadow = g_object_new (CM_TYPE_SHADOW, NULL);

    shadow->x = x;
    shadow->y = y;
    shadow->width = width;
    shadow->height = height;
    
    return shadow;
}
