/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "wsint.h"

G_DEFINE_TYPE (WsServerRegion, ws_server_region, WS_TYPE_RESOURCE);

static void
ws_server_region_finalize (GObject *object)
{
    WsServerRegion *region = WS_SERVER_REGION (object);
    
    XFixesDestroyRegion (WS_RESOURCE_XDISPLAY (region),
			 WS_RESOURCE_XID (region));
    
    G_OBJECT_CLASS (ws_server_region_parent_class)->finalize (object);
}

static void
ws_server_region_class_init (WsServerRegionClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    object_class->finalize = ws_server_region_finalize;
}

static void
ws_server_region_init (WsServerRegion *region)
{
    
}

WsServerRegion *
ws_server_region_new (WsDisplay *display)
{
    XID xregion = XFixesCreateRegion (display->xdisplay, NULL, 0);
    
    WsServerRegion *region = g_object_new (WS_TYPE_REGION,
					   "display", display,
					   "xid", xregion,
					   "foreign", FALSE,
					   NULL);
    
    return region;
}

void
ws_server_region_union (WsServerRegion *region,
			WsServerRegion *other)
{
    XFixesUnionRegion (WS_RESOURCE_XDISPLAY (region),
		       WS_RESOURCE_XID (region),
		       WS_RESOURCE_XID (region),
		       WS_RESOURCE_XID (other));
}

WsRectangle *
ws_server_region_query_rectangles (WsServerRegion *region,
				   guint          *n_rectangles)
{
    int n_rects = 0;
    XRectangle *xrects;
    
    xrects = XFixesFetchRegion (WS_RESOURCE_XDISPLAY (region),
				WS_RESOURCE_XID (region), &n_rects);
    
    if (n_rectangles)
	*n_rectangles = n_rects;
    
    if (xrects)
    {
	WsRectangle *ws_rects = g_new (WsRectangle, n_rects);
	int i;
	
	for (i = 0; i < n_rects; ++i)
	{
	    ws_rects[i].x = xrects[i].x;
	    ws_rects[i].y = xrects[i].y;
	    ws_rects[i].width = xrects[i].width;
	    ws_rects[i].height = xrects[i].height;
	}
	
	return ws_rects;
    }
    
    return NULL;
}
