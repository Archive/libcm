/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "wsint.h"

G_DEFINE_TYPE (WsDrawable, ws_drawable, WS_TYPE_RESOURCE);

static GObject *ws_drawable_constructor  (GType                  type,
					  guint                  n_construct_properties,
					  GObjectConstructParam *construct_params);

enum
{
    DAMAGE_NOTIFY_EVENT,
    N_SIGNALS
};

static guint signals[N_SIGNALS] = { 0 };

static void
ws_drawable_finalize (GObject *object)
{
    WsDrawable *drawable = WS_DRAWABLE (object);

    if (drawable->damage)
    {
	_ws_display_unregister_damage (WS_RESOURCE (object)->display,
				       drawable->damage);
    }
    
    G_OBJECT_CLASS (ws_drawable_parent_class)->finalize (object);
}

static void
ws_drawable_class_init (WsDrawableClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    
    object_class->constructor = ws_drawable_constructor;
    object_class->finalize = ws_drawable_finalize;

    signals[DAMAGE_NOTIFY_EVENT] =
	g_signal_new ("damage_notify_event",
		      G_OBJECT_CLASS_TYPE (object_class),
		      G_SIGNAL_RUN_LAST,
		      0,
		      NULL, NULL,
		      g_cclosure_marshal_VOID__VOID,
		      G_TYPE_NONE, 0);
}

static void
on_damage (WsDisplay *display,
	   Damage     damage,
	   gpointer   data)
{
    WsDrawable *drawable = data;

    if (drawable->damage)
    {
	ws_display_begin_error_trap (display);
	
	XDamageSubtract (WS_RESOURCE_XDISPLAY (drawable),
			 drawable->damage,
			 None, None);
    
	ws_display_end_error_trap (display);
    }
    
    g_signal_emit (drawable, signals[DAMAGE_NOTIFY_EVENT], 0);
}

static void
ws_drawable_init (WsDrawable *drawable)
{
}

static GObject *
ws_drawable_constructor  (GType                  type,
			  guint                  n_construct_properties,
			  GObjectConstructParam *construct_params)
{
    GObject *object;
    WsDrawable *drawable;
    XID xdamage;
    
    object = G_OBJECT_CLASS (ws_drawable_parent_class)->constructor (
	type, n_construct_properties, construct_params);
    
    drawable = WS_DRAWABLE (object);

    /* We need to trap errors here as the drawable could be
     * an input-only window
     *
     * FIXME: maybe it's better to not use the _with_return variant
     * and instead just treat the drawable->damage as untrusted.
     */
    ws_display_begin_error_trap (WS_RESOURCE (drawable)->display);
    
    xdamage = XDamageCreate (WS_RESOURCE_XDISPLAY (drawable),
			     WS_RESOURCE_XID (drawable),
			     XDamageReportNonEmpty);

    XDamageSubtract (WS_RESOURCE_XDISPLAY (drawable),
		     xdamage,
		     None, None);

    if (!ws_display_end_error_trap_with_return (WS_RESOURCE (drawable)->display))
    {
	drawable->damage = xdamage;
	
	_ws_display_register_damage (WS_RESOURCE (drawable)->display,
				     drawable->damage,
				     on_damage, drawable);
    }

    return object;
}


void
ws_drawable_fill_area (WsDrawable *drawable,
		       int x, int y, int width, int height,
		       const WsColor *color)
{
#if 0
    XrenderColor rcolor;
#endif
    
    /* FIXME implement this - we really need a format for
     * every drawable, including pixmaps.
     */
}

void
ws_drawable_copy_area (WsDrawable *src,
		       int sx, int sy, int width, int height,
		       WsDrawable *dest,
		       int dx, int dy,
		       WsServerRegion *clip)
{
    Display *xdisplay = WS_RESOURCE_XDISPLAY (src);
    
    GC gc = XCreateGC (xdisplay, WS_RESOURCE_XID (src), 0, NULL);
    
#if 0
    if (clip)
	XFixesSetGCClipRegion (xdisplay, gc, 0, 0, clip->xregion);
#endif
    
    XSetForeground (xdisplay, gc, 0x00FFFF00);
    
    XFillRectangle (xdisplay, WS_RESOURCE_XID (dest), gc, 0, 0, width, height);
    
    XCopyArea (xdisplay,
	       WS_RESOURCE_XID (src),
	       WS_RESOURCE_XID (dest), gc,
	       sx, sy, width, height, dx, dy);
    
    XFreeGC (WS_RESOURCE_XDISPLAY (src), gc);
}

static gboolean
xdrawable_query_geometry_internal (Display    *xdisplay,
				   XID	       drawable,
				   Window     *root,
				   int        *x,
				   int        *y,
				   guint      *width,
				   guint      *height,
				   guint      *depth)
{
    int x_internal, y_internal;
    Window root_internal;
    guint width_internal, height_internal, depth_internal;
    guint bw_dummy;
    
    g_return_val_if_fail (drawable != None, FALSE);
    
    if (!XGetGeometry (
	    xdisplay, drawable, &root_internal,
	    &x_internal, &y_internal,
	    &width_internal, &height_internal, &bw_dummy, &depth_internal))
    {
	return FALSE;
    }
    
    if (root)
	*root = root_internal;
    
    if (x)
	*x = x_internal;
    
    if (y)
	*y = y_internal;
    
    if (width)
	*width = width_internal;
    
    if (height)
	*height = height_internal;
    
    if (depth)
	*depth = depth_internal;
    
    return TRUE;
}

WsDisplay *
ws_drawable_get_display (WsDrawable *drawable)
{
    g_return_val_if_fail (drawable != NULL, NULL);
    
    return WS_RESOURCE (drawable)->display;
}

/* Based on code found in Xmu */
static Screen *
drawable_get_xscreen (WsDrawable *drawable)
{
    Display *xdisplay;
    Window root;
    int i;
    
    xdisplay = WS_RESOURCE_XDISPLAY (drawable);
    
    xdrawable_query_geometry_internal (xdisplay, WS_RESOURCE_XID (drawable),
				       &root, NULL, NULL, NULL, NULL,
				       NULL);
    
    for (i = 0; i < ScreenCount (xdisplay); i++)
    {
        if (root == RootWindow (xdisplay, i))
	    return ScreenOfDisplay (xdisplay, i);
    }
    
    return NULL;
}

WsScreen *
ws_drawable_query_screen (WsDrawable *drawable)
{
    g_return_val_if_fail (WS_IS_DRAWABLE (drawable), NULL);
    
    if (!drawable->screen)
    {
	int i;
	Screen *xscreen;
	WsDisplay *display;
	
	display = WS_RESOURCE (drawable)->display;
	xscreen = drawable_get_xscreen (drawable);
	
	for (i = 0; i < WS_RESOURCE (drawable)->display->n_screens; ++i)
	{
	    if (display->screens[i]->xscreen == xscreen)
		drawable->screen = display->screens[i];
	}
    }
    
    return drawable->screen;
}

gboolean
ws_drawable_query_geometry (WsDrawable *drawable,
			    WsRectangle *rectangle)
{
    Window root;		/* dummy */
    guint bw, depth;		/* dummies */
    WsRectangle rect;
    
    if (!XGetGeometry (
	    WS_RESOURCE (drawable)->display->xdisplay,
	    WS_RESOURCE (drawable)->xid,
	    &root,
	    &rect.x, &rect.y,
	    &rect.width, &rect.height,
	    &bw, &depth))
    {
	return FALSE;
    }
    else
    {
	if (rectangle)
	    *rectangle = rect;
	
	return TRUE;
    }
}

int
ws_drawable_query_depth (WsDrawable *drawable)
{
    guint depth;
    
    if (xdrawable_query_geometry_internal (
	    WS_RESOURCE_XDISPLAY (drawable),
	    WS_RESOURCE_XID (drawable),
	    NULL, NULL, NULL, NULL, NULL, &depth))
    {
	return depth;
    }
    
    return -1;
}

struct WsBits
{
    XImage *image;
};

WsFormat
ws_drawable_get_format (WsDrawable *drawable)
{
    if (WS_DRAWABLE_GET_CLASS (drawable)->get_format)
    {
	return WS_DRAWABLE_GET_CLASS (drawable)->get_format (drawable);
    }
    else
    {
	g_warning ("ws_drawable_get_format not implemented for this type");
	return WS_FORMAT_UNKNOWN;
    }
}

WsBits *
ws_drawable_get_bits (WsDrawable *drawable,
		      WsRectangle *rect)
{
    WsRectangle geometry;
    WsBits *bits;
    
    bits = g_new0 (WsBits, 1);
    
    if (!rect)
    {
	ws_drawable_query_geometry (drawable, &geometry);
	
	rect = &geometry;
	
	rect->x = 0;
	rect->y = 0;
    }
    
    bits->image = XGetImage (WS_RESOURCE (drawable)->display->xdisplay,
			     WS_RESOURCE (drawable)->xid,
			     rect->x, rect->y,
			     rect->width, rect->height,
			     (guint)-1, ZPixmap);
    
    if (!bits->image)
    {
	g_free (bits);
	return NULL;
    }
    
    return bits;
}

const guchar *
ws_bits_get_info (WsBits   *bits,
		  guint    *width,
		  guint    *height,
		  guint    *bytes_per_pixel,
		  guint    *bytes_per_line)
{
    if (width)
	*width = bits->image->width;
    
    if (height)
	*height = bits->image->height;
    
    if (bytes_per_pixel)
	*bytes_per_pixel = bits->image->bits_per_pixel / 8;
    
    if (bytes_per_line)
	*bytes_per_line = bits->image->bytes_per_line;
    
    return (guchar *)bits->image->data;
}

void
ws_bits_free (WsBits *bits)
{
    XDestroyImage (bits->image);
    
    g_free (bits);
}
