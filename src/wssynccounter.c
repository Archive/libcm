/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include <X11/Xlib.h>
#include <X11/extensions/sync.h>
#include "wsint.h"

G_DEFINE_TYPE (WsSyncCounter, ws_sync_counter, WS_TYPE_RESOURCE);

XSyncValue
_ws_gint64_to_value (gint64 i)
{
    XSyncValue value;

    value.hi = ((i & (G_GUINT64_CONSTANT (0xffffffff) << 32))) >> 32;
    value.lo = (i & 0xffffffff);

    return value;
}

gint64
_ws_value_to_gint64 (XSyncValue value)
{
    return ((gint64)value.hi << 32) | value.lo;
}

static void
ws_sync_counter_finalize (GObject *object)
{
    if (!WS_RESOURCE (object)->foreign)
    {
	XSyncDestroyCounter (WS_RESOURCE_XDISPLAY (object),
			     WS_RESOURCE_XID (object));
    }
    
    G_OBJECT_CLASS (ws_sync_counter_parent_class)->finalize (object);
}

static void
ws_sync_counter_class_init (WsSyncCounterClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    object_class->finalize = ws_sync_counter_finalize;
}

static void
ws_sync_counter_init (WsSyncCounter *sync_counter)
{

}

static WsSyncCounter *
counter_new (WsDisplay *display,
	     XID	xid,
	     gboolean	foreign)
{
    return g_object_new (WS_TYPE_SYNC_COUNTER,
			 "display", display,
			 "xid", xid,
			 "foreign", foreign,
			 NULL);
}

WsSyncCounter *
ws_sync_counter_new         (WsDisplay     *display,
			     gint64         v)
{
    XID xcounter = XSyncCreateCounter (
	display->xdisplay, _ws_gint64_to_value (v));

    return counter_new (display, xcounter, FALSE);
}

WsSyncCounter *
ws_sync_counter_ensure (WsDisplay *display,
			XSyncCounter xid)
{
    WsSyncCounter *counter =
	g_hash_table_lookup (display->xresources, (gpointer)xid);
    
    if (!counter)
	counter = counter_new (display, xid, TRUE);
    
    return counter;
}

gint64
ws_sync_counter_query_value (WsSyncCounter *counter)
{
    XSyncValue value;

    g_return_val_if_fail (WS_IS_SYNC_COUNTER (counter), 0);
    
    XSyncQueryCounter (WS_RESOURCE_XDISPLAY (counter),
		       WS_RESOURCE_XID (counter),
		       &value);

    return _ws_value_to_gint64 (value);
}

void
ws_sync_counter_set (WsSyncCounter *counter,
		     gint64	    v)
{
    g_return_if_fail (WS_IS_SYNC_COUNTER (counter));
    
    XSyncSetCounter (WS_RESOURCE_XDISPLAY (counter),
		     WS_RESOURCE_XID (counter),
		     _ws_gint64_to_value (v));
}   

void
ws_sync_counter_change (WsSyncCounter *counter,
			gint64	       delta)
{
    g_return_if_fail (WS_IS_SYNC_COUNTER (counter));

    XSyncChangeCounter (WS_RESOURCE_XDISPLAY (counter),
			WS_RESOURCE_XID (counter),
			_ws_gint64_to_value (delta));
}

void
ws_sync_counter_await (WsSyncCounter *counter,
		       gint64	     value)
{
    XSyncWaitCondition wait_cond;
    
    g_return_if_fail (WS_IS_SYNC_COUNTER (counter));

    wait_cond.trigger.counter = WS_RESOURCE_XID (counter);
    wait_cond.trigger.value_type = XSyncAbsolute;
    wait_cond.trigger.wait_value = _ws_gint64_to_value (value);
    wait_cond.trigger.test_type = XSyncPositiveComparison;
    wait_cond.event_threshold = _ws_gint64_to_value (0);

    XSyncAwait (WS_RESOURCE_XDISPLAY (counter),
		&wait_cond,
		1);
}
