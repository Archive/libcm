/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#ifndef _STATE_H
#define _STATE_H

#include <glib.h>
#include <glib-object.h>

#include "node.h"

#define CM_TYPE_STATE            (cm_state_get_type ())
#define CM_STATE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CM_TYPE_STATE, CmState))
#define CM_STATE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CM_TYPE_STATE, CmStateClass))
#define CM_IS_STATE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CM_TYPE_STATE))
#define CM_IS_STATE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CM_TYPE_STATE))
#define CM_STATE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CM_TYPE_STATE, CmStateClass))

typedef struct _CmStateClass CmStateClass;

typedef void (* CmBeginFunc) (gpointer data);
typedef void (* CmEndFunc)   (gpointer data);
typedef void (* CmVertexFunc)      (gdouble	*x,
				    gdouble	*y,
				    gdouble	*z,
				    gdouble	 u,
				    gdouble	 v,
				    gpointer	 data);

struct _CmState
{
    CmNode parent_instance;
    
    GQueue	*vertex_funcs;

    int		disable_depth_buffer_count;
    int		disable_color_buffer_count;

    GQueue	*covered_regions;
};

struct _CmStateClass
{
    CmNodeClass parent_class;
};

GType cm_state_get_type (void);

CmState *cm_state_new (void);

/* Used by state nodes */

void cm_state_push_vertex_func (CmState	     *state,
				CmVertexFunc  vertex_func,
				gpointer      data);
void cm_state_pop_vertex_func  (CmState	     *state);

/* Used by rendering nodes */
void cm_state_set_vertex_state (CmState *state,
				gdouble *x,
				gdouble *y,
				gdouble *z,
				gdouble  u,
				gdouble  v);

void cm_state_disable_depth_buffer_update (CmState *state);
void cm_state_enable_depth_buffer_update (CmState *state);
gboolean cm_state_depth_buffer_update_enabled (CmState *state);

void cm_state_disable_color_buffer_update (CmState *state);
void cm_state_enable_color_buffer_update (CmState *state);
gboolean cm_state_color_buffer_update_enabled (CmState *state);

void cm_state_push_covered_region (CmState *state);
void cm_state_add_covered_region (CmState *state,
				  WsRegion *region);
void cm_state_pop_covered_region (CmState *state);
const WsRegion *cm_state_peek_covered_region (CmState *state);
void cm_state_set_screen_coords (CmState *state);
WsRegion *cm_state_get_visible_region (CmState *state);

#endif /* _STATE_H */
