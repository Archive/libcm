/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "ws.h"
#include "wsint.h"

static gboolean
paint_window (gpointer data)
{
    Display *display;
    XID xid;
    WsPixmap *pixmap = data;
    XGCValues gc_values;
    GC gc;

    display = WS_RESOURCE_XDISPLAY (pixmap);
    xid = WS_RESOURCE_XID (pixmap);

    g_print ("painting on %lx\n", xid);

    gc_values.foreground = 0xff00ff00;
    gc = XCreateGC (display, xid, GCForeground, &gc_values);

    XFillRectangle (display, xid, gc, 0, 0, 32000, 32000);

    XFreeGC (display, gc);

    return TRUE;
}

int
main ()
{
    g_type_init ();
    WsDisplay *display = ws_display_new (NULL);
    WsScreen *screen = ws_display_get_default_screen (display);
    WsWindow *root = ws_screen_get_root_window (screen);
    WsPixmap *pixmap =
	ws_pixmap_new (WS_DRAWABLE (root), 100, 100);
    GMainLoop *loop = g_main_loop_new (NULL, TRUE);

    ws_display_init_damage (display);
    
    ws_display_set_synchronize (display, TRUE);
    
    ws_pixmap_get_texture (pixmap);

    g_idle_add (paint_window, pixmap);

    g_main_loop_run (loop);
    return 0;
}
