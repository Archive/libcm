/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "ws.h"
#include "wsint.h"

int
main ()
{
    WsDisplay *dpy1, *dpy2;
    WsSyncCounter *counter1, *counter2;

    g_type_init ();
    
    dpy1 = ws_display_new (NULL);
    dpy2 = ws_display_new (NULL);

    ws_display_init_sync (dpy1);
    ws_display_init_sync (dpy2);
    
    counter1 = ws_sync_counter_new (dpy1, 10);
    counter2 = ws_sync_counter_ensure (dpy2, WS_RESOURCE_XID (counter1));

    ws_display_sync (dpy1);
    
    ws_sync_counter_await (counter1, 11);

    ws_sync_counter_change (counter2, 1);

    g_print ("value: %lld\n", ws_sync_counter_query_value (counter2));
    
    ws_display_sync (dpy1);

    g_print ("success\n");
    return 0;
}
