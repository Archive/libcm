/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "ws.h"
#include "wsint.h"
#include "grid.h"
#include "deform.h"
#include "ortho.h"
#include "magnifier.h"
#include <GL/glu.h>
#include <math.h>

static gboolean
paint_window (gpointer data)
{
    Display *display;
    XID xid;
    WsPixmap *pixmap = data;
    XGCValues gc_values;
    GC gc;
    
    display = WS_RESOURCE_XDISPLAY (pixmap);
    xid = WS_RESOURCE_XID (pixmap);
    
    g_print ("painting on %lx\n", xid);
    
    gc_values.foreground = 0xff00ff00;
    gc = XCreateGC (display, xid, GCForeground, &gc_values);
    
    XFillRectangle (display, xid, gc, 0, 0, 32000, 32000);
    
    XFreeGC (display, gc);
    
    return TRUE;
}

static void
wobble (int u, int v, int x, int y, int width, int height,
	int *deformed_x, int *deformed_y,
	gpointer data)
{
    double elapsed = *(double *)data;

    *deformed_x = u + cos((x * elapsed + u + y * elapsed + v) / 64.0) * 8;
    *deformed_y = v + sin((x * elapsed + u + y * elapsed + v) / 64.0) * 8;
}

static void
shape (CmGrid *grid)
{
    WsRectangle rect1 = { 200, 200, 500, 300 };
    WsRectangle rect2 = { 300, 300, 300, 100 };
    
    WsRegion *reg1 = ws_region_rectangle (&rect1);
    WsRegion *reg2 = ws_region_rectangle (&rect2);

    ws_region_subtract (reg1, reg2);

    cm_grid_set_region (grid, reg1);
}

int
main ()
{
    g_type_init ();
    
    WsDisplay *display = ws_display_new (NULL);
    WsScreen *screen = ws_display_get_default_screen (display);
    WsWindow *gl_window = ws_screen_get_gl_window (screen);
    WsWindow *root = ws_screen_get_root_window (screen);
    GMainLoop *loop = g_main_loop_new (NULL, TRUE);
    CmGrid *grid;
    CmMagnifier *mag;
    CmOrtho *ortho;
    CmDeform *deform;
    GTimer *timer = g_timer_new ();
    double time;
    WsRectangle source;
    WsRectangle target;
	
    ws_window_map (gl_window);

    grid = cm_grid_new (200, 200, 500, 300, 16, 16);

    shape (grid);

    source.x = 200;
    source.y = 200;
    source.width = 300;
    source.height = 300;

    target.x = 800;
    target.y = 1200 - 150;
    target.width = 150;
    target.height = 150;
    
    deform = cm_deform_new (CM_NODE (grid), 200, 200, 500, 300);

    ortho = cm_ortho_new (CM_NODE (deform), 0, 0, 1600, 1200);
    
    mag = cm_magnifier_new (ortho, &source, &target);
    
    cm_deform_set_deform (deform, wobble, &time);

    glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
#if 0
    glEnable (GL_POLYGON_SMOOTH);
#endif
    glEnable (GL_LINE_SMOOTH);
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint (GL_POLYGON_SMOOTH_HINT, GL_NICEST);

    for (;;)
    {
	time = g_timer_elapsed (timer, NULL);
	glClearColor (1.0, 1.0, 1.0, 1.0);
	
	glClear (GL_COLOR_BUFFER_BIT);

	glColor4f (0.0, 0.0, 0.0, 1.0);

	cm_node_render (CM_NODE (mag), NULL);

	ws_window_gl_swap_buffers (gl_window);
	ws_window_gl_finish (gl_window);
    }    
    g_main_loop_run (loop);
    return 0;
}
