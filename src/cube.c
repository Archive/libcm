/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include <glib.h>
#include <GL/gl.h>
#include "node.h"
#include "cube.h"

G_DEFINE_TYPE (CmCube, cm_cube, CM_TYPE_NODE);

static void cube_render (CmNode *node,
			 CmState *state);
static void cube_compute_extents (CmNode  *node,
				  Extents *extents);

static void
cm_cube_finalize (GObject *object)
{
    CmCube *cube = CM_CUBE (object);
    int i;
    
    G_OBJECT_CLASS (cm_cube_parent_class)->finalize (object);

    for (i = 0; i < 6; ++i)
	cm_node_disown_child (CM_NODE (cube), &cube->faces[i]);
}

static void
cm_cube_class_init (CmCubeClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = CM_NODE_CLASS (class);
    
    object_class->finalize = cm_cube_finalize;

    node_class->render = cube_render;
    node_class->compute_extents = cube_compute_extents;
}

static void
cm_cube_init (CmCube *cube)
{
    
}

static void
cube_compute_extents (CmNode  *node,
		      Extents *extents)
{
    extents->x = 0.0;
    extents->y = 0.0;
    extents->z = 0.0;
    extents->width = 1.0;
    extents->height = 1.0;
    extents->depth = 1.0;
}

static void
cube_render (CmNode  *node,
	     CmState *state)
{
    CmCube *cube = (CmCube *)node;
    
#if 0
    double wt = 1.0;
    double ht = 1.0;
    
    glBegin (GL_QUADS);
    
    glColor3f (1.0, 0.0, 0.0);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (0.0f, 0.0f, 0.0f);
    glTexCoord2f (  wt, 0.0f); glVertex3f (1.0f, 0.0f, 0.0f);
    glTexCoord2f (  wt,   ht); glVertex3f (1.0f, 1.0f, 0.0f);
    glTexCoord2f (0.0f,   ht); glVertex3f (0.0f, 1.0f, 0.0f);
    
    glColor3f (0.0, 1.0, 0.0);
    glTexCoord2f (  wt, 0.0f); glVertex3f (0.0f, 0.0f, -1.0f);
    glTexCoord2f (  wt,   ht); glVertex3f (0.0f, 1.0f, -1.0f);
    glTexCoord2f (0.0f,   ht); glVertex3f (1.0f, 1.0f, -1.0f);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (1.0f, 0.0f, -1.0f);
    
    glColor3f (0.0, 0.0, 1.0);
    glTexCoord2f (0.0f,   ht); glVertex3f (0.0f, 1.0f, -1.0f);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (0.0f, 1.0f,  0.0f);
    glTexCoord2f (  wt, 0.0f); glVertex3f (1.0f, 1.0f,  0.0f);
    glTexCoord2f (  wt,   ht); glVertex3f (1.0f, 1.0f, -1.0f);
    
    glColor3f (1.0, 1.0, 0.0);
    glTexCoord2f (  wt,   ht); glVertex3f (0.0f, 0.0f, -1.0f);
    glTexCoord2f (0.0f,   ht); glVertex3f (1.0f, 0.0f, -1.0f);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (1.0f, 0.0f,  0.0f);
    glTexCoord2f (  wt, 0.0f); glVertex3f (0.0f, 0.0f,  0.0f);
    
    glColor3f (0.0, 1.0, 1.0);
    glTexCoord2f (  wt, 0.0f); glVertex3f (1.0f, 0.0f, -1.0f);
    glTexCoord2f (  wt,   ht); glVertex3f (1.0f, 1.0f, -1.0f);
    glTexCoord2f (0.0f,   ht); glVertex3f (1.0f, 1.0f,  0.0f);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (1.0f, 0.0f,  0.0f);
    
    glColor3f (1.0, 0.0, 1.0);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (0.0f, 0.0f, -1.0f);
    glTexCoord2f (  wt, 0.0f); glVertex3f (0.0f, 0.0f,  0.0f);
    glTexCoord2f (  wt,   ht); glVertex3f (0.0f, 1.0f,  0.0f);
    glTexCoord2f (0.0f,   ht); glVertex3f (0.0f, 1.0f, -1.0f);
    
    glEnd ();
#endif
    
    glColor4f (1.0, 0.3, 0.7, 0.6);

    glPushMatrix ();

    if (cube->faces[0])
	cm_node_render (cube->faces[0], state);
    
    glTranslatef (0.0, 1.0, 0.0);
    glRotatef (90, 1.0, 0.0, 0.0);
    glTranslatef (0.0, 1.0, 0.0);

    if (cube->faces[1])
	cm_node_render (cube->faces[1], state);

    glTranslatef (0.0, 1.0, 0.0);
    glRotatef (90, 1.0, 0.0, 0.0);
    glTranslatef (0.0, 1.0, 0.0);
    
    if (cube->faces[2])
	cm_node_render (cube->faces[2], state);

    glTranslatef (0.0, 1.0, 0.0);
    glRotatef (90, 1.0, 0.0, 0.0);
    glTranslatef (0.0, 1.0, 0.0);
    
    if (cube->faces[3])
	cm_node_render (cube->faces[3], state);
    
    /* back to normal */
    glTranslatef (0.0, 1.0, 0.0);
    glRotatef (90, 1.0, 0.0, 0.0);
    glTranslatef (0.0, 1.0, 0.0);
    
    glTranslatef (1.0, 0.0, 0.0);
    glRotatef (-90, 0.0, 1.0, 0.0);
    glTranslatef (1.0, 0.0, 0.0);
    
    if (cube->faces[4])
	cm_node_render (cube->faces[4], state);

    glTranslatef (0.0, 0.0, 2.0);
    
    if (cube->faces[5])
	cm_node_render (cube->faces[5], state);

    glPopMatrix ();
}

CmCube *
cm_cube_new (void)
{
    CmCube *node = g_object_new (CM_TYPE_CUBE, NULL);
    
    return node;
}

void
cm_cube_set_face (CmCube *cube,
		  int face_no,
		  CmNode *child)
{
    g_return_if_fail (face_no < 6);

    cm_node_own_child (CM_NODE (cube), &(cube->faces[face_no]), child);

    g_assert (cube->faces[face_no] == child);
}
