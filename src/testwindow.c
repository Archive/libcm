/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include <gtk/gtk.h>

static int size = 100;
static gboolean flag;

static gboolean
flip (gpointer data)
{
    GtkWidget *window = data;

    flag = !flag;
    
    gtk_widget_queue_draw (window);
    
    return TRUE;
}

static gboolean
on_expose (GtkWidget *widget,
	   GdkEventExpose *expose)
{
    GdkColor pink   = { 0x0000, 0xffff, 0xa000, 0xeeee } ;
    GdkColor yellow = { 0x0000, 0xffff, 0xffff, 0x2222 } ;
    GdkGC *gc1 = gdk_gc_new (widget->window);
    GdkGC *gc2 = gdk_gc_new (widget->window);
    int i, j;
    int width, height;

    gdk_window_get_size (widget->window, &width, &height);
    gdk_gc_set_rgb_fg_color (gc1, &pink);
    gdk_gc_set_rgb_fg_color (gc2, &yellow);

    for (i = 0; i < height; i += size)
    {
	for (j = 0; j < width; j += size)
	{
	    GdkGC *gc;
	    
	    if (i/size % 2 == j/size % 2)
		gc = flag? gc1 : gc2;
	    else
		gc = flag? gc2 : gc1;
	    
	    gdk_draw_rectangle (widget->window, gc, TRUE, j, i, size, size);
	}
    }

    g_object_unref (gc1);
    g_object_unref (gc2);
    
    return TRUE;
}

int
main (int argc, char **argv)
{
    GtkWidget *window;
    
    gtk_init (&argc, &argv);

    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

    gtk_window_set_default_size (window, 600, 400);

    gtk_window_set_title (window, "Compositing Test Window");
    
    gtk_widget_realize (window);
    
    g_signal_connect (window, "expose_event",
		      G_CALLBACK (on_expose), NULL);

    g_signal_connect (window, "delete_event",
		      G_CALLBACK (gtk_main_quit), NULL);
    
    gtk_widget_show_all (window);

    g_timeout_add (500, flip, window);
    
    gtk_main ();
    
    return 0;
}
