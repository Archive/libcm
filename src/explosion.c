/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include <math.h>
#include "explosion.h"
#include "state.h"

G_DEFINE_TYPE (CmExplosion, cm_explosion, CM_TYPE_NODE);

typedef struct
{
    double x;
    double y;
} Point;

typedef struct
{
    GList *points;
} Polygon;

static void cm_explosion_render (CmNode *node, CmState *state);
static Point *point_new (double x, double y);
static void point_free (Point *p);
static double point_dist (Point *p1, Point *p2);
static Polygon *polygon_new (void);
static void polygon_free (Polygon *polygon);
static void polygon_add_point (Polygon *polygon,
			       double x,
			       double y);
static Polygon *polygon_copy (Polygon *polygon);
static void polygon_split (Polygon *polygon,
			   Polygon **poly1,
			   Polygon **poly2);
static Point *polygon_compute_center (Polygon *polygon);

static void
cm_explosion_finalize (GObject *object)
{
    G_OBJECT_CLASS (cm_explosion_parent_class)->finalize (object);
}

static void
cm_explosion_class_init (CmExplosionClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = CM_NODE_CLASS (class);
    
    object_class->finalize = cm_explosion_finalize;
    node_class->render = cm_explosion_render;
}

static void
cm_explosion_init (CmExplosion *explosion)
{
    
}

static GList *
explode (GList *polygons, Polygon *polygon, int depth)
{
    Polygon *p1, *p2;
    
    if (depth == 0)
	return g_list_prepend (polygons, polygon_copy (polygon));
    
    polygon_split (polygon, &p1, &p2);
    
#if 0
    g_print ("split :\n");
    print_poly (points);
    g_print ("into \n");
    print_poly (p1);
    g_print ("and\n");
    print_poly (p2);
#endif
    
    polygons = explode (polygons, p1, depth - 1);
    polygons = explode (polygons, p2, depth - 1);

    polygon_free (p1);
    polygon_free (p2);

    return polygons;
}

static GList *
generate_polygons (GList *list, WsRectangle *rect)
{
    Polygon *polygon = polygon_new ();
    GList *result;
    
    polygon_add_point (polygon, rect->x, rect->y);
    polygon_add_point (polygon, rect->x + rect->width, rect->y);
    polygon_add_point (polygon, rect->x + rect->width, rect->y + rect->height);
    polygon_add_point (polygon, rect->x, rect->y + rect->height);
    
    result = explode (list, polygon, 7);
    
    polygon_free (polygon);
    
    return result;
}

static void
render_polygon (CmExplosion *explosion,
		CmState     *state,
		Polygon     *polygon,
		WsRectangle *clipbox)
{
    GList *list;
    Point *center;
    Point *rcenter;
    Point *vector;
    double len, time;
    gdouble level;
    
    rcenter = point_new (clipbox->x + clipbox->width / 2.0, clipbox->y + clipbox->height / 2.0);
    
    center = polygon_compute_center (polygon);
    
    len = point_dist (center, rcenter);
    
    vector = point_new ((center->x - rcenter->x) / (len),
			(center->y - rcenter->y) / (len));
    
    
    time = cm_node_get_time (CM_NODE (explosion));
    
    level = explosion->level;
    
    glPushMatrix ();
    
    glTranslatef (center->x, center->y, 0.0);

    glTranslatef (30 * pow (level, 2) * len * vector->x + vector->x,
		  30 * pow (level, 2) * len * vector->y + vector->y,
		  0.0);
    
#if 0
    glRotatef (10 * sin (len * vector->x) * cos (len * vector->y) *
	       level * 360, 0.0, 0.0, 1.0);
#endif
    
#if 0
    g_print ("len: %f\n", len);
#endif
    
#if 0
    g_print ("vector, len: (%f %f) %f\n", vector->x, vector->y, len);
    g_print ("modified vector: %f %f\n", 0.5 * len * vector->x, 0.5 * len * vector->y);
#endif
    
    glTranslatef (-center->x, -center->y, 0.0);
    
    glBegin (GL_POLYGON);
    
    for (list = polygon->points; list; list=list->next)
    {
	Point *point = list->data;
	
	gdouble x = point->x;
	gdouble y = point->y;
	gdouble u = (x - clipbox->x) / clipbox->width;
	gdouble v = (y - clipbox->y) / clipbox->height;
	
	cm_state_set_vertex_state (state, &x, &y, NULL, u, v);
	
	glVertex2f (x, y);
    }

    glEnd ();

    glPushAttrib (GL_CURRENT_BIT);
    
    glDisable (GL_TEXTURE_RECTANGLE_ARB);
    
    glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
    glColor4f (0.0, 0.0, 0.0, 1.0);

    glBegin (GL_POLYGON);
    
    for (list = polygon->points; list; list=list->next)
    {
	Point *point = list->data;
	
	gdouble x = point->x;
	gdouble y = point->y;
	gdouble u = (x - clipbox->x) / clipbox->width;
	gdouble v = (y - clipbox->y) / clipbox->height;
	
	cm_state_set_vertex_state (state, &x, &y, NULL, u, v);
	
	glVertex2f (x, y);
    }

    glEnd ();

    glEnable (GL_TEXTURE_RECTANGLE_ARB);
    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);

    glPopAttrib();
    
    point_free (center);
    point_free (rcenter);
    point_free (vector);
    
    glPopMatrix();
}

static void
free_polygons (CmExplosion *explosion)
{
    GList *list;

    for (list = explosion->polygons; list; list = list->next)
    {
	Polygon *polygon = list->data;
	
	polygon_free (polygon);
    }

    g_list_free (explosion->polygons);
    explosion->polygons = NULL;
}

static void
cm_explosion_render (CmNode *node,
		     CmState *state)
{
    CmExplosion *explosion = CM_EXPLOSION (node);
    WsRectangle *rects;
    int n_rectangles;
    int i;
    GList *list;
    
    /* generate list of polygons */
    if (!explosion->region)
	return;

    if (!explosion->polygons)
    {
	ws_region_get_rectangles (explosion->region, &rects, &n_rectangles);
	for (i = 0; i < n_rectangles; ++i)
	{
	    WsRectangle *rect = &(rects[i]);
	    
	    explosion->polygons = generate_polygons (explosion->polygons, rect);
	}
	if (rects)
	    g_free (rects);
    }
    
    WsRectangle clipbox;
    
    ws_region_get_clipbox (explosion->region, &clipbox);
    
    /* render list of polygons */
    for (list = explosion->polygons; list != NULL; list = list->next)
    {
	Polygon *polygon = list->data;
	
	render_polygon (explosion, state, polygon, &clipbox);
    }
}

CmExplosion *
cm_explosion_new (int x, int y, int w, int h)
{
    CmExplosion *explosion = g_object_new (CM_TYPE_EXPLOSION, NULL);
    
    cm_explosion_set_geometry (explosion, x, y, w, h);
    
    return explosion;
}

void
cm_explosion_set_geometry (CmExplosion *explosion,
			   int x, int y,
			   int w, int h)
{
    WsRectangle rect;
    WsRegion *region;
    
    rect.x = x;
    rect.y = y;
    rect.width = w;
    rect.height = h;
    
    region = ws_region_rectangle (&rect);
    
    cm_explosion_set_region (explosion, region);
    
    ws_region_destroy (region);
}

void
cm_explosion_set_region (CmExplosion *explosion,
			 WsRegion *region)
{
    if (explosion->region && ws_region_equal (explosion->region, region))
	return;
    
    if (explosion->region)
	ws_region_destroy (explosion->region);
    
    explosion->region = ws_region_copy (region);
    free_polygons (explosion);
}


/* Geometry stuff */

typedef struct
{
    Point *p1;
    Point *p2;
} LineSegment;

static void
print_point (Point *p)
{
    if (p)
	g_print ("point: %f %f\n", p->x, p->y);
    else
	g_print ("no such point\n");
}

static void
print_poly (GList *points)
{
    GList *list;
    
    g_print ("Polygon: ");
    
    for (list = points; list != NULL; list = list->next)
    {
	Point *p = list->data;
	
	g_print ("(%.2f, %.2f)%s", p->x, p->y, list->next? ", " : "");
    }
    
    g_print ("\n");
}


static gboolean
is_zero (double d)
{
    return fabs (d) <= DBL_EPSILON;
}

static Point *
point_new (double x, double y)
{
    Point *p = g_new0 (Point, 1);
    
    p->x = x;
    p->y = y;
    
    return p;
}

static double
point_dist (Point *p1, Point *p2)
{
    return sqrt (pow (fabs (p1->x - p2->x), 2.0) + pow (fabs (p1->y - p2->y), 2));
    
}

static Polygon *
polygon_new (void)
{
    return g_new0 (Polygon, 1);
}

static void
point_free (Point *point)
{
    g_free (point);
}

static LineSegment *
line_segment_new (double x0, double y0, double x1, double y1)
{
    LineSegment *ls = g_new0 (LineSegment, 1);
    
    ls->p1 = point_new (x0, y0);
    ls->p2 = point_new (x1, y1);
    
    return ls;
}

static void
line_segment_free (LineSegment *segment)
{
    point_free (segment->p1);
    point_free (segment->p2);
    g_free (segment);
}

static double
compute_area (Polygon *polygon)
{
    double a;
    GList *list;
    
    a = 0.0;
    
    for (list = polygon->points; list; list = list->next)
    {
	Point *point_0 = list->data;
	Point *point_1 = list->next? list->next->data : polygon->points->data;
	
	a += (point_0->x * point_1->y - point_1->x * point_0->y);
    }
    
    return 0.5 * a;
}

static Point *
compute_intersection (LineSegment *l1,
		      LineSegment *l2)
{
    double ua;
    double ub;
    double den;
    
    double x1, x2, x3, x4;
    double y1, y2, y3, y4;
    
    x1 = l1->p1->x;
    x2 = l1->p2->x;
    x3 = l2->p1->x;
    x4 = l2->p2->x;
    
    y1 = l1->p1->y;
    y2 = l1->p2->y;
    y3 = l2->p1->y;
    y4 = l2->p2->y;
    
    den = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);
    
    if (is_zero (den))
    {
	/* The segments are parallel */
	return NULL;
    }
    
    ua = (x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3);
    ub = (x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3);
    
    ua /= den;
    ub /= den;
    
    if (ua >= 0.0 && ua <= 1.0 &&
	ub >= 0.0 && ub <= 1.0)
    {
	return point_new (x1 + ua * (x2 - x1), y1 + ua * (y2 - y1));
    }
    
    return NULL;
}

static void
compute_extremeties (Polygon *polygon,
		     double *min_x, double *max_x,
		     double *min_y, double *max_y)
{
    GList *list;
    double t;
    
    g_return_if_fail (polygon != NULL);
    
    if (!min_x)
	min_x = &t;
    if (!max_x)
	max_x = &t;
    if (!min_y)
	min_y = &t;
    if (!max_y)
	max_y = &t;
    
    *min_x = G_MAXDOUBLE;
    *max_x = G_MINDOUBLE;
    *min_y = G_MAXDOUBLE;
    *max_y = G_MINDOUBLE;
    
    for (list = polygon->points; list != NULL; list = list->next)
    {
	Point *p = list->data;
	
	if (p->x < *min_x)
	    *min_x = p->x;
	
	if (p->x > *max_x)
	    *max_x = p->x;
	
	if (p->y < *min_y)
	    *min_y = p->y;
	
	if (p->y > *max_y)
	    *max_y = p->y;
    }
}

static Point *
polygon_compute_center (Polygon *polygon)
{
    GList *list;
    Point *center;
    double min_x;
    double max_x;
    double min_y;
    double max_y;
    
    compute_extremeties (polygon, &min_x, &max_x, &min_y, &max_y);
    
#if 0
    center = point_new (0.5 * (max_x + min_x), 0.5 * (max_y + min_y));
#if 0
    
    g_print ("%f %f %f %f\n", min_x, max_x, min_y, max_y);
    
    g_print ("%f - %f = %f    %f\n", max_y, min_y, max_y - min_y, 0.5 * (max_y - min_y));
    
    g_print ("the center of \n");
    print_poly (points);
#endif
#if 0
    g_print ("is\n");
    print_point (center);
#endif
    
#if 0
    print_point (center);
#endif
    
    center_points = g_list_prepend (center_points, center);
    return center;
#endif
    
    center = point_new (0.0, 0.0);
    
    for (list = polygon->points; list; list = list->next)
    {
	Point *point_0 = list->data;
	Point *point_1 = list->next? list->next->data : polygon->points->data;
	
	center->x += (point_0->x + point_1->x) * (point_0->x * point_1->y - point_1->x * point_0->y);
	center->y += (point_0->y + point_1->y) * (point_0->x * point_1->y - point_1->x * point_0->y);
    }
    
    center->x /= 6 * compute_area (polygon);
    center->y /= 6 * compute_area (polygon);
    
#if 0
    g_print ("the center of \n");
    print_poly (points);
    g_print ("is\n");
    print_point (center);
#endif
    
    return center;
}

static LineSegment *
pick_points (Point *center,
	     double min_x, double max_x,
	     double min_y, double max_y)
{
    double xc = center->x;
    double yc = center->y;
    double x0;
    double y0;
    double x1;
    double y1;
    
#if 0
    g_assert (center->x <= max_x);
    g_assert (center->y <= max_y);
#endif
    
    if (g_random_int_range (0, 2))
    {
#if 0
	g_print ("horizontal\n");
#endif
	
	/* horizontal */
	y0 = g_random_double_range (min_y, max_y);
	x0 = min_x - g_random_double ();
	x1 = max_x + g_random_double ();
	y1 = y0 + ((x1 - x0) / (xc - x0)) * (yc - y0);
    }
    else
    {
#if 0
	g_print ("vertical\n");
#endif
	
	/* vertical */
	x0 = g_random_double_range (min_x, max_x);
	y0 = min_y - g_random_double ();
	y1 = max_y + g_random_double ();
	x1 = x0 + ((y1 - y0) / (yc - y0)) * (xc - x0);
    }
    
    return line_segment_new (x0, y0, x1, y1);
}

static LineSegment *
choose_split_segment (Polygon *polygon)
{
    double min_x, max_x, min_y, max_y;
    Point *center;
    LineSegment *segment;
    
    center = polygon_compute_center (polygon);
    compute_extremeties (polygon, &min_x, &max_x, &min_y, &max_y);
    segment = pick_points (center, min_x, max_x, min_y, max_y);
    point_free (center);
    return segment;
}

static void
polygon_split (Polygon *polygon,
	       Polygon **poly1,
	       Polygon **poly2)
{
    GList *list;
    LineSegment *segment;
    Polygon **current;
    
    g_return_if_fail (polygon != NULL);
    g_return_if_fail (poly1 != NULL);
    g_return_if_fail (poly2 != NULL);
    
    segment = choose_split_segment (polygon);
    
    *poly1 = polygon_new ();
    *poly2 = polygon_new ();
    
    current = poly1;
    
    for (list = polygon->points; list; list = list->next)
    {
	Point *p1 = list->data;
	Point *p2 = list->next? list->next->data : polygon->points->data;
	LineSegment *temp = line_segment_new (p1->x, p1->y, p2->x, p2->y);
	Point *intersection = compute_intersection (segment, temp);
	
	polygon_add_point (*current, p1->x, p1->y);
	
	if (intersection)
	{
	    polygon_add_point (*current, intersection->x, intersection->y);
	    
	    if (current == poly1)
		current = poly2;
	    else
		current = poly1;
	    
	    polygon_add_point (*current, intersection->x, intersection->y);

	    point_free (intersection);
	}

	line_segment_free (temp);
    }

    line_segment_free (segment);
    
    g_assert (current == poly1);
    
    if (!*poly2 || !*poly1)
    {
#if 0
	g_print ("polygon:\n");
	print_poly (polygon);
#endif
	
#if 0
	g_print ("segment: \n");
	print_point (segment->p1);
	print_point (segment->p2);
#endif
#if 0
	exit (1);
#endif
    }
    
#if 0
    g_assert (*poly1 != NULL);
    g_assert (*poly2 != NULL);
#endif
}

static void
polygon_add_point (Polygon *polygon,
		   double x,
		   double y)
{
    Point *p;
    
    p = point_new (x, y);
    polygon->points = g_list_prepend (polygon->points, p);
}

static Polygon *
polygon_copy (Polygon *polygon)
{
    GList *list;
    Polygon *copy;

    copy = polygon_new ();

    for (list = polygon->points; list; list = list->next)
    {
	Point *p = list->data;
	polygon_add_point (copy, p->x, p->y);
    }

    return copy;
}

static void
polygon_free (Polygon *polygon)
{
    GList *list;
    
    for (list = polygon->points; list != NULL; list = list->next)
	point_free (list->data);
    
    g_list_free (polygon->points);
    g_free (polygon);
}

void
cm_explosion_set_level    (CmExplosion *explosion,
			   double	     level)
{
    explosion->level = level;

    cm_node_queue_repaint (CM_NODE (explosion));
}
