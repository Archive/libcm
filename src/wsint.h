/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include <X11/Xresource.h>
#include <X11/X.h>
#include <X11/Xutil.h>
#include <X11/extensions/Xcomposite.h>
#include <X11/extensions/Xdamage.h>
#include <X11/extensions/sync.h>

#define GLX_GLXEXT_PROTOTYPES
#include <GL/glx.h>
#include <GL/glxext.h>

#include "ws.h"

#ifndef _WSINT_H_
#define _WSINT_H_

typedef struct
{
    gboolean	available;
    int		event_base;
    int		error_base;
} Extension;

/*
 * Display object
 */
typedef struct Trap Trap;

typedef int (* ErrorHandlerFunc) (Display *, XErrorEvent *);
typedef void (* DamageCallback) (WsDisplay *	display,
				 XID		damage,
				 gpointer	data);
				 
struct WsDisplay
{
    GObject	parent_instance;
    
    Display *	xdisplay;
    
    int		n_screens;
    WsScreen **	screens;
    
    GHashTable *xresources;
    
    Extension	damage;
    Extension	composite;
    Extension   test;
    Extension	fixes;
    Extension	glx;
    Extension	sync;

    Trap	*traps;

    ErrorHandlerFunc	old_handler;

    GHashTable  *damage_table;
};

struct WsDisplayClass
{
    GObjectClass parent_class;
};

void _ws_display_add_resource    (WsDisplay      *display,
				  XID             xid,
				  WsResource     *resource);
void _ws_display_remove_resource    (WsDisplay      *display,
				     XID	     xid);
void _ws_display_register_damage (WsDisplay      *display,
				  XID             damage,
				  DamageCallback  cb,
				  gpointer        data);
void _ws_display_unregister_damage (WsDisplay	 *display,
				    XID		  damage);
WsScreen *_ws_display_lookup_screen (WsDisplay    *display,
				     Screen       *xscreen);

/*
 * Screen object
 */
struct WsScreen
{
    GObject	 parent_instance;
    WsDisplay   *display;
    GLXContext	 context;
    Screen	*xscreen;
    WsWindow    *overlay_window;
    WsWindow	*gl_window;
};

struct WsScreenClass
{
    GObjectClass parent_class;
};

WsScreen *_ws_screen_new (WsDisplay *display,
			  Screen    *xscreen);

/*
 * Resource object - things that have an XID
 */
#define WS_RESOURCE_XID(res) (WS_RESOURCE (res)->xid)
#define WS_RESOURCE_XDISPLAY(res) (WS_RESOURCE (res)->display->xdisplay)

struct WsResource
{
    GObject	parent_instance;

    WsDisplay * display;
    XID		xid;

    gboolean	foreign;		/* if TRUE, the resource should not be destroyed by us */
};

struct WsResourceClass
{
    GObjectClass parent_class;
};

/*
 * Region object
 */

struct WsServerRegion
{
    WsResource		parent_instance;
};

struct WsServerRegionClass
{
    WsResourceClass	parent_class;
};

/*
 * Drawable object
 */
#define WS_DRAWABLE_XSCREEN(drw) ((ws_drawable_query_screen (			\
				       WS_DRAWABLE (drw))->xscreen))
#define WS_DRAWABLE_XSCREEN_NO(drw) (XScreenNumberOfScreen (WS_DRAWABLE_XSCREEN (drw)))

struct WsDrawable
{
    WsResource		parent_instance;
    WsScreen *		screen;
    Damage		damage;
};

struct WsDrawableClass
{
    WsResourceClass	parent_class;

    WsFormat (* get_format) (WsDrawable *);
};

Screen *_xdrawable_to_xscreen (Display *xdisplay, XID xid);

/*
 * Pixmap object
 */
struct WsPixmap
{
    WsDrawable	parent_instance;

    /* Texture information */
    GLuint	texture;
    GLXPixmap	glx_pixmap;

    WsFormat	format;
  gboolean      do_updates;
};

struct WsPixmapClass
{
    WsDrawableClass parent_class;
};

WsPixmap *_ws_pixmap_ensure (WsDisplay     *ws,
			     Pixmap         xwindow,
			     WsFormat	    format,
			     gboolean	    foreign);


/*
 * Window object
 */
struct WsWindow
{
    WsDrawable		parent_instance;

    gboolean		has_format;
    WsFormat		format;
};
    
struct WsWindowClass
{
    WsDrawableClass	parent_class;    
};

void      _ws_window_process_event (WsWindow   *window,
				    XEvent     *xevent);
WsWindow *_ws_window_ensure        (WsDisplay  *ws,
				    Window      xwindow,
				    gboolean    foreign);
gchar *	  _ws_display_get_atom_name (WsDisplay *display,
				     Atom	atom);
Atom	  _ws_display_intern_atom (WsDisplay *display,
				   const char *string);


/* Sync */
XSyncValue _ws_gint64_to_value (gint64 i);
gint64     _ws_value_to_gint64 (XSyncValue value);

/*
 * SyncCounter object
 */
struct WsSyncCounter
{
    WsResource parent_instance;
};

struct WsSyncCounterClass
{
    WsResourceClass parent_class;
};

WsSyncCounter *_ws_sync_counter_ensure (WsDisplay *display,
					XID xid,
					gboolean foreign);
/*
 * SyncAlarm
 */
struct WsSyncAlarm
{
    WsResource parent_instance;

    WsSyncCounter *counter;
};

struct WsSyncAlarmClass
{
    WsResourceClass parent_class;
};

void _ws_sync_alarm_process_event (WsSyncAlarm *alarm,
				   XEvent      *xevent);

#endif
