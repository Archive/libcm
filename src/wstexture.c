/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "ws.h"

G_DEFINE_TYPE (WsTexture, ws_texture, G_TYPE_OBJECT);

static void
ws_texture_finalize (GObject *object)
{
    G_OBJECT_CLASS (ws_texture_parent_class)->finalize (object);
}

static void
ws_texture_class_init (WsTextureClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    object_class->finalize = ws_texture_finalize;
}

static void
ws_texture_init (WsTexture *texture)
{

}
