/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "ws.h"

WsDisplay *display;
WsSyncCounter *counter;
WsSyncAlarm *alarm;

static void
on_notify (WsAlarmNotifyEvent *event)
{
    g_print ("received notification\n");
}

static gboolean
step_counter (gpointer datea)
{
    g_print ("stepping\n");
    ws_sync_counter_change (counter, 1);

    g_print ("new value: %lld\n", ws_sync_counter_query_value (counter));
    
    return TRUE;
}

int
main ()
{
    GMainLoop *loop;
    
    g_type_init ();

    loop = g_main_loop_new (NULL, FALSE);
    
    display = ws_display_new (NULL);

    ws_display_init_sync (display);
    
    ws_display_set_synchronize (display, TRUE);
    
    counter = ws_sync_counter_new (display, 100);

    ws_display_sync (display);
    
    alarm = ws_sync_alarm_new (display, counter);

    ws_sync_alarm_set (alarm, 102);

    g_signal_connect (alarm, "alarm_notify_event", G_CALLBACK (on_notify), NULL);

    g_timeout_add (500, step_counter, NULL);
    
    g_main_loop_run (loop);
}
