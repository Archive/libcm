/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include <X11/Xlib.h>
#include <X11/extensions/sync.h>
#include "wsint.h"

G_DEFINE_TYPE (WsSyncAlarm, ws_sync_alarm, WS_TYPE_RESOURCE);

enum {
    ALARM_NOTIFY_EVENT,
    N_SIGNALS
};

static guint signals[N_SIGNALS] = { 0 };

static void
ws_sync_alarm_finalize (GObject *object)
{
    if (!WS_RESOURCE (object)->foreign)
    {
	XSyncDestroyAlarm (WS_RESOURCE_XDISPLAY (object),
			   WS_RESOURCE_XID (object));
    }
    
    G_OBJECT_CLASS (ws_sync_alarm_parent_class)->finalize (object);
}

static void
ws_sync_alarm_class_init (WsSyncAlarmClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    
    object_class->finalize = ws_sync_alarm_finalize;
    
    signals[ALARM_NOTIFY_EVENT] =
	g_signal_new ("alarm_notify_event",
		      G_OBJECT_CLASS_TYPE (object_class),
		      G_SIGNAL_RUN_LAST,
		      0,
		      NULL, NULL,
		      g_cclosure_marshal_VOID__POINTER,
		      G_TYPE_NONE, 1,
		      G_TYPE_POINTER);
}

static void
ws_sync_alarm_init (WsSyncAlarm *sync_alarm)
{
}

WsSyncAlarm *
ws_sync_alarm_new (WsDisplay     *display,
		   WsSyncCounter *counter)
{
    XSyncAlarmAttributes attrs;
    XID xalarm;
    WsSyncAlarm *alarm;
    
    attrs.trigger.counter = WS_RESOURCE_XID (counter);
    attrs.trigger.wait_value = _ws_gint64_to_value (0);
    attrs.trigger.value_type = XSyncAbsolute;
    attrs.trigger.test_type = XSyncPositiveComparison;
    attrs.delta = _ws_gint64_to_value (0);
    attrs.events = False;

    g_assert (XSyncValueIsZero (attrs.delta));
    
    xalarm = XSyncCreateAlarm (display->xdisplay,
			       XSyncCACounter |
			       XSyncCAValue |
			       XSyncCAValueType |
			       XSyncCATestType |
			       XSyncCADelta |
			       XSyncCAEvents,
			       &attrs);
    
    alarm = g_object_new (WS_TYPE_SYNC_ALARM,
			  "display", display,
			  "xid", xalarm,
			  "foreign", FALSE,
			  NULL);

    alarm->counter = counter;
    
    return alarm;
}

void
ws_sync_alarm_set (WsSyncAlarm   *alarm,
		   gint64         value)
{
    XSyncAlarmAttributes attrs;

    attrs.trigger.wait_value = _ws_gint64_to_value (value);
    attrs.events = True;
    
    XSyncChangeAlarm (WS_RESOURCE_XDISPLAY (alarm),
		      WS_RESOURCE_XID (alarm),
		      XSyncCAValue | XSyncCAEvents,
		      &attrs);
}

void
_ws_sync_alarm_process_event (WsSyncAlarm *alarm,
			      XEvent      *xevent)
{
    WsDisplay *display = WS_RESOURCE (alarm)->display;

    g_object_ref (alarm);

    if (xevent->type == display->sync.event_base + XSyncAlarmNotify)
    {
	XSyncAlarmNotifyEvent	*xnotify = (XSyncAlarmNotifyEvent *)xevent;
	WsAlarmNotifyEvent	 event;

	event.alarm = alarm;
	event.counter = alarm->counter;
	event.counter_value = _ws_value_to_gint64 (xnotify->counter_value);
	event.alarm_value = _ws_value_to_gint64 (xnotify->alarm_value);

	g_signal_emit (alarm, signals[ALARM_NOTIFY_EVENT], 0, &event);
    }
    
    g_object_unref (alarm);
}

