/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "drawable-node.h"
#include <string.h>
#include "wsint.h"
#include "state.h"
#include <math.h>
#include "stacker.h"

#include <X11/Xlib.h>
#include <X11/extensions/Xfixes.h>

#if G_BYTE_ORDER == G_LITTLE_ENDIAN
#   define PIXEL_TYPE GL_UNSIGNED_INT_8_8_8_8_REV
#else
#   define PIXEL_TYPE GL_UNSIGNED_INT_8_8_8_8
#endif

G_DEFINE_TYPE (CmDrawableNode, cm_drawable_node, CM_TYPE_NODE);

static void drawable_node_render (CmNode *node,
				  CmState *state);
static void drawable_node_compute_extents (CmNode  *node,
					   Extents *extents);
static WsRegion *drawable_node_get_covered_region (CmNode *node);

static void
cm_drawable_node_finalize (GObject *object)
{
    CmDrawableNode *node = CM_DRAWABLE_NODE (object);
    WsDisplay *display = WS_RESOURCE (node->drawable)->display;
    
    ws_display_begin_error_trap (display);

    if (node->pix_texture)
	g_object_unref (node->pix_texture);
    if (node->grid)
	g_object_unref (node->grid);
    if (node->explosion)
	g_object_unref (node->explosion);
    if (node->pix_texture)
	g_object_unref (node->pix_texture);
    if (node->tex_env)
	g_object_unref (node->tex_env);
    if (node->stacker)
	g_object_unref (node->stacker);
    if (node->shadow)
	g_object_unref (node->shadow);

    cm_node_disown_child (CM_NODE (node), (CmNode **)&node->deform);
    
    G_OBJECT_CLASS (cm_drawable_node_parent_class)->finalize (object);
}

static void
cm_drawable_node_class_init (CmDrawableNodeClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = CM_NODE_CLASS (class);
    
    object_class->finalize = cm_drawable_node_finalize;
    
    node_class->render = drawable_node_render;
    node_class->compute_extents = drawable_node_compute_extents;
    node_class->get_covered_region = drawable_node_get_covered_region;
}

static void
cm_drawable_node_init (CmDrawableNode *drawable_node)
{
    CmDeform *deform;

    drawable_node->grid =
	cm_grid_new (0, 0, 20, 20, 32, 32);
    drawable_node->explosion =
	cm_explosion_new (20, 20, 20, 20);
    drawable_node->pix_texture =
	cm_pix_texture_new (NULL,
			    CM_NODE (drawable_node->grid));
    drawable_node->tex_env =
	cm_tex_env_new (CM_NODE (drawable_node->pix_texture));
    drawable_node->stacker = cm_stacker_new ();
    drawable_node->shadow = cm_shadow_new (20, 20, 32, 32);
    cm_stacker_add_child (drawable_node->stacker, CM_NODE (drawable_node->shadow));
    cm_stacker_add_child (drawable_node->stacker, CM_NODE (drawable_node->tex_env));
    deform =
	cm_deform_new (CM_NODE (drawable_node->stacker), 0, 0, 32, 32);
    cm_node_own_child (CM_NODE (drawable_node), &drawable_node->deform, deform);
    g_object_unref (deform);
    drawable_node->explosion_level = 0.0;
}

static void
queue_paint (CmDrawableNode *node)
{
    cm_node_queue_repaint (CM_NODE (node));
}

static void
print_matrix (const char *header, GLdouble m[16])
{
    int i, j;
    
    g_print ("%s:\n", header);
    
    for (i = 0; i < 16; ++i)
    {
	g_print ("%f ", m[i]);
	
	if (++j == 4)
	{
	    g_print ("\n");
	    j = 0;
	}
    }
}

static void
print_rectangle (const char *header, WsRectangle *rect)
{
    g_print ("%s\n", header);
    
    g_print ("%d %d %d %d\n", rect->x, rect->y, rect->width, rect->height);
}

static void
print_coord (int x, int y, int z)
{
    GLdouble model[16];
    GLdouble proj[16];
    GLint    view[4];
    double   wx, wy, wz;
    
    glGetDoublev (GL_MODELVIEW_MATRIX, model);
    glGetDoublev (GL_PROJECTION_MATRIX, proj);
    glGetIntegerv (GL_VIEWPORT, view);
    
    gluProject (x, y, z,
		model, proj, view,
		&wx, &wy, &wz);
    
#if 0
    print_matrix ("model", model);
    print_matrix ("proj",  proj);
    g_print ("viewport: %d %d %d %d\n", view[0], view[1], view[2], view[3]);
    
    g_print ("projected: %d %d %d  =>   %f %f %f\n", x, y, z, wx, wy, wz);
#endif
}

static void
drawable_node_vertex (gdouble *x,
		      gdouble *y,
		      gdouble *z,
		      gdouble  u,
		      gdouble  v,
		      gpointer data)
    
{
    CmDrawableNode *dnode = data;
    WsRectangle clipbox;

    cm_drawable_node_get_clipbox (dnode, &clipbox);
    
    glTexCoord2f (u * clipbox.width,
		  v * clipbox.height);
}

static WsRegion *
get_shape (CmDrawableNode *dnode)
{
    WsRectangle clipbox;
    WsRegion *reg;

    cm_drawable_node_get_clipbox (dnode, &clipbox);

    if (dnode->shape)
    {
	reg = ws_region_copy (dnode->shape);
	ws_region_offset (reg, clipbox.x, clipbox.y);
    }
    else
    {
	reg = ws_region_rectangle (&clipbox);
    }

    return reg;
}

static void
print_region (const char *header, WsRegion *region)
{
    int i;
    int n_rects;
    WsRectangle *rects = NULL;

    ws_region_get_rectangles (region, &rects, &n_rects);

    g_print ("%s: \n", header);
    for (i = 0; i < n_rects; ++i)
    {
	WsRectangle *r = &(rects[i]);

	g_print ("   %d %d %d %d\n", r->x, r->y, r->width, r->height);
    }
    
    if (rects)
	g_free (rects);
    
}

static WsRegion *
drawable_node_get_covered_region (CmNode *node)
{
    CmDrawableNode *dnode = CM_DRAWABLE_NODE (node);
    WsRegion *result;

    ws_display_begin_error_trap (WS_RESOURCE (dnode->drawable)->display);
    
    if (ws_window_query_input_only (WS_WINDOW (dnode->drawable)))
    {
	result = NULL;
	goto out;
    }
    else if (!dnode->viewable)
    {
	result = NULL;
	goto out;
    }
    else if (dnode->alpha != 1.0)
    {
	result = NULL;
	goto out;
    }
    else if (cm_pix_texture_has_alpha (dnode->pix_texture))
    {
	result = NULL;
	goto out;
    }
    else if (dnode->explosion_level > DBL_EPSILON)
    {
	result = NULL;
	goto out;
    }
    else if (dnode->deformed)
    {
	result = NULL;
	goto out;
    }
    else
    {
	result = get_shape (dnode);
	goto out;
    }

out:
    ws_display_end_error_trap (WS_RESOURCE (dnode->drawable)->display);
    
    return result;
}

static void
set_geometries (CmDrawableNode *dnode)
{
    WsRegion *reg = NULL;
    WsRectangle clipbox;

    reg = get_shape (dnode);

    ws_region_get_clipbox (reg, &clipbox);
    
    cm_grid_set_region (dnode->grid, reg);
    cm_explosion_set_region (dnode->explosion, reg);
    
    cm_deform_set_geometry (dnode->deform,
			    clipbox.x,
			    clipbox.y,
			    clipbox.width,
			    clipbox.height);
    cm_shadow_set_geometry (dnode->shadow,
			    clipbox.x,
			    clipbox.y,
			    clipbox.width,
			    clipbox.height);
    
    ws_region_destroy (reg);	
}

static void
drawable_node_render (CmNode *node,
		      CmState *state)
{
    WsScreen *screen;
    CmDrawableNode *dnode = CM_DRAWABLE_NODE (node);

#if 0
    g_print ("are we going to render %lx (%p)?\n",
	     WS_RESOURCE_XID (dnode->drawable), dnode);
#endif
    
    if (!dnode->viewable)
    {
#if 0
	g_print ("not rendering %lx since it is not viewable\n",
		 WS_RESOURCE_XID (dnode->drawable));
#endif
	return;
    }
    
    if (!dnode->pix_texture)
    {
#if 0
	g_print ("not rendering %lx since it doesn't have a pix_texture\n",
		 WS_RESOURCE_XID (dnode->drawable));
#endif
	return;
    }
    
    if (!dnode->pix_texture->pixmap)
    {
#if 0
	g_print ("not rendering %lx since it doesn't have a pixmap\n",
		 WS_RESOURCE_XID (dnode->drawable));
#endif
	return;
    }
    
#if 0
    g_print ("rendering %lx\n", WS_RESOURCE_XID (dnode->drawable));
#endif

    WsPixmap *pixmap = dnode->pix_texture->pixmap;
    WsDisplay *display = WS_RESOURCE (pixmap)->display;
    
    ws_display_begin_error_trap (display);

    if (ws_window_query_input_only (
	    WS_WINDOW (dnode->drawable)))
    {
	ws_display_end_error_trap (display);
	return;
    }

    screen = ws_drawable_query_screen (
	WS_DRAWABLE (dnode->pix_texture->pixmap));

    if (!screen)
    {
	/* The pixmap was bad or something */
	ws_display_end_error_trap (display);
	return;
    }
    
    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
    
    glPushMatrix();

    cm_state_set_screen_coords (state);
#if 0
    gluOrtho2D (0,
		ws_screen_get_width (screen),
		ws_screen_get_height (screen), 0);
#endif
    
    /* Empirically determined constant that gets rid of some fuzzyness
     */
#if 0
    glTranslatef (-0.125, -0.125, 0);
#endif
    
#if 0
    g_print ("setting geometry: %d %d %d %d\n",
	     dnode->real_x, dnode->real_y,
	     dnode->real_width, dnode->real_height);
#endif
    
    set_geometries (dnode);
    
#if 0
    glColor4f (1.0, 1.0, 1.0, dnode->alpha);
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glEnable (GL_BLEND);
    glBlendFunc (dnode->alpha < 0.99 ? GL_SRC_ALPHA :
		 GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    
#endif
#if 0
    cm_node_render (CM_NODE (dnode->pix_texture), state);
#endif
    
#if 0
    glTranslatef (20 * g_random_double(), 20 * g_random_double(), 0.0);
#endif
    
    cm_node_render (CM_NODE (dnode->deform), state);
    
    glPopMatrix();
    
    glBindTexture (GL_TEXTURE_RECTANGLE_ARB, 0);
    ws_display_end_error_trap (display);
    
}

static void
drawable_node_compute_extents (CmNode  *node,
			       Extents *extents)
{
    extents->x = 0.0;
    extents->y = 0.0;
    extents->z = 0.0;
    extents->width = 1.0;
    extents->height = 1.0;
    extents->depth = 1.0;
}

static void
on_damage (WsPixmap *pixmap, gpointer data)
{
    CmDrawableNode *node = data;

    queue_paint (node);
}

static void
refresh_pixmap (CmDrawableNode *node)
{
    WsWindow *window;
    WsDisplay *display = WS_RESOURCE (node->drawable)->display;
    WsPixmap *pixmap;

    if (node->pix_texture->pixmap && !node->pix_texture->pixmap->do_updates)
    {
#if 0
	g_print ("not refreshing\n");
#endif
	return;
    }

    window = WS_WINDOW (node->drawable);
    
    ws_display_begin_error_trap (display);
    
    pixmap = NULL;
    
    if (ws_window_query_mapped (window)		&&
	!ws_window_query_input_only (window))
    {
	pixmap = ws_window_name_pixmap (window);
#if 0
	g_print ("name pixmap on %lx  %p\n", WS_RESOURCE_XID (window), pixmap);
#endif
    }
    
    if (ws_display_end_error_trap_with_return (display))
    {
	if (pixmap)
	{
	    ws_display_begin_error_trap (display);
	
	    g_object_unref (pixmap);

	    ws_display_end_error_trap (display);
	    
	    pixmap = NULL;
	}
    }

    ws_display_begin_error_trap (display);
    
    cm_pix_texture_set_pixmap (node->pix_texture, pixmap);
    
    if (pixmap)
    {
	g_signal_connect (pixmap, "damage_notify_event", G_CALLBACK (on_damage), node);

	g_object_unref (pixmap);
    }
	
    ws_display_end_error_trap (display);
}

CmDrawableNode *
cm_drawable_node_new (WsDrawable *drawable,
		      WsRectangle *geometry)
{
    CmDrawableNode *node;
    WsDisplay *display;
    WsRectangle dummy;
    
    g_return_val_if_fail (drawable != NULL, NULL);

    if (!geometry)
    {
	/* FIXME: possibly just assertion fail if this happens? */
	dummy.x = 0;
	dummy.y = 0;
	dummy.width = 100;
	dummy.height = 100;

	geometry = &dummy;
    }
    
    display = WS_RESOURCE (drawable)->display;
    
    node = g_object_new (CM_TYPE_DRAWABLE_NODE, NULL);
    
    node->drawable = drawable;
    
    node->viewable = TRUE;
    
    node->timer = g_timer_new ();

    node->geometry = *geometry;
    node->shape = NULL;
    
    ws_display_init_composite (ws_drawable_get_display (drawable));
    ws_display_init_damage (ws_drawable_get_display (drawable));
    
    node->alpha = 1.0;
    
    if (WS_IS_WINDOW (drawable))
    {
	refresh_pixmap (node);
    }

    return node;
}

void
cm_drawable_node_set_viewable (CmDrawableNode *node,
			       gboolean	  viewable)
{
    node->viewable = viewable;
    
    cm_node_queue_repaint ((CmNode *)node);
}

gboolean
cm_drawable_node_get_viewable (CmDrawableNode *node)
{
    return node->viewable;
}

void
cm_drawable_node_set_geometry (CmDrawableNode *node,
			       WsRectangle    *geometry)
{
    g_return_if_fail (geometry != NULL);

    node->geometry = *geometry;
    
    queue_paint (node);
}

void
cm_drawable_node_set_patch (CmDrawableNode *node,
			    CmPoint points[][4])
{
    g_return_if_fail (CM_IS_DRAWABLE_NODE (node));

    node->deformed = TRUE;
    
    cm_deform_set_patch (node->deform, points);
}

void
cm_drawable_node_unset_patch (CmDrawableNode *node)
{
    cm_deform_set_deform (node->deform, NULL, NULL);

    node->deformed = FALSE;
    
    queue_paint (node);
}

void
cm_drawable_node_set_updates (CmDrawableNode *node,
			      gboolean        updates)
{
    updates = !!updates;

    if (node->pix_texture->pixmap)
	ws_pixmap_set_updates (node->pix_texture->pixmap, updates);
}

void
cm_drawable_node_set_alpha (CmDrawableNode *node,
			    double alpha)
{
    node->alpha = alpha;

    if (node->alpha < 0.99)
    {
	WsColor alpha = { node->alpha * 0xffff, 0xffff, 0xffff, 0xffff };
	
	cm_tex_env_set_modulate (node->tex_env, &alpha);
    }
    else
    {
	cm_tex_env_set_replace (node->tex_env);
    }
    
    queue_paint (node);
}

void
cm_drawable_node_update_pixmap (CmDrawableNode *node)
{
#if 0
    g_print ("refreshing pixmap because app told us to\n");
#endif
    refresh_pixmap (node);
}

void
cm_drawable_node_set_shape (CmDrawableNode *node,
			    WsRegion *shape)
{
    WsRegion *new_shape = shape? ws_region_copy (shape) : NULL;

    if (node->shape)
	ws_region_destroy (node->shape);

    node->shape = new_shape;

    queue_paint (node);
}

void
cm_drawable_node_get_clipbox  (CmDrawableNode *node,
			       WsRectangle    *clipbox)
{
    if (!clipbox)
	return;

    *clipbox = node->geometry;
}

void
cm_drawable_node_set_scale_rect (CmDrawableNode *node,
				 WsRectangle  *rect)
{
    cm_deform_set_rectangle (node->deform, rect);

    node->deformed = TRUE;
    
    queue_paint (node);
}

void
cm_drawable_node_set_explosion_level (CmDrawableNode *node,
				      gdouble	       explosion_level)
{
    if (fabs (explosion_level) < DBL_EPSILON)
    {
	cm_pix_texture_set_child (node->pix_texture, node->grid);

	if (cm_node_is_toplevel (CM_NODE (node->shadow)))
	{
	    cm_stacker_add_child (node->stacker, CM_NODE (node->shadow));
	    cm_stacker_lower_child (node->stacker, CM_NODE (node->shadow));
	}
    }
    else
    {
	cm_pix_texture_set_child (node->pix_texture, node->explosion);

	if (!cm_node_is_toplevel (CM_NODE (node->shadow)))
	    cm_stacker_remove_child (CM_NODE (node->shadow)->parents->data, CM_NODE (node->shadow));
    }

    cm_explosion_set_level (node->explosion, explosion_level);
    node->explosion_level = explosion_level;
}
