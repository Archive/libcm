/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include <math.h>
#include <GL/glu.h>

#include "magnifier.h"
#include "state.h"

G_DEFINE_TYPE (CmMagnifier, cm_magnifier, CM_TYPE_NODE);

static void cm_magnifier_render (CmNode *node,
				 CmState *state);

static void
cm_magnifier_finalize (GObject *object)
{
    CmMagnifier *mag = CM_MAGNIFIER (object);
    
    cm_node_disown_child (CM_NODE (mag), &mag->child);
    
    G_OBJECT_CLASS (cm_magnifier_parent_class)->finalize (object);
}

static void
print_matrix (const char *header, GLdouble m[16])
{
    int i, j;

    g_print ("%s:\n", header);

    for (i = 0; i < 16; ++i)
    {
        g_print ("%f ", m[i]);

        if (++j == 4)
        {
            g_print ("\n");
            j = 0;
        }
    }
}

static void
print_coord (int x, int y, int z)
{
    GLdouble model[16];
    GLdouble proj[16];
    GLint    view[4];
    double   wx, wy, wz;

    glGetDoublev (GL_MODELVIEW_MATRIX, model);
    glGetDoublev (GL_PROJECTION_MATRIX, proj);
    glGetIntegerv (GL_VIEWPORT, view);

    gluProject (x, y, z,
                model, proj, view,
                &wx, &wy, &wz);

    print_matrix ("model", model);
    print_matrix ("proj",  proj);
    g_print ("viewport: %d %d %d %d\n", view[0], view[1], view[2], view[3]);

    g_print ("projected: %d %d %d  =>   %f %f %f\n", x, y, z, wx, wy, wz);
}

typedef struct
{
    double x;
    double y;
    double width;
    double height;
} DRectangle;

static void
screen_to_identity (WsRectangle *screen,
		    DRectangle  *identity,
		    double scr_w,
		    double scr_h)
{
#if 0
    g_print ("input: %d %d %d %d\n", screen->x, screen->y, screen->width, screen->height);
#endif
    identity->x = (screen->x / scr_w) * 2 - 1;
    identity->y = 1 - (screen->y / scr_h) * 2;
    identity->width = (screen->width / scr_w) * 2;
    identity->height = (screen->height / scr_h) * 2;
}

static void
print_drect (const char *header,
	      DRectangle *drect)
{
    g_print ("%s\n", header);
    g_print ("%f %f %f %f\n", drect->x, drect->y, drect->width, drect->height);
}

static void
cm_magnifier_render (CmNode *node,
		     CmState *state)
{
    CmMagnifier *mag = CM_MAGNIFIER (node);
    DRectangle source_trans;
    DRectangle target_trans;
    GLint coords[4];
    double sw, sh;
    
    cm_node_render (mag->child, state);

    if (!mag->active)
	return;

    glGetIntegerv (GL_VIEWPORT, coords);

    sw = coords[2];
    sh = coords[3];
    
    glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
    
    screen_to_identity (&mag->source, &source_trans, sw, sh);
    screen_to_identity (&mag->target, &target_trans, sw, sh);
    
    glPushMatrix();

#if 0
    glBegin (GL_LINES);
    glVertex2i (-1, 0);
    glVertex2i (1, 0);
    glVertex2i (0, -1);
    glVertex2i (0, 1);
    glEnd ();
    
    print_drect ("source", &source_trans);
    print_drect ("target", &target_trans);
#endif

    glColor4f (1.0, 0.0, 0.0, 1.0);
    glRectf (source_trans.x, source_trans.y,
	     source_trans.x + source_trans.width,
	     source_trans.y - source_trans.height);

    double xscale = target_trans.width / source_trans.width;
    double yscale = target_trans.height / source_trans.height;
    
    glScalef (xscale, yscale, 0.0);

    glTranslatef (-source_trans.x, -source_trans.y, 0);
    glTranslatef (- source_trans.width / 2.0,
		  source_trans.height / 2.0,
		  0.0);
    glTranslatef (target_trans.x / xscale, target_trans.y / yscale, 0.0);
    glTranslatef (target_trans.width / (xscale * 2.0),
		  - target_trans.height / (yscale * 2.0),
		  0.0);

    glScissor (mag->target.x,
	       sh - (mag->target.y + mag->target.height),
	       mag->target.width,
	       mag->target.height);
    
    glEnable (GL_SCISSOR_TEST);

    glColor4f (0.0, 0.0, 0.0, 1.0);
    
    cm_node_render (mag->child, state);

    glDisable (GL_SCISSOR_TEST);

    glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
    
    glColor4f (0.0, 1.0, 0.0, 1.0);
    glRectf (source_trans.x, source_trans.y,
	     source_trans.x + source_trans.width,
	     source_trans.y - source_trans.height);

    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
    
    glPopMatrix();
}

static void
cm_magnifier_class_init (CmMagnifierClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = CM_NODE_CLASS (class);
    
    object_class->finalize = cm_magnifier_finalize;
    node_class->render = cm_magnifier_render;
}

static void
cm_magnifier_init (CmMagnifier *magnifier)
{
}

CmMagnifier *
cm_magnifier_new (CmNode *child,
		  WsRectangle *source,
		  WsRectangle *target)
{
    g_return_val_if_fail (child != NULL, NULL);
    CmMagnifier *mag = g_object_new (CM_TYPE_MAGNIFIER, NULL);

    mag->source = *source;
    mag->target = *target;
    mag->active = TRUE;
    
    cm_node_own_child (CM_NODE (mag), &mag->child, child);
    
    return mag;
}

void
cm_magnifier_set_active (CmMagnifier *magnifier,
			 gboolean     active)
{
    g_return_if_fail (CM_IS_MAGNIFIER (magnifier));
    
    magnifier->active = !!active;

    cm_node_queue_repaint (CM_NODE (magnifier));
}
