/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "wsint.h"

/* Mostly Cut and paste from GDK */

gboolean
ws_rectangle_intersect (WsRectangle *src1,
			WsRectangle *src2,
			WsRectangle *dest)
{
    gint dest_x, dest_y;
    gint dest_w, dest_h;
    gint return_val;
    
    g_return_val_if_fail (src1 != NULL, FALSE);
    g_return_val_if_fail (src2 != NULL, FALSE);
    
    return_val = FALSE;
    
    dest_x = MAX (src1->x, src2->x);
    dest_y = MAX (src1->y, src2->y);
    dest_w = MIN (src1->x + src1->width, src2->x + src2->width) - dest_x;
    dest_h = MIN (src1->y + src1->height, src2->y + src2->height) - dest_y;
    
    if (dest_w > 0 && dest_h > 0)
    {
	if (dest)
	{
	    dest->x = dest_x;
	    dest->y = dest_y;
	    dest->width = dest_w;
	    dest->height = dest_h;
	}

	return_val = TRUE;
    }
    else
    {
	if (dest)
	{
	    dest->width = 0;
	    dest->height = 0;
	}
    }
    
    return return_val;
}

gboolean
ws_format_is_viewable (WsFormat format)
{
    g_return_val_if_fail (format != WS_FORMAT_LAST, FALSE);
    
    return (format != WS_FORMAT_INPUT_ONLY &&
	    format != WS_FORMAT_UNKNOWN);
}

void
ws_format_get_masks (WsFormat format,
		     gulong *  red_mask,
		     gulong *  green_mask,
		     gulong *  blue_mask)
{
    gulong rm = 0;
    gulong gm = 0;
    gulong bm = 0;
    
    g_return_if_fail (ws_format_is_viewable (format));

    switch (format)
    {
    case WS_FORMAT_RGB_16:
	rm = 0xf800;
	gm = 0x07e0;
	bm = 0x001f;
	break;
	
    case WS_FORMAT_RGB_24:
	rm = 0xff0000;
	gm = 0x00ff00;
	bm = 0x0000ff;
	break;
	
    case WS_FORMAT_XRGB_32:
	rm = 0xff0000;
	gm = 0x00ff00;
	bm = 0x0000ff;
	break;
	
    case WS_FORMAT_ARGB_32:
	rm = 0xff0000;
	gm = 0x00ff00;
	bm = 0x0000ff;
	break;
	
    case WS_FORMAT_INPUT_ONLY:
    case WS_FORMAT_UNKNOWN:
    case WS_FORMAT_LAST:
	g_assert_not_reached();
	return;
    }

    if (red_mask)
	*red_mask = rm;

    if (green_mask)
	*green_mask = gm;

    if (blue_mask)
	*blue_mask = bm;
}

guint
ws_format_get_depth (WsFormat format)
{
    g_return_val_if_fail (ws_format_is_viewable (format), 0);

    switch (format)
    {
    case WS_FORMAT_RGB_16:
	return 16;

    case WS_FORMAT_RGB_24:
	return 24;
	
    case WS_FORMAT_XRGB_32:
	return 32;

    case WS_FORMAT_ARGB_32:
	return 32;

    case WS_FORMAT_INPUT_ONLY:
    case WS_FORMAT_UNKNOWN:
    case WS_FORMAT_LAST:
	break;
    };

    g_assert_not_reached();
    return 42;
}

gboolean
ws_format_has_alpha (WsFormat format)
{
    return format == WS_FORMAT_ARGB_32;
}
