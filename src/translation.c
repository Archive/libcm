/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include <glib.h>
#include <GL/gl.h>
#include "node.h"
#include "translation.h"

static void translation_compute_extents (CmNode  *node,
					 Extents *extents);
static void translation_render          (CmNode  *node,
					 CmState *state);

G_DEFINE_TYPE (CmTranslation, cm_translation, CM_TYPE_NODE);

static void
cm_translation_finalize (GObject *object)
{
    G_OBJECT_CLASS (cm_translation_parent_class)->finalize (object);
}

static void
cm_translation_class_init (CmTranslationClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = CM_NODE_CLASS (class);
    
    object_class->finalize = cm_translation_finalize;
    
    node_class->render = translation_render;
    node_class->compute_extents = translation_compute_extents;
}

static void
cm_translation_init (CmTranslation *translation)
{

}

static void
translation_compute_extents (CmNode *node,
			     Extents *extents)
{
    extents->x = 0.0;
    extents->y = 0.0;
    extents->z = 0.0;
    extents->width = 1.0;
    extents->height = 1.0;
    extents->depth = 1.0;
}

static void
translation_render (CmNode *node,
		    CmState *state)
{
    CmTranslation *translation = (CmTranslation *)node;
    
    glPushMatrix();

    glTranslatef (translation->x,
		  translation->y,
		  translation->z);

    cm_node_render (translation->child, state);

    glPopMatrix();
}

CmTranslation *
cm_translation_new (CmNode *child)
{
    CmTranslation *translation = g_object_new (CM_TYPE_TRANSLATION, NULL);

    translation->x = 0.0;
    translation->y = 0.0;
    translation->z = 0.0;

    translation->child = child;
    
    return translation;
}

void
cm_translation_set_translation (CmTranslation *translation,
				gdouble	     x,
				gdouble	     y,
				gdouble      z)
{
    translation->x = x;
    translation->y = y;
    translation->z = z;
}

