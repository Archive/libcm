/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "node.h"
#include <glib.h>
#include "state.h"

G_DEFINE_TYPE (CmNode, cm_node, G_TYPE_OBJECT);

static WsRegion *cm_node_real_get_covered_region (CmNode *node);

enum
{
    NEED_REPAINT,
    N_SIGNALS
};

static guint signals[N_SIGNALS];

static void
cm_node_finalize (GObject *object)
{
    G_OBJECT_CLASS (cm_node_parent_class)->finalize (object);
}

static void
cm_node_class_init (CmNodeClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = CM_NODE_CLASS (class);
    
    object_class->finalize = cm_node_finalize;

    node_class->get_covered_region = cm_node_real_get_covered_region;
    
    signals[NEED_REPAINT] = g_signal_new ("need_repaint",
					  G_OBJECT_CLASS_TYPE (object_class),
					  G_SIGNAL_RUN_LAST,
					  0,
					  NULL, NULL,
					  g_cclosure_marshal_VOID__VOID,
					  G_TYPE_NONE, 0);
}

static void
cm_node_init (CmNode *node)
{
    
}

static WsRegion *
cm_node_real_get_covered_region (CmNode *node)
{
    return NULL;
}

gboolean
cm_node_is_toplevel (CmNode *node)
{
    GList *list;

    for (list = node->parents; list != NULL; list = list->next)
    {
	if (list->data)
	    return FALSE;
    }

    return TRUE;
}
	    
void
cm_node_queue_repaint (CmNode *node)
{
    while (!cm_node_is_toplevel (node))
    {
	GList *list;
	
	for (list = node->parents; list != NULL; list = list->next)
	{
	    CmNode *parent = list->data;
	    
	    if (parent)
	    {
		node = parent;
		break;
	    }
	}
    }

#if 0
    g_print ("emitting on %s\n", G_OBJECT_TYPE_NAME (toplevel));
#endif
    
    g_signal_emit (node, signals[NEED_REPAINT], 0);
}

gdouble
cm_node_get_time (CmNode *node)
{
    static GTimer *timer;
    
    if (!timer)
	timer = g_timer_new ();
    
    return g_timer_elapsed (timer, NULL);
}

void
cm_node_render (CmNode *node,
		CmState *state)
{
    CmState *free_me = NULL;
    
    g_return_if_fail (CM_IS_NODE (node));
    
    if (!state)
	free_me = state = cm_state_new ();
    
    CM_NODE_GET_CLASS (node)->render (node, state);
    
    if (free_me)
	g_object_unref (G_OBJECT (free_me));
}

WsRegion *
cm_node_get_covered_region (CmNode *node)
{
    g_return_val_if_fail (CM_IS_NODE (node), NULL);
    
    return CM_NODE_GET_CLASS (node)->get_covered_region (node);
}

static void
cm_node_remove_parent (CmNode *node,
		       CmNode *parent)
{
    GList *list;

    for (list = node->parents; list != NULL; list = list->next)
    {
	if (list->data == parent)
	{
	    g_object_remove_weak_pointer (G_OBJECT (list->data),
					  (gpointer *)&(list->data));
	    list->data = NULL;
	    break;
	}
    }
}

static void
cm_node_add_parent (CmNode *node,
		    CmNode *parent)
{
    g_return_if_fail (parent != NULL);
    
    if (g_list_find (node->parents, parent))
    {
	g_warning ("A node cannot be added twice to the same parent");
	return;
    }
    
    node->parents = g_list_prepend (node->parents, parent);
    
    g_object_add_weak_pointer (G_OBJECT (parent),
			       (gpointer *)&(node->parents->data));
}

void
cm_node_disown_child (CmNode *node,
		      CmNode **child_location)
{
    g_return_if_fail (CM_IS_NODE (node));
    g_return_if_fail (child_location != NULL);
    g_return_if_fail (*child_location == NULL || CM_IS_NODE (*child_location));
    
    if (*child_location)
    {
	cm_node_remove_parent (*child_location, node);
	g_object_unref (*child_location);
    }

    *child_location = NULL;
    
    cm_node_queue_repaint (node);
}

void
cm_node_own_child (CmNode *node,
		   CmNode **child_location,
		   CmNode *child)
{
    g_return_if_fail (CM_IS_NODE (node));
    g_return_if_fail (child == NULL || CM_IS_NODE (child));
    g_return_if_fail (child_location != NULL);
    
    if (*child_location == child)
	return;
    
    cm_node_disown_child (node, child_location);
    
    *child_location = child;
    
    if (*child_location)
    {
	cm_node_add_parent (*child_location, node);
	g_object_ref (*child_location);
    }
    
    cm_node_queue_repaint (node);
}

#if 0
GList *
cm_node_list_children (CmNode *node)
{
    return NULL;
}
#endif

