/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "node.h"
#include "ws.h"

#define CM_TYPE_PIX_TEXTURE            (cm_pix_texture_get_type ())
#define CM_PIX_TEXTURE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CM_TYPE_PIX_TEXTURE, CmPixTexture))
#define CM_PIX_TEXTURE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CM_TYPE_PIX_TEXTURE, CmPixTextureClass))
#define CM_IS_PIX_TEXTURE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CM_TYPE_PIX_TEXTURE))
#define CM_IS_PIX_TEXTURE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CM_TYPE_PIX_TEXTURE))
#define CM_PIX_TEXTURE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CM_TYPE_PIX_TEXTURE, CmPixTextureClass))

typedef struct _CmPixTexture CmPixTexture;
typedef struct _CmPixTextureClass CmPixTextureClass;

struct _CmPixTexture
{
    CmNode parent_instance;

    gboolean		updates;
    WsRectangle		geometry;
    WsPixmap *		pixmap;
    CmNode *		child;
};

struct _CmPixTextureClass
{
    CmNodeClass parent_class;
};

GType cm_pix_texture_get_type (void);

CmPixTexture *cm_pix_texture_new (WsPixmap *pixmap,
				  CmNode *child);
void cm_pix_texture_set_child (CmPixTexture *pix_texture,
			       CmNode *child);
void cm_pix_texture_set_pixmap (CmPixTexture *pix_texture,
				WsPixmap *pixmap);
gboolean cm_pix_texture_has_alpha (CmPixTexture *pix_texture);
