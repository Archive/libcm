/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "node.h"
#include "state.h"

#define CM_TYPE_TEX_ENV            (cm_tex_env_get_type ())
#define CM_TEX_ENV(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CM_TYPE_TEX_ENV, CmTexEnv))
#define CM_TEX_ENV_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CM_TYPE_TEX_ENV, CmTexEnvClass))
#define CM_IS_TEX_ENV(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CM_TYPE_TEX_ENV))
#define CM_IS_TEX_ENV_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CM_TYPE_TEX_ENV))
#define CM_TEX_ENV_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CM_TYPE_TEX_ENV, CmTexEnvClass))

typedef struct _CmTexEnv CmTexEnv;
typedef struct _CmTexEnvClass CmTexEnvClass;

typedef enum
{
    CM_MODULATE,
    CM_REPLACE,
} CmTexEnvFunc;

struct _CmTexEnv
{
    CmNode		parent_instance;

    CmNode *		child;

    CmTexEnvFunc	func;

    gboolean		has_modulate_color;
    WsColor		modulate_color;
};

struct _CmTexEnvClass
{
    CmNodeClass parent_class;
};

GType cm_tex_env_get_type (void);

CmTexEnv *cm_tex_env_new (CmNode *child);

/* FIXME: Maybe there should be a mode where color comes from the actual fragment,
 * instead of being set explicitly here. How do we generally deal with colors?
 * 
 */
void cm_tex_env_set_modulate (CmTexEnv *env,
			      WsColor *color);
void cm_tex_env_set_replace (CmTexEnv *env);
