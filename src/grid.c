/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "grid.h"
#include "state.h"

G_DEFINE_TYPE (CmGrid, cm_grid, CM_TYPE_NODE);

static void cm_grid_render (CmNode *node, CmState *state);

static void
cm_grid_finalize (GObject *object)
{
    G_OBJECT_CLASS (cm_grid_parent_class)->finalize (object);
}

typedef struct
{
    double x;
    double y;
} Point;

static void
emit_vertex (WsRectangle *rect, CmState *state,
	     Point *points, int h_dim, int i, int j)
{
    gdouble x = points[j * h_dim + i].x;
    gdouble y = points[j * h_dim + i].y;
    gdouble u = (x - rect->x) / rect->width;
    gdouble v = (y - rect->y) / rect->height;
    
    cm_state_set_vertex_state (state, &x, &y, NULL, u, v);

    glVertex2i ((int)x, (int)y);
}

static void
cm_grid_render_rect (CmState *state,
		     int hspacing,
		     int vspacing,
		     WsRectangle *rect,
		     WsRectangle *clipbox)
{
    Point *points;
    int i, j, k;
    
    /* Build table of points */
    int h_dim = (rect->width / hspacing) + 2;
    int v_dim = (rect->height / vspacing) + 2;
    
    points = g_malloc (h_dim * v_dim * sizeof (Point));

    for (i = 0; i < h_dim; ++i)
    {
	for (j = 0; j < v_dim; ++j)
	{
	    double x = rect->x + hspacing * i;
	    double y = rect->y + vspacing * j;
	    
	    if (x > rect->x + rect->width)
		x = rect->x + rect->width;
	    
	    if (y > rect->y + rect->height)
		y = rect->y + rect->height;

	    points[j * h_dim + i].x = x;
	    points[j * h_dim + i].y = y;
	}
    }

    /* Emit points as quad strips */
    for (i = 1; i < h_dim; ++i)
    {
	glBegin (GL_QUAD_STRIP);
	
	emit_vertex (clipbox, state, points, h_dim, i - 1, 0);
	emit_vertex (clipbox, state, points, h_dim, i, 0);

	for (j = 1; j < v_dim; ++j)
	{
	    emit_vertex (clipbox, state, points, h_dim, i - 1, j);
	    emit_vertex (clipbox, state, points, h_dim, i, j);
	}

	glEnd ();
    }

    g_free (points);
}

static void
dump_region (const char *header,
	     WsRegion *region)
{
    WsRectangle *rects;
    int		 n_rects;
    int i;
    ws_region_get_rectangles (region, &rects, &n_rects);

    g_print ("%s\n", header);
    for (i = 0; i < n_rects; ++i)
    {
	g_print ("     %d %d %d %d\n",
		 rects[i].x,
		 rects[i].y,
		 rects[i].width,
		 rects[i].height);
    }
}

static void
print_region (const char *header, WsRegion *region)
{
    int i;
    int n_rects;
    WsRectangle *rects = NULL;

    ws_region_get_rectangles (region, &rects, &n_rects);

    g_print ("%s: \n", header);
    for (i = 0; i < n_rects; ++i)
    {
	WsRectangle *r = &(rects[i]);

	g_print ("   %d %d %d %d\n", r->x, r->y, r->width, r->height);
    }
    
    if (rects)
	g_free (rects);
    
}

static void
cm_grid_render (CmNode *node,
		CmState *state)
{
    CmGrid *grid = CM_GRID (node);
    WsRectangle *rects;
    int i, n_rects;
    WsRectangle clipbox;
    WsRegion *region;

#if 0
    print_region ("covered region: ", cm_state_peek_covered_region (state));
#endif
    
    ws_region_get_clipbox (grid->region, &clipbox);


    region = cm_state_get_visible_region (state);

    ws_region_intersect (region, grid->region);
    
    ws_region_get_rectangles (region, &rects, &n_rects);

#if 0
    glPushAttrib (GL_ALL_ATTRIB_BITS);
    
    if (n_rects > 10)
	glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
#endif
    
    for (i = 0; i < n_rects; ++i)
    {
	cm_grid_render_rect (state,
			     grid->hspacing,
			     grid->vspacing, &(rects[i]), &clipbox);
    }

    ws_region_destroy (region);
    
#if 0
    glPopAttrib ();
#endif
    
    if (rects)
	g_free (rects);
}

static void
cm_grid_class_init (CmGridClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = CM_NODE_CLASS (class);
    
    object_class->finalize = cm_grid_finalize;
    node_class->render = cm_grid_render;
}

static void
cm_grid_init (CmGrid *grid)
{
    grid->hspacing = 32;
    grid->vspacing = 32;
}

CmGrid *
cm_grid_new (int x, int y, int w, int h,
	     int hspacing,
	     int vspacing)
{
    CmGrid *grid = g_object_new (CM_TYPE_GRID, NULL);
    
    cm_grid_set_geometry (grid, x, y, w, h);
    
    return grid;
}

void
cm_grid_set_region (CmGrid *grid,
		    WsRegion *region)
{
    if (grid->region)
	ws_region_destroy (grid->region);

    grid->region = ws_region_copy (region);
}

void
cm_grid_set_geometry (CmGrid *grid,
		      int x, int y,
		      int w, int h)
{
    WsRectangle rect;
    WsRegion *region;
    
    g_return_if_fail (CM_IS_GRID (grid));
    
    rect.x = x;
    rect.y = y;
    rect.width = w;
    rect.height = h;
    
    region = ws_region_rectangle (&rect);
    
    cm_grid_set_region (grid, region);
    
    ws_region_destroy (region);
}
