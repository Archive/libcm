/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#ifndef CUBE_H
#define CUBE_H

#define CM_TYPE_CUBE            (cm_cube_get_type ())
#define CM_CUBE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CM_TYPE_CUBE, CmCube))
#define CM_CUBE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CM_TYPE_CUBE, CmCubeClass))
#define CM_IS_CUBE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CM_TYPE_CUBE))
#define CM_IS_CUBE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CM_TYPE_CUBE))
#define CM_CUBE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CM_TYPE_CUBE, CmCubeClass))

typedef struct _CmCube CmCube;
typedef struct _CmCubeClass CmCubeClass;

struct _CmCube
{
    CmNode parent_instance;

    CmNode *faces[6];
};

struct _CmCubeClass
{
    CmNodeClass parent_class;
};

GType cm_cube_get_type (void);

CmCube *cm_cube_new (void);


void cm_cube_set_face (CmCube *cube,
		       int face_no,
		       CmNode *child);

#endif
