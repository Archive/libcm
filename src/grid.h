/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "node.h"

#include "ws.h"

#define CM_TYPE_GRID            (cm_grid_get_type ())
#define CM_GRID(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CM_TYPE_GRID, CmGrid))
#define CM_GRID_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CM_TYPE_GRID, CmGridClass))
#define CM_IS_GRID(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CM_TYPE_GRID))
#define CM_IS_GRID_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CM_TYPE_GRID))
#define CM_GRID_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CM_TYPE_GRID, CmGridClass))

typedef struct _CmGrid CmGrid;
typedef struct _CmGridClass CmGridClass;

struct _CmGrid
{
    CmNode parent_instance;

    int hspacing;
    int vspacing;

    WsRegion *region;
};

struct _CmGridClass
{
    CmNodeClass parent_class;
};

GType cm_grid_get_type (void);

CmGrid *cm_grid_new (int x, int y, int w, int h,
		  int hspacing,
		  int vspacing);

void cm_grid_set_geometry (CmGrid *grid,
			   int x, int y,
			   int w, int h);
void cm_grid_set_region (CmGrid *grid,
			 WsRegion *region);
