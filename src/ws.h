/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#ifndef WS_H
#define WS_H

#include <glib.h>
#include <glib-object.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <GL/gl.h>

typedef struct WsDisplay WsDisplay;
typedef struct WsDisplayClass WsDisplayClass;
typedef struct WsScreen WsScreen;
typedef struct WsScreenClass WsScreenClass;
typedef struct WsResource WsResource;
typedef struct WsResourceClass WsResourceClass;
typedef struct WsDrawable WsDrawable;
typedef struct WsDrawableClass WsDrawableClass;
typedef struct WsPixmap WsPixmap;
typedef struct WsPixmapClass WsPixmapClass;
typedef struct WsWindow WsWindow;
typedef struct WsWindowClass WsWindowClass;
typedef struct WsServerRegion WsServerRegion;
typedef struct WsServerRegionClass WsServerRegionClass;
typedef struct WsBits WsBits;
typedef struct WsSyncCounter WsSyncCounter;
typedef struct WsSyncCounterClass WsSyncCounterClass;
typedef struct WsSyncAlarm WsSyncAlarm;
typedef struct WsSyncAlarmClass WsSyncAlarmClass;

/* WsTime */
#define WS_CURRENT_TIME		(0L)

/* WsRectangle */

typedef struct
{
    int x, y;
    int width, height;
} WsRectangle;

gboolean     ws_rectangle_intersect (WsRectangle *src1,
				     WsRectangle *src2,
				     WsRectangle *dest);

/* WsFormat */

/* This is my current understanding. In case anybody
 * is interested in this stuff.
 *
 * Suppose guchar *pixel points to a pixel in WS_FORMAT_ABCD. Then,
 * on little endian:
 *
 *	pixel[0] is the D component
 *	pixel[1] is the C component
 *	pixel[2] is the B component
 *	pixel[3] is the A component
 *
 * on big endian:
 *
 *	pixel[0] is the A component
 *	pixel[1] is the B component
 *      etc.
 *
 * Note that OpenGL uses a different scheme, where ABCD always
 * means that A is higher in memory than D, independent of
 * endianness. (This is the part I'm unsure about).
 *
 */
typedef enum
{
    WS_FORMAT_UNKNOWN,
    WS_FORMAT_INPUT_ONLY,
    WS_FORMAT_RGB_16,
    WS_FORMAT_RGB_24,
    WS_FORMAT_XRGB_32,
    WS_FORMAT_ARGB_32,
    WS_FORMAT_LAST
} WsFormat;

guint    ws_format_get_depth (WsFormat format);
gboolean ws_format_is_viewable (WsFormat format);
void     ws_format_get_masks (WsFormat format,
			      gulong *  red_mask,
			      gulong *  green_mask,
			      gulong *  blue_mask);
gboolean ws_format_has_alpha (WsFormat format);

/* WsColor */

typedef struct
{
    gint alpha;
    gint red;
    gint green;
    gint blue;
} WsColor;

/* WsDisplay */

/* FIXME Maybe get rid of Event posfix */

typedef struct WsConfigureEvent WsConfigureEvent;
typedef struct WsSelectionClearEvent WsSelectionClearEvent;
typedef struct WsAlarmNotifyEvent WsAlarmNotifyEvent;
typedef struct WsDamageNotifyEvent WsDamageNotifyEvent;

typedef enum
{
    WS_CONFIGURE_EVENT,
    WS_SELECTION_CLEAR
} WsEventType;

struct WsConfigureEvent
{
    WsWindow   *window;
    int		x;
    int		y;
    int		width;
    int		height;
    int		border_width;
    WsWindow   *above_this;
    gboolean	override_redirect;
};

struct WsSelectionClearEvent
{
    WsWindow	*window;
    const char	*selection;
};

struct WsAlarmNotifyEvent
{
    WsSyncAlarm		*alarm;
    WsSyncCounter	*counter;
    gint64		 counter_value;
    gint64 		 alarm_value;
};

#if 0
typedef struct
{
    WsEventType		type;
    
    union
    {
	WsConfigureEvent	configure;
	WsSelectionClearEvent	clear;
	WsAlarmNotifyEvent	alarm;
    } u;
} WsEvent;
#endif


#define WS_TYPE_DISPLAY            (ws_display_get_type ())
#define WS_DISPLAY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WS_TYPE_DISPLAY, WsDisplay))
#define WS_DISPLAY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WS_TYPE_DISPLAY, WsDisplayClass))
#define WS_IS_DISPLAY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WS_TYPE_DISPLAY))
#define WS_IS_DISPLAY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WS_TYPE_DISPLAY))
#define WS_DISPLAY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WS_TYPE_DISPLAY, WsDisplayClass))

GType      ws_display_get_type                   (void);
WsDisplay *ws_display_new                        (const char  *dpy);
WsDisplay *ws_display_new_from_xdisplay          (Display     *xdisplay);
void       ws_display_process_xevent             (WsDisplay   *display,
						  XEvent      *xev);
void       ws_display_process_xerror             (WsDisplay   *display,
						  XErrorEvent *error);
void       ws_display_grab                       (WsDisplay   *display);
void       ws_display_ungrab                     (WsDisplay   *display);
gboolean   ws_display_init_composite             (WsDisplay   *display);
gboolean   ws_display_init_damage                (WsDisplay   *display);
gboolean   ws_display_init_fixes                 (WsDisplay   *display);
gboolean   ws_display_init_test                  (WsDisplay   *display);
gboolean   ws_display_init_glx                   (WsDisplay   *display);
gboolean   ws_display_init_sync			 (WsDisplay   *display);
GList *    ws_display_list_screens               (WsDisplay   *display);
void       ws_display_sync                       (WsDisplay   *display);
void       ws_display_sync_with_xdisplay         (WsDisplay   *display,
						  gpointer     dpy);
void       ws_display_flush                      (WsDisplay   *display);
void       ws_display_set_synchronize            (WsDisplay   *display,
						  gboolean     sync);
void       ws_display_set_ignore_grabs           (WsDisplay   *display,
						  gboolean     ignore);
void       ws_display_begin_error_trap           (WsDisplay   *display);
void       ws_display_end_error_trap             (WsDisplay   *display);
gboolean   ws_display_end_error_trap_with_return (WsDisplay   *display);
WsScreen * ws_display_get_default_screen         (WsDisplay   *display);
WsScreen * ws_display_get_screen_from_number     (WsDisplay   *display,
						  int          number);

/* WsScreen */
#define WS_TYPE_SCREEN            (ws_screen_get_type ())
#define WS_SCREEN(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WS_TYPE_SCREEN, WsScreen))
#define WS_SCREEN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WS_TYPE_SCREEN, WsScreenClass))
#define WS_IS_SCREEN(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WS_TYPE_SCREEN))
#define WS_IS_SCREEN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WS_TYPE_SCREEN))
#define WS_SCREEN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WS_TYPE_SCREEN, WsScreenClass))

GType     ws_screen_get_type          (void);
WsWindow *ws_screen_get_root_window   (WsScreen *screen);
int       ws_screen_get_number        (WsScreen *screen);
void      ws_screen_graphics_sync     (WsScreen *screen);
int       ws_screen_get_height        (WsScreen *screen);
int       ws_screen_get_width         (WsScreen *screen);
WsWindow *ws_screen_get_gl_window     (WsScreen *screen);
void      ws_screen_release_gl_window (WsScreen *screen);

/* WsResource */
#define WS_TYPE_RESOURCE            (ws_resource_get_type ())
#define WS_RESOURCE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WS_TYPE_RESOURCE, WsResource))
#define WS_RESOURCE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WS_TYPE_RESOURCE, WsResourceClass))
#define WS_IS_RESOURCE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WS_TYPE_RESOURCE))
#define WS_IS_RESOURCE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WS_TYPE_RESOURCE))
#define WS_RESOURCE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WS_TYPE_RESOURCE, WsResourceClass))

GType ws_resource_get_type (void);


/* WsServerRegion */
#define WS_TYPE_REGION                  (ws_server_region_get_type ())
#define WS_SERVER_REGION(obj)           (G_TYPE_CHECK_INSTANCE_CAST ((obj), WS_TYPE_REGION, WsServerRegion))
#define WS_SERVER_REGION_CLASS(klass)   (G_TYPE_CHECK_CLASS_CAST ((klass),  WS_TYPE_REGION, WsServerRegionClass))
#define WS_IS_REGION(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WS_TYPE_REGION))
#define WS_IS_REGION_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass),  WS_TYPE_REGION))
#define WS_SERVER_REGION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj),  WS_TYPE_REGION, WsServerRegionClass))

GType ws_server_region_get_type (void);

WsServerRegion *ws_server_region_new              (WsDisplay      *ws);
WsRectangle *   ws_server_region_query_rectangles (WsServerRegion *region,
						   guint          *n_rectangles);
void            ws_server_region_union            (WsServerRegion *region,
						   WsServerRegion *other);

/* WsDrawable */
#define WS_TYPE_DRAWABLE            (ws_drawable_get_type ())
#define WS_DRAWABLE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WS_TYPE_DRAWABLE, WsDrawable))
#define WS_DRAWABLE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WS_TYPE_DRAWABLE, WsDrawableClass))
#define WS_IS_DRAWABLE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WS_TYPE_DRAWABLE))
#define WS_IS_DRAWABLE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WS_TYPE_DRAWABLE))
#define WS_DRAWABLE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WS_TYPE_DRAWABLE, WsDrawableClass))

GType ws_drawable_get_type (void);

WsDisplay *   ws_drawable_get_display    (WsDrawable     *window);
WsScreen *    ws_drawable_query_screen   (WsDrawable     *window);
void          ws_drawable_copy_area      (WsDrawable     *src,
					  int             src_x,
					  int             src_y,
					  int             width,
					  int             height,
					  WsDrawable     *dst,
					  int             dst_x,
					  int             dst_y,
					  WsServerRegion *clip);
gboolean      ws_drawable_query_geometry (WsDrawable     *drawable,
					  WsRectangle    *rect);
int           ws_drawable_query_depth    (WsDrawable     *drawable);
WsBits   *    ws_drawable_get_bits       (WsDrawable     *drawable,
					  WsRectangle    *rect);
WsFormat      ws_drawable_get_format     (WsDrawable     *drawable);
const guchar *ws_bits_get_info           (WsBits         *bits,
					  guint          *width,
					  guint          *height,
					  guint          *bytes_per_pixel,
					  guint          *bytes_per_row);
void          ws_bits_free               (WsBits         *bits);
void          ws_drawable_free           (WsDrawable     *drawable);


/* WsPixmap */
#define WS_TYPE_PIXMAP            (ws_pixmap_get_type ())
#define WS_PIXMAP(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WS_TYPE_PIXMAP, WsPixmap))
#define WS_PIXMAP_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WS_TYPE_PIXMAP, WsPixmapClass))
#define WS_IS_PIXMAP(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WS_TYPE_PIXMAP))
#define WS_IS_PIXMAP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WS_TYPE_PIXMAP))
#define WS_PIXMAP_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WS_TYPE_PIXMAP, WsPixmapClass))

GType     ws_pixmap_get_type (void);

WsPixmap *ws_pixmap_new (WsDrawable *drawable,
			 int	     width,
			 int	     height);
GLuint	  ws_pixmap_get_texture (WsPixmap *pixmap);
void      ws_pixmap_set_updates (WsPixmap *pixmap,
				 gboolean  do_updates);
gboolean  ws_pixmap_get_updates (WsPixmap *pixmap);

/* WsRegion */
typedef struct WsRegion WsRegion;

/* Types of overlapping between a rectangle and a region
 * WS_OVERLAP_RECTANGLE_IN: rectangle is in region
 * WS_OVERLAP_RECTANGLE_OUT: rectangle in not in region
 * WS_OVERLAP_RECTANGLE_PART: rectangle in partially in region
 */
typedef enum
{
    WS_OVERLAP_RECTANGLE_IN,
    WS_OVERLAP_RECTANGLE_OUT,
    WS_OVERLAP_RECTANGLE_PART
} WsOverlapType;

WsRegion *    ws_region_new             (void);
WsRegion *    ws_region_copy            (WsRegion     *region);
WsRegion *    ws_region_rectangle       (WsRectangle  *rectangle);
void          ws_region_destroy         (WsRegion     *region);
void          ws_region_get_clipbox     (WsRegion     *region,
					 WsRectangle  *rectangle);
void          ws_region_get_rectangles  (WsRegion     *region,
					 WsRectangle **rectangles,
					 gint         *n_rectangles);
gboolean      ws_region_empty           (WsRegion     *region);
gboolean      ws_region_equal           (WsRegion     *region1,
					 WsRegion     *region2);
gboolean      ws_region_point_in        (WsRegion     *region,
					 int           x,
					 int           y);
WsOverlapType ws_region_rect_in         (WsRegion     *region,
					 WsRectangle  *rect);
void          ws_region_offset          (WsRegion     *region,
					 gint          dx,
					 gint          dy);
void          ws_region_shrink          (WsRegion     *region,
					 gint          dx,
					 gint          dy);
void          ws_region_union_with_rect (WsRegion     *region,
					 WsRectangle  *rect);
void          ws_region_intersect       (WsRegion     *source1,
					 WsRegion     *source2);
void          ws_region_union           (WsRegion     *source1,
					 WsRegion     *source2);
void          ws_region_subtract        (WsRegion     *source1,
					 WsRegion     *source2);
void          ws_region_xor             (WsRegion     *source1,
					 WsRegion     *source2);

/* WsTexture */

/* WsWindow */
#define WS_TYPE_WINDOW            (ws_window_get_type ())
#define WS_WINDOW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WS_TYPE_WINDOW, WsWindow))
#define WS_WINDOW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WS_TYPE_WINDOW, WsWindowClass))
#define WS_IS_WINDOW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WS_TYPE_WINDOW))
#define WS_IS_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WS_TYPE_WINDOW))
#define WS_WINDOW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WS_TYPE_WINDOW, WsWindowClass))

GType ws_window_get_type (void);

gboolean       ws_window_query_mapped              (WsWindow       *window);
void           ws_window_raise                     (WsWindow       *window);
void           ws_window_map                       (WsWindow       *window);
void           ws_window_unmap                     (WsWindow       *window);
WsWindow *     ws_window_new                       (WsWindow       *parent);
GList *        ws_window_query_subwindows          (WsWindow       *window);
void           ws_window_unredirect                (WsWindow       *window);
void           ws_window_redirect                  (WsWindow       *window);
void           ws_window_set_override_redirect     (WsWindow       *window,
						    gboolean        override_redirect);
gboolean       ws_window_query_override_redirect   (WsWindow       *window);
WsPixmap *     ws_window_name_pixmap               (WsWindow       *window);
void           ws_window_redirect_subwindows       (WsWindow       *window);
void           ws_window_unredirect_subwindows     (WsWindow       *window);
gboolean       ws_window_query_input_only          (WsWindow       *window);
gboolean       ws_window_query_viewable            (WsWindow       *window);
char *         ws_window_query_title               (WsWindow       *window);
WsWindow *     ws_window_lookup                    (WsDisplay      *display,
						    Window          xid);
void           ws_window_own_selection             (WsWindow       *window,
						    const char     *selection,
						    guint32         time);
/* FIXME: figure out whether shapes are on the server or locally */
void           ws_window_set_input_shape           (WsWindow       *window,
						    WsServerRegion *shape);
WsRegion *     ws_window_get_output_shape          (WsWindow       *window);
gchar **       ws_window_get_property_atom_list    (WsWindow       *window,
						    const char     *property_name);
WsSyncCounter *ws_window_get_property_sync_counter (WsWindow       *window,
						    const char     *property_name);
void	       ws_window_send_client_message	   (WsWindow	   *window,
						    const char     *msg_type,
						    guint32	    msg[5]);
void           ws_window_send_configure_notify     (WsWindow *window,
						    int       x,
						    int       y,
						    int       width,
						    int       height,
						    int       border_width,
						    gboolean  override_redirect);

/*
 * WsSyncCounter
 */
#define WS_TYPE_SYNC_COUNTER            (ws_sync_counter_get_type ())
#define WS_SYNC_COUNTER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WS_TYPE_SYNC_COUNTER, WsSyncCounter))
#define WS_SYNC_COUNTER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WS_TYPE_SYNC_COUNTER, WsSyncCounterClass))
#define WS_IS_SYNC_COUNTER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WS_TYPE_SYNC_COUNTER))
#define WS_IS_SYNC_COUNTER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WS_TYPE_SYNC_COUNTER))
#define WS_SYNC_COUNTER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WS_TYPE_SYNC_COUNTER, WsSyncCounterClass))

GType ws_sync_counter_get_type (void);

WsSyncCounter *ws_sync_counter_new         (WsDisplay     *display,
					    gint64         value);
WsSyncCounter *ws_sync_counter_ensure      (WsDisplay     *display,
					    XID		   xid);
gint64         ws_sync_counter_query_value (WsSyncCounter *counter);
void           ws_sync_counter_set         (WsSyncCounter *counter,
					    gint64         v);
void           ws_sync_counter_change      (WsSyncCounter *counter,
					    gint64         delta);
void	       ws_sync_counter_await       (WsSyncCounter *counter,
					    gint64	   value);

/*
 * WsSyncAlarm
 */
#define WS_TYPE_SYNC_ALARM            (ws_sync_alarm_get_type ())
#define WS_SYNC_ALARM(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WS_TYPE_SYNC_ALARM, WsSyncAlarm))
#define WS_SYNC_ALARM_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WS_TYPE_SYNC_ALARM, WsSyncAlarmClass))
#define WS_IS_SYNC_ALARM(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WS_TYPE_SYNC_ALARM))
#define WS_IS_SYNC_ALARM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WS_TYPE_SYNC_ALARM))
#define WS_SYNC_ALARM_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WS_TYPE_SYNC_ALARM, WsSyncAlarmClass))

GType ws_sync_alarm_get_type (void);

WsSyncAlarm *ws_sync_alarm_new (WsDisplay     *display,
				WsSyncCounter *counter);
void         ws_sync_alarm_set (WsSyncAlarm   *alarm,
				gint64         value);

/* WsTexture */
#define WS_TYPE_TEXTURE            (ws_texture_get_type ())
#define WS_TEXTURE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), WS_TYPE_TEXTURE, WsTexture))
#define WS_TEXTURE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  WS_TYPE_TEXTURE, WsTextureClass))
#define WS_IS_TEXTURE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WS_TYPE_TEXTURE))
#define WS_IS_TEXTURE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  WS_TYPE_TEXTURE))
#define WS_TEXTURE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  WS_TYPE_TEXTURE, WsTextureClass))

typedef struct _WsTexture WsTexture;
typedef struct _WsTextureClass WsTextureClass;

struct _WsTexture
{
    GObject parent_instance;
};

struct _WsTextureClass
{
    GObjectClass parent_class;
};

GType ws_texture_get_type (void);


/* Possible GL API */
void      ws_window_setup_gl              (WsWindow    *window);
void      ws_window_gl_begin              (WsWindow    *window);
void      ws_window_gl_end                (WsWindow    *window);
void      ws_window_gl_swap_buffers       (WsWindow    *window);

/* Maybe also "get texture from drawable/pixmap" */

void enable_tfp (void);

#endif
