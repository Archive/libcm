/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "state.h"

#include <GL/glu.h>

G_DEFINE_TYPE (CmState, cm_state, CM_TYPE_NODE);

static void
cm_state_finalize (GObject *object)
{
    GList *list;
    CmState *state = CM_STATE (object);

    /* free vertex funcs */
    g_queue_free (state->vertex_funcs);

    /* free covered regions */
    for (list = state->covered_regions->head; list; list = list->next)
	ws_region_destroy (list->data);
    g_queue_free (state->covered_regions);
    
    G_OBJECT_CLASS (cm_state_parent_class)->finalize (object);
}

static void
cm_state_class_init (CmStateClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    object_class->finalize = cm_state_finalize;
}

static void
cm_state_init (CmState *state)
{
    state->vertex_funcs = g_queue_new ();
    state->covered_regions = g_queue_new ();

    g_queue_push_head (state->covered_regions, ws_region_new ());
}

void
cm_state_push_vertex_func (CmState	   *state,
			   CmVertexFunc	    vertex_func,
			   gpointer	    data)
{
    g_return_if_fail (CM_IS_STATE (state));
    g_return_if_fail (vertex_func != NULL);
    
    g_queue_push_head (state->vertex_funcs, data);
    g_queue_push_head (state->vertex_funcs, vertex_func);
}

void
cm_state_pop_vertex_func (CmState *state)
{
    if (g_queue_is_empty (state->vertex_funcs))
    {
	g_warning ("cm_state_pop_vertex_func() called without corresponding "
		   "call to cm_state_push_vertex_func()");
	return;
    }

    g_queue_pop_head (state->vertex_funcs);
    g_queue_pop_head (state->vertex_funcs);
}

void
cm_state_set_vertex_state (CmState *state,
			   gdouble *x,
			   gdouble *y,
			   gdouble *z,
			   gdouble  u,
			   gdouble  v)
{
    GList *list;

    gdouble dummy1 = 0.0;
    gdouble dummy2 = 0.0;
    gdouble dummy3 = 0.0;

    if (!x)
	x = &dummy1;

    if (!y)
	y = &dummy2;

    if (!z)
	z = &dummy3;
    
    for (list = state->vertex_funcs->head; list != NULL; list = list->next->next)
    {
	CmVertexFunc func = list->data;
	gpointer     data = list->next->data;
	
	func (x, y, z, u, v, data);
    }
}

CmState *
cm_state_new (void)
{
    return g_object_new (CM_TYPE_STATE, NULL);
}

void
cm_state_disable_depth_buffer_update (CmState *state)
{
    if (state->disable_depth_buffer_count == 0)
	glDepthMask (GL_FALSE);
    
    state->disable_depth_buffer_count++;
}

void
cm_state_enable_depth_buffer_update (CmState *state)
{
    if (state->disable_depth_buffer_count == 0)
    {
	g_warning ("Can't enable depth buffer updates without first "
		   "disabling them (disable/enable must come in pairs");
	return;
    }

    state->disable_depth_buffer_count--;

    if (state->disable_depth_buffer_count == 0)
	glDepthMask (GL_TRUE);
}

gboolean
cm_state_depth_buffer_update_enabled (CmState *state)
{
    return state->disable_depth_buffer_count == 0;
}

void
cm_state_disable_color_buffer_update (CmState *state)
{
    if (state->disable_color_buffer_count == 0)
	glColorMask (GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    
    state->disable_color_buffer_count++;
}

void
cm_state_enable_color_buffer_update (CmState *state)
{
    if (state->disable_color_buffer_count == 0)
    {
	g_warning ("Can't enable color buffer updates without first "
		   "disabling them (disable/enable must come in pairs");
	return;
    }

    state->disable_color_buffer_count--;

    if (state->disable_color_buffer_count == 0)
	glColorMask (GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    
}

gboolean
cm_state_color_buffer_update_enabled (CmState *state)
{
    return state->disable_color_buffer_count == 0;
}

void
cm_state_push_covered_region (CmState *state)
{
    WsRegion *top = state->covered_regions->head->data;

    g_queue_push_head (state->covered_regions, ws_region_copy (top));
}

void
cm_state_add_covered_region (CmState *state,
			     WsRegion *region)
{
    WsRegion *top = g_queue_peek_head (state->covered_regions);

    ws_region_union (top, region);
}

void
cm_state_pop_covered_region (CmState *state)
{
    WsRegion *top = g_queue_pop_head (state->covered_regions);

    if (!top || g_queue_is_empty (state->covered_regions))
    {
	g_warning ("pop covered region without corresponding push\n");
	return;
    }

    ws_region_destroy (top);
}

const WsRegion *
cm_state_peek_covered_region (CmState *state)
{
    return g_queue_peek_head (state->covered_regions);
}

static void
get_viewport (WsRectangle *rect)
{
    int coords [4];
    
    glGetIntegerv (GL_VIEWPORT, coords);

    rect->x = coords[0];
    rect->y = coords[1];
    rect->width = coords[2];
    rect->height = coords[3];
}

void
cm_state_set_screen_coords (CmState *state)
{
    WsRectangle viewport;

    get_viewport (&viewport);

    gluOrtho2D (0, viewport.width, viewport.height, 0);
}

WsRegion *
cm_state_get_visible_region (CmState *state)
{
    WsRectangle viewport;
    WsRegion *region;

    get_viewport (&viewport);

    /* Hack to make the initial visible region include
     * areas outside the viewport, so that the grid of a
     * wobbled window doesn't cull those areas
     */
    viewport.x -= viewport.width;
    viewport.y -= viewport.height;
    viewport.height *= 3;
    viewport.width *= 3;
    
    region = ws_region_rectangle (&viewport);

    ws_region_subtract (region, cm_state_peek_covered_region(state));

    return region;
}
