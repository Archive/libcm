/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "node.h"

#define CM_TYPE_ROTATION            (cm_rotation_get_type ())
#define CM_ROTATION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CM_TYPE_ROTATION, CmRotation))
#define CM_ROTATION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CM_TYPE_ROTATION, CmRotationClass))
#define CM_IS_ROTATION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CM_TYPE_ROTATION))
#define CM_IS_ROTATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CM_TYPE_ROTATION))
#define CM_ROTATION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CM_TYPE_ROTATION, CmRotationClass))

typedef struct _CmRotation CmRotation;
typedef struct _CmRotationClass CmRotationClass;

struct _CmRotation
{
    CmNode parent_instance;
    
    CmNode *	child;
    
    gdouble	angle;
    gdouble	x;
    gdouble	y;
    gdouble	z;}
;

struct _CmRotationClass
{
    CmNodeClass parent_class;
};

GType cm_rotation_get_type (void);

CmRotation *cm_rotation_new (CmNode *child);
void	    cm_rotation_set_rotation (CmRotation *rotation,
				     gdouble	 angle,
				     gdouble	 x,
				     gdouble	 y,
				     gdouble     z);
