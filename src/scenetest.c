/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "node.h"

#include "ws.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>
#include <math.h>

#include "drawable-node.h"
#include "cube.h"
#include <string.h>
#include "clip.h"
#include "wobbler.h"
#include "stacker.h"
#include "cube.h"

#if 0
static void
draw_background (CmNode *node)
{
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f (0.2, 0.3, 0.8, g_random_double());
    
    glBegin (GL_QUADS);
    glVertex2f (0.0, 0.0);
    glVertex2f (0.0, 1.0);
    glVertex2f (1.0, 1.0);
    glVertex2f (1.0, 0.0);
    glEnd ();
}
#endif

#if 0
static CmNode *
create_background ()
{
    CmNode *node = g_new0 (CmNode, 1);
    
    node->render = draw_background;
    
    return node;
}
#endif

#if 0
static void
draw_coordinate_system (CmNode *node)
{
    int i;
    
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f (1.0, 1.0, 1.0, 1.0);
    
    glBegin (GL_LINES);
    
    glVertex2f (0.0, 0.0);
    glVertex2f (1.0, 0.0);
    
    glVertex2f (0.0, 0.0);
    glVertex2f (0.0, 1.0);
    
    glVertex2f (0.0, 0.0);
    glVertex3f (0.0, 0.0, 1.0);
    
    glColor4f (0.0, 1.0, 0.0, 0.8);
    
    for (i = -10; i < 10; ++i)
    {
	double x = i / 10.0;
	double z = i / 10.0;
	
	glVertex3f (x, 0.0, -1.0);
	glVertex3f (x, 0.0, 1.0);
	
	glVertex3f (-1.0, 0.0, z);
	glVertex3f (1.0, 0.0, z);
    }
    
    glEnd ();
}
#endif

#if 0
static CmNode *
create_coordinate_system ()
{
    CmNode *node = g_new0 (CmNode, 1);
    
    node->render = draw_coordinate_system;
    
    return node;
}
#endif

#if 0
static gdouble cube_time;
#endif

#if 0
static void
draw_cube (CmNode *node)
{
    double wt = 1.0;
    double ht = 1.0;
    
    glBegin (GL_QUADS);
    glColor3f (1.0, 0.0, 0.0);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (0.0f, 0.0f, 0.0f);
    glTexCoord2f (  wt, 0.0f); glVertex3f (1.0f, 0.0f, 0.0f);
    glTexCoord2f (  wt,   ht); glVertex3f (1.0f, 1.0f, 0.0f);
    glTexCoord2f (0.0f,   ht); glVertex3f (0.0f, 1.0f, 0.0f);
    
    glColor3f (0.0, 1.0, 0.0);
    glTexCoord2f (  wt, 0.0f); glVertex3f (0.0f, 0.0f, -1.0f);
    glTexCoord2f (  wt,   ht); glVertex3f (0.0f, 1.0f, -1.0f);
    glTexCoord2f (0.0f,   ht); glVertex3f (1.0f, 1.0f, -1.0f);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (1.0f, 0.0f, -1.0f);
    
    glColor3f (0.0, 0.0, 1.0);
    glTexCoord2f (0.0f,   ht); glVertex3f (0.0f, 1.0f, -1.0f);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (0.0f, 1.0f,  0.0f);
    glTexCoord2f (  wt, 0.0f); glVertex3f (1.0f, 1.0f,  0.0f);
    glTexCoord2f (  wt,   ht); glVertex3f (1.0f, 1.0f, -1.0f);
    
    glColor3f (1.0, 1.0, 0.0);
    glTexCoord2f (  wt,   ht); glVertex3f (0.0f, 0.0f, -1.0f);
    glTexCoord2f (0.0f,   ht); glVertex3f (1.0f, 0.0f, -1.0f);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (1.0f, 0.0f,  0.0f);
    glTexCoord2f (  wt, 0.0f); glVertex3f (0.0f, 0.0f,  0.0f);
    
    glColor3f (0.0, 1.0, 1.0);
    glTexCoord2f (  wt, 0.0f); glVertex3f (1.0f, 0.0f, -1.0f);
    glTexCoord2f (  wt,   ht); glVertex3f (1.0f, 1.0f, -1.0f);
    glTexCoord2f (0.0f,   ht); glVertex3f (1.0f, 1.0f,  0.0f);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (1.0f, 0.0f,  0.0f);
    
    glColor3f (1.0, 0.0, 1.0);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (0.0f, 0.0f, -1.0f);
    glTexCoord2f (  wt, 0.0f); glVertex3f (0.0f, 0.0f,  0.0f);
    glTexCoord2f (  wt,   ht); glVertex3f (0.0f, 1.0f,  0.0f);
    glTexCoord2f (0.0f,   ht); glVertex3f (0.0f, 1.0f, -1.0f);
    
    glEnd ();
}
#endif

static GTimer *timer;
#if 0
static Node *coord;
static Node *cube;
#endif

typedef struct PaintInfo
{
    WsWindow *gl_window;
    GTimer *timer;
    CmNode *scene;
} PaintInfo;

#if 0
static GList *drawables;
#endif

static gboolean
do_paint (gpointer data)
{
    PaintInfo *info = data;
    
    static double xa = 0.0;
    static double ya = 0.0;
    static double za = 0.0;
    
    double f = 1.0;
    
    gdouble time = g_timer_elapsed (timer, NULL);
    
    xa = time * 90 * f;
    ya = time * 45 * f;
    za = time * 30 * f;
    
    ws_window_raise (info->gl_window);
    
    glClearColor (0.5, 0.5, 0.2, 1.0);
    glClear (GL_COLOR_BUFFER_BIT);
    glClear (GL_DEPTH_BUFFER_BIT);
    
    glEnable (GL_DEPTH_TEST);
    
    /* Set up default coordinate system */
    glMatrixMode (GL_MODELVIEW);
    
    glLoadIdentity ();
    glTranslatef (-1.0, -1.0, 0.0);
    glScalef (2.0, 2.0, -2.0);
    
    glTranslatef (0.0, 0.0, 1.2075);
    
#if 0
    glRotatef (xa, 1.0, 0.0, 0.0);
    glRotatef (ya, 0.0, 1.0, 0.0);
    glRotatef (za, 0.0, 0.0, 1.0);
#endif
    
    cm_node_render (info->scene, NULL);
    
    
#if 0
    /* draw cube a little further back */
    double f = 0.5;
    
    xa = time * 90 * f;
    ya = time * 45 * f;
    za = time * 30 * f;
    
    /* Move the visual coordinate system a little back */
    glTranslatef (0.0, 0.0, sin (time));
    
    glRotatef (10 * time, 0.0, 1.0, 0.0);
    glRotatef (2 * time, 0.0, 0.0, 1.0);
    
    glPushMatrix();
    
    glRotatef (xa, 1.0, 0.0, 0.0);
    glRotatef (ya, 0.0, 1.0, 0.0);
    glRotatef (za, 0.0, 0.0, 1.0);
    
    glEnable (GL_DEPTH_TEST);
    
    cube_time = 30 * time;
    cube->render(cube);
    
    glPopMatrix();
    
#if 0
    glDisable (GL_DEPTH_TEST);
#endif
    
    coord->render(coord);
#endif
    
    ws_window_gl_swap_buffers (info->gl_window);
    
    ws_display_flush (ws_drawable_get_display ((WsDrawable *)info->gl_window));
    
    return TRUE;
}

#if 0
static gboolean
do_quit (gpointer data)
{
    GMainLoop *loop = data;
    
    g_main_loop_quit (loop);
    
    return FALSE;
}
#endif

#if 0
static WsWindow *
get_desktop_window (WsWindow *root)
{
    WsWindow *result = NULL;
    GList *toplevels = ws_window_query_subwindows (root);
    GList *list;
    
    for (list = toplevels; list != NULL; list = list->next)
    {
	WsWindow *window = list->data;
	WsRectangle rect;
	
	ws_drawable_query_geometry ((WsDrawable *)window, &rect);
	
	if (!ws_window_query_input_only (window))
	{
	    if (ws_window_query_viewable (window))
	    {
		char *title = ws_window_query_title (window);
		
		if (title && strcmp (title, "Desktop") == 0)
		{
		    result = window;
		    break;
		}
	    }
	}
    }
    
    return result;
}
#endif

#if 0
static void
add_windows (WsWindow *root, CubeNode *cube_node)
{
    WsWindow *result = NULL;
    GList *list;
    CubeNode *cube2;
    GList *toplevels = ws_window_query_subwindows (root);
    
    face = 0;
    for (list = toplevels; list != NULL; list = list->next)
    {
	WsWindow *window = list->data;
	WsRectangle rect;
	
	ws_drawable_query_geometry (window, &rect);
	
	if (window != gl_window && !ws_window_query_input_only (window))
	{
	    if (ws_window_query_viewable (window))
	    {
		g_print ("title %s\n", ws_window_query_title (window));
		cube_set_face (cube_node, face++,
			       drawable_node_new (window));
	    }
	    
	    if (face == 6)
		break;
#if 0
	    if (face == 2)
		break;
#endif
	}
    }
}
#endif

static gboolean
compositable (WsWindow *window)
{
    if (ws_window_query_viewable (window) &&
	!ws_window_query_input_only (window))
    {
	return TRUE;
    }
    
    return FALSE;
    
}

#if 0
static WsWindow *
find_nautilus_window (WsWindow *root)
{
    GList *list;
    GList *toplevels = ws_window_query_subwindows (root);
    
    for (list = toplevels; list != NULL; list = list->next)
    {
	WsWindow *window = list->data;
	char *title;
	
	title = ws_window_query_title (window);
	
	if (compositable (window) && strcmp (title, "Desktop") == 0)
	    return window;
    }
    
    return NULL;
}
#endif

static CmNode *
create_desktop (WsWindow *root, PaintInfo *info)
{
    CmStacker *stacker;
    GList *toplevels = ws_window_query_subwindows (root);
    GList *list;
    int f;
    
    stacker = cm_stacker_new ();
    
    f = 0;
    for (list = toplevels; list != NULL; list = list->next)
    {
	WsWindow *window = list->data;
	
	if (compositable (window) && window != info->gl_window)
	{
	    WsRectangle rect;
	    CmDrawableNode *drawable;

	    ws_drawable_query_geometry (WS_DRAWABLE (window), &rect);

	    cm_drawable_node_new ((WsDrawable *)window, &rect);
	    
	    f++;
	    g_print ("%d\n", f);
	    
	    cm_stacker_add_child (stacker, (CmNode *)drawable);
	}
    }
    
    return (CmNode *)stacker;
}

static void
setup_for_compositing (WsScreen *screen, PaintInfo *info)
{
    info->gl_window = ws_screen_get_gl_window (screen);
    
    /* redirect all toplevels */
    ws_window_redirect_subwindows (ws_screen_get_root_window (screen));
    
    /* map and unredirect the gl window */
    ws_window_set_override_redirect (info->gl_window, TRUE);
    ws_window_map (info->gl_window);
    ws_window_unredirect (info->gl_window);
    
    ws_display_sync (ws_drawable_get_display ((WsDrawable *)info->gl_window));
    
    /* quit after a while */
    
    glMatrixMode (GL_MODELVIEW);
    
    glTranslatef (-1.0, -1.0, 0.0);
    
    glMatrixMode (GL_PROJECTION);
    gluPerspective( 45.0f, 1.0, 0.1f, 10.0f );
}

int
main ()
{
    g_type_init ();
    
    GMainLoop *loop = g_main_loop_new (NULL, 0);
    WsDisplay *display = ws_display_new (NULL);
    WsWindow *root = ws_screen_get_root_window (ws_display_get_default_screen (display));
    PaintInfo info;
    CmNode *desktop;
    CmNode *clipper;
    CmCube *cube;

    ws_display_init_composite (display);
    
#if 0
    g_print ("huh\n");
#endif
    
    setup_for_compositing (ws_display_get_default_screen (display), &info);
    
    desktop = create_desktop (root, &info);
    
    cube = cm_cube_new ();
    
#if 0
    clipper = clipper_node_new (0.25, 0.25, 0.7, 0.7, desktop);
#endif

    clipper = CM_NODE (wobbler_node_new (desktop));
    
    cm_cube_set_face (cube, 0, clipper);
    cm_cube_set_face (cube, 1, clipper);
    cm_cube_set_face (cube, 2, clipper);
    cm_cube_set_face (cube, 3, clipper);
    cm_cube_set_face (cube, 4, clipper);
    cm_cube_set_face (cube, 5, clipper);

    info.scene = CM_NODE (cube);
    
    info.scene = clipper;
    
    g_idle_add (do_paint, &info);
    
    timer = g_timer_new ();
    
    g_main_loop_run (loop);
    
    return 0;
}
