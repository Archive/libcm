/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include <string.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#include <X11/extensions/shape.h>
#include <X11/Xatom.h>

#include <glib-object.h>

#include "ws.h"
#include "wsint.h"

G_DEFINE_TYPE (WsWindow, ws_window, WS_TYPE_DRAWABLE);

static WsFormat ws_window_get_format (WsDrawable *drawable);

enum {
    CONFIGURE_EVENT,
    SELECTION_CLEAR_EVENT,
    N_SIGNALS
};

static guint signals[N_SIGNALS] = { 0 };

static void
ws_window_finalize (GObject *object)
{
    if (!WS_RESOURCE (object)->foreign)
    {
	XDestroyWindow (WS_RESOURCE_XDISPLAY (object),
			WS_RESOURCE_XID (object));
    }
    
    G_OBJECT_CLASS (ws_window_parent_class)->finalize (object);
}

static void
ws_window_class_init (WsWindowClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    WsDrawableClass *drawable_class = WS_DRAWABLE_CLASS (class);
    
    object_class->finalize = ws_window_finalize;
    drawable_class->get_format = ws_window_get_format;
    
    signals[CONFIGURE_EVENT] =
	g_signal_new ("configure_event",
		      G_OBJECT_CLASS_TYPE (object_class),
		      G_SIGNAL_RUN_LAST,
		      0,
		      NULL, NULL,
		      g_cclosure_marshal_VOID__POINTER,
		      G_TYPE_NONE, 1,
		      G_TYPE_POINTER);
    
    signals[SELECTION_CLEAR_EVENT] =
	g_signal_new ("selection_clear_event",
		      G_OBJECT_CLASS_TYPE (object_class),
		      G_SIGNAL_RUN_LAST,
		      0,
		      NULL, NULL,
		      g_cclosure_marshal_VOID__POINTER,
		      G_TYPE_NONE, 1,
		      G_TYPE_POINTER);
}

static void
ws_window_init (WsWindow *window)
{
    
}

static WsFormat
ws_window_get_format (WsDrawable *drawable)
{
    XWindowAttributes attrs;
    Display *xdisplay = WS_RESOURCE_XDISPLAY (drawable);
    Window xwindow = WS_RESOURCE_XID (drawable);
    WsWindow *window = WS_WINDOW (drawable);
    
    if (window->has_format)
	return window->format;
    
    if (!XGetWindowAttributes (xdisplay, xwindow, &attrs))
	return WS_FORMAT_UNKNOWN;
    
    if (attrs.class == InputOnly)
    {
	window->format = WS_FORMAT_INPUT_ONLY;
    }
    else if (attrs.depth == 16 &&
	     attrs.visual->red_mask == 0xf800 &&
	     attrs.visual->green_mask == 0x7e0 &&
	     attrs.visual->blue_mask == 0x1f)
    {
	window->format = WS_FORMAT_RGB_16;
    }
    else if (attrs.depth == 24 &&
	     attrs.visual->red_mask == 0xff0000 &&
	     attrs.visual->green_mask == 0xff00 &&
	     attrs.visual->blue_mask == 0xff)
    {
	window->format = WS_FORMAT_RGB_24;
    }
    else if (attrs.depth == 32 &&
	     attrs.visual->red_mask == 0xff0000 &&
	     attrs.visual->green_mask == 0xff00 &&
	     attrs.visual->blue_mask == 0xff)
    {
	window->format = WS_FORMAT_ARGB_32;
    }
    else
    {
	g_warning ("Unknown visual format depth=%d, r=%#lx/g=%#lx/b=%#lx",
		   attrs.depth, attrs.visual->red_mask,
		   attrs.visual->green_mask, attrs.visual->blue_mask);
	
	window->format = WS_FORMAT_UNKNOWN;
    }
    
    window->has_format = TRUE;
    
    return window->format;
}

static WsWindow *
window_new (WsDisplay *display,
	    Window xwindow,
	    gboolean foreign)
{
    g_assert (!g_hash_table_lookup (display->xresources, (gpointer)xwindow));
    
    WsWindow *window = g_object_new (WS_TYPE_WINDOW,
				     "display", display,
				     "xid", xwindow,
				     "foreign", foreign,
				     NULL);
    
    g_assert (xwindow == WS_RESOURCE_XID (window));
    
    ws_display_begin_error_trap (display);
    
    XSelectInput (display->xdisplay, xwindow,
		  StructureNotifyMask);
    
    ws_display_end_error_trap (display);
    
    return window;
}

WsWindow *
ws_window_new (WsWindow *parent)
{
    Window xwindow;
    WsDisplay *display = WS_RESOURCE (parent)->display;
    
    xwindow = XCreateSimpleWindow (WS_RESOURCE_XDISPLAY (parent),
				   WS_RESOURCE_XID (parent),
				   0, 0, 1, 1, 0, 0, 0);
    
    return window_new (display, xwindow, FALSE);
}

WsWindow *
_ws_window_ensure (WsDisplay *display,
		   Window  xwindow,
		   gboolean foreign)
{
    WsWindow *window;
    
    if (!xwindow)
	return NULL;
    
    window = g_hash_table_lookup (display->xresources, (gpointer)xwindow);
    
    if (!window)
	window = window_new (display, xwindow, foreign);
    
    return window;
}

void
ws_window_set_input_shape (WsWindow       *window,
			   WsServerRegion *shape)
{
    Display *xdisplay = WS_RESOURCE_XDISPLAY (window);
    
    XFixesSetWindowShapeRegion (xdisplay,
				WS_RESOURCE_XID (window),
				ShapeInput,
				0, 0, WS_RESOURCE_XID (shape));
}

/* Create a window and make it the active GL window
 * The right way forward here is most likely to have a new
 * subclass "GlWindow" with methods
 *
 *	   begin()				// make it active if it isn't
 *	   end()				// 
 *         swap_buffers();			// 
 *
 */
void
ws_window_redirect_subwindows (WsWindow *window)
{
    g_return_if_fail (WS_RESOURCE (window)->display->composite.available);
    
    XCompositeRedirectSubwindows (
	WS_RESOURCE_XDISPLAY (window), WS_RESOURCE_XID (window),
	CompositeRedirectManual);
}

void
ws_window_unredirect_subwindows (WsWindow *window)
{
    g_return_if_fail (WS_RESOURCE (window)->display->composite.available);
    
    XCompositeUnredirectSubwindows (
	WS_RESOURCE_XDISPLAY (window), WS_RESOURCE_XID (window),
	CompositeRedirectManual);
}

void
ws_window_map (WsWindow *window)
{
    Display *xdisplay;
    
    g_return_if_fail (window != NULL);
    
    xdisplay = WS_RESOURCE_XDISPLAY (window);
    
    XMapWindow (xdisplay, WS_RESOURCE_XID (window));
}

void
ws_window_unmap (WsWindow *window)
{
    g_return_if_fail (window != NULL);
    
    XUnmapWindow (WS_RESOURCE_XDISPLAY (window), WS_RESOURCE_XID (window));
}

void
ws_window_raise (WsWindow *window)
{
    WsScreen *screen;
    
    g_return_if_fail (window != NULL);
    
    screen = ws_drawable_query_screen (WS_DRAWABLE (window));
    
    if (window == screen->overlay_window)
	return;
    
    XRaiseWindow (WS_RESOURCE_XDISPLAY (window), WS_RESOURCE_XID (window));
}

void
ws_window_lower (WsWindow *window)
{
    g_return_if_fail (window != NULL);
    
    XLowerWindow (WS_RESOURCE_XDISPLAY (window), WS_RESOURCE_XID (window));
}

void
ws_window_gl_swap_buffers (WsWindow *window)
{
    glXSwapBuffers (WS_RESOURCE_XDISPLAY (window), WS_RESOURCE_XID (window));
}

void
ws_window_gl_finish (WsWindow *window)
{
    glFinish ();
}

void
ws_window_unredirect (WsWindow *window)
{
    WsScreen *screen = ws_drawable_query_screen (WS_DRAWABLE (window));
    
    if (screen->overlay_window == window)
	return;
    
    XCompositeUnredirectWindow (
	WS_RESOURCE_XDISPLAY (window),
	WS_RESOURCE_XID (window), CompositeRedirectManual);
}

void
ws_window_set_override_redirect (WsWindow *window, gboolean override_redirect)
{
    XSetWindowAttributes attrs;
    
    attrs.override_redirect = !!override_redirect;
    
    XChangeWindowAttributes (WS_RESOURCE_XDISPLAY (window), WS_RESOURCE_XID (window),
			     CWOverrideRedirect, &attrs);
}

gboolean
ws_window_query_input_only (WsWindow *window)
{
    WsFormat format = ws_drawable_get_format (WS_DRAWABLE (window));
    
    return format == WS_FORMAT_INPUT_ONLY;
}

gboolean
ws_window_query_mapped (WsWindow *window)
{
    XWindowAttributes attrs;
    
    if (!XGetWindowAttributes (WS_RESOURCE_XDISPLAY (window), WS_RESOURCE_XID (window), &attrs))
	return FALSE;
    
    return attrs.map_state == IsUnviewable || attrs.map_state == IsViewable;
}

gboolean
ws_window_query_override_redirect   (WsWindow       *window)
{
    XWindowAttributes attrs;

    if (!XGetWindowAttributes (WS_RESOURCE_XDISPLAY (window),
			       WS_RESOURCE_XID (window), &attrs))
	return FALSE;

    return attrs.override_redirect;
}

gboolean
ws_window_query_viewable (WsWindow *window)
{
    XWindowAttributes attrs;
    
    if (!XGetWindowAttributes (WS_RESOURCE_XDISPLAY (window), WS_RESOURCE_XID (window), &attrs))
	return FALSE;
    
    return attrs.map_state == IsViewable;
}

GList *
ws_window_query_subwindows (WsWindow *window)
{
    Window root, parent;		/* dummies */
    Window *children;
    guint n_children;
    int i;
    GList *result = NULL;
    
    if (XQueryTree (WS_RESOURCE_XDISPLAY (window),
		    WS_RESOURCE_XID (window), &root, &parent,
		    &children, &n_children))
    {
	for (i = 0; i < n_children; ++i)
	{
	    WsWindow *child = _ws_window_ensure (WS_RESOURCE (window)->display,
						 children[i], TRUE);
	    
	    result = g_list_prepend (result, child);
	}
	
	XFree (children);
    }
    
    return result;
}

/* FIXME: This function just uses XFetchName - it should do the
 * _NET_WM stuff. If this function should exist at all, which maybe it
 * shouldn't
 */

gchar *
ws_window_query_title (WsWindow *window)
{
    char *result = NULL;
    char *name;
    
    if (XFetchName (WS_RESOURCE_XDISPLAY (window),
		    WS_RESOURCE_XID (window), &name))
    {
	result = g_strdup (name);
	XFree (name);
    }
    else
    {
	result = g_strdup ("No title");
    }
    
    return result;
}

WsWindow *
ws_window_lookup (WsDisplay *display,
		  Window     xwindow)
{
    return _ws_window_ensure (display, xwindow, TRUE);
}


WsPixmap *
ws_window_name_pixmap (WsWindow *window)
{
    Pixmap xpixmap = XCompositeNameWindowPixmap (WS_RESOURCE_XDISPLAY (window),
						 WS_RESOURCE_XID (window));
    
    return _ws_pixmap_ensure (WS_RESOURCE (window)->display,
			      xpixmap,
			      ws_drawable_get_format (WS_DRAWABLE (window)), FALSE);
}

void
_ws_window_process_event (WsWindow   *window,
			  XEvent     *xevent)
{
    WsDisplay *display = WS_RESOURCE (window)->display;
    
    g_object_ref (window);
    
    if (xevent->type == ConfigureNotify)
    {
	XConfigureEvent	      *xconfigure = (XConfigureEvent *)xevent;
	WsConfigureEvent       event;
	
	event.window = window;
	event.x = xconfigure->x;
	event.y = xconfigure->y;
	event.width = xconfigure->width;
	event.height = xconfigure->height;
	event.border_width = xconfigure->border_width;
	event.above_this = _ws_window_ensure (display, xconfigure->above, TRUE);
	event.override_redirect = xconfigure->override_redirect;
	
	g_signal_emit (window, signals[CONFIGURE_EVENT], 0, &event);
    }
    else if (xevent->type == SelectionClear)
    {
	XSelectionClearEvent   *xselectionclear =
	    (XSelectionClearEvent *)xevent;
	WsSelectionClearEvent	event;
	
	event.window = window;
	event.selection =
	    _ws_display_get_atom_name (display, xselectionclear->selection);
	
	g_signal_emit (window, signals[SELECTION_CLEAR_EVENT], 0, &event);
	
	g_free ((gchar *)event.selection);
    }
    
    g_object_unref (window);
}

WsRegion *
ws_window_get_output_shape (WsWindow *window)
{
    XRectangle *rects;
    int ordering;
    int n_rects;
    WsRegion *region = ws_region_new ();
    
    rects = XShapeGetRectangles (
	WS_RESOURCE_XDISPLAY (window),
	WS_RESOURCE_XID (window), ShapeBounding,
	&n_rects, &ordering);
    
    if (rects)
    {
	int i;
	
	for (i = 0; i < n_rects; ++i)
 	{
	    WsRectangle rect;
	    
	    rect.x = rects[i].x;
	    rect.y = rects[i].y;
	    rect.width = rects[i].width;
	    rect.height = rects[i].height;
	    
	    ws_region_union_with_rect (region, &rect);
	}
	
	XFree (rects);
    }
    
    return region;
}

#if 0
typedef struct
{
    MetaDisplay   *display;
    Window         xwindow;
    Atom           xatom;
    Atom           type;
    int            format;
    unsigned long  n_items;
    unsigned long  bytes_after;
    unsigned char *prop;
} GetPropertyResults;
#endif

static guint8 *
get_property (WsWindow		 *window,
              const char	 *property_name,
	      int		  req_format,
              Atom                req_type,
	      guint		 *n_items)
{
    WsDisplay *display = WS_RESOURCE (window)->display;
    Window xwindow = WS_RESOURCE_XID (window);
    int ret;
    Atom type;
    int format;
    gulong n_items_internal;
    guchar *property = NULL;
    gulong bytes_after;
    guint8 *result;
    Atom xatom = _ws_display_intern_atom (display, property_name);
    
    g_return_val_if_fail (req_type != None, FALSE);
    g_return_val_if_fail (req_format == 8 ||
			  req_format == 16 ||
			  req_format == 32,
			  FALSE);
    
    ws_display_begin_error_trap (display);
    
    ret = XGetWindowProperty (display->xdisplay, xwindow, xatom, 0, G_MAXLONG, False,
			      req_type, &type, &format,
			      &n_items_internal, &bytes_after,
			      &property);
    
    
    ws_display_end_error_trap (display);
    
    if (ret != Success || type == None)
    {
	result = NULL;
	goto out;
    }
    
    if (format != req_format || type != req_type)
    {
	result = NULL;
	goto out;
    }
    
    result = g_memdup (property, n_items_internal * (format / 8));
    
    if (n_items)
	*n_items = n_items_internal;
    
out:
    if (property)
	XFree (property);
    return result;
}


/**
 * ws_window_get_property_atom_list:
 * @window: 
 * @property_name: 
 * 
 * Return value should be freed with g_strfreev()
 * 
 * Return value: 
 **/
gchar **
ws_window_get_property_atom_list (WsWindow *window,
				  const char *property_name)
{
    guint n_atoms;
    Atom *atoms;
    int i;
    char **result;
    WsDisplay *display = WS_RESOURCE (window)->display;
    
    atoms = (Atom *)get_property (window, property_name,
				  32, XA_ATOM, &n_atoms);
    
    if (!atoms)
	return NULL;
    
    result = g_new (gchar *, n_atoms + 1);
    
    for (i = 0; i < n_atoms; ++i)
    {
	Atom atom = atoms[i];
	
	result[i] = g_strdup (_ws_display_get_atom_name (display, atom));
    }
    
    result[i] = NULL;
    
    g_free (atoms);
    
    return result;
}

WsSyncCounter *
ws_window_get_property_sync_counter (WsWindow *window,
				     const char *property_name)
{
    guint n_items;
    guint8 *data;
    WsSyncCounter *result;
    XID xcounter;
    
    result = NULL;
    
    data = get_property (window, property_name, 32, XA_CARDINAL, &n_items);
    
    if (!data)
	return NULL;
    
    if (n_items != 1)
	goto out;
    
    memcpy (&xcounter, data, sizeof (xcounter));
    
    result = ws_sync_counter_ensure (WS_RESOURCE (window)->display, xcounter);
    
out:
    g_free (data);
    
    return result;
}

void
ws_window_own_selection (WsWindow *	window,
			 const char *	selection,
			 guint32	time)
{
    WsDisplay *display = WS_RESOURCE (window)->display;
    Atom atom = _ws_display_intern_atom (display, selection);
    
    XSetSelectionOwner (WS_RESOURCE_XDISPLAY (window), atom,
			WS_RESOURCE_XID (window),
			time);
}

void
ws_window_send_client_message (WsWindow	  *window,
			       const char *msg_type,
			       guint32	   msg[5])
{
    WsDisplay *display = WS_RESOURCE (window)->display;
    XClientMessageEvent ev;
    
    ev.type = ClientMessage;
    ev.window = WS_RESOURCE_XID (window);
    ev.message_type = _ws_display_intern_atom (display, msg_type);
    ev.format = 32;
    ev.data.l[0] = msg[0];
    ev.data.l[1] = msg[1];
    ev.data.l[2] = msg[2];
    ev.data.l[3] = msg[3];
    ev.data.l[4] = msg[4];
    
    XSendEvent (display->xdisplay,
		WS_RESOURCE_XID (window),
		False, 0, (XEvent*) &ev);
}

void
ws_window_send_configure_notify (WsWindow *window,
				 int       x,
				 int       y,
				 int       width,
				 int       height,
				 int       border_width,
				 gboolean  override_redirect)
{
  XEvent event;

  event.type = ConfigureNotify;
  event.xconfigure.display = WS_RESOURCE_XDISPLAY (window);
  event.xconfigure.event = WS_RESOURCE_XID (window);
  event.xconfigure.window = WS_RESOURCE_XID (window);
  event.xconfigure.x = x;
  event.xconfigure.y = y;
  event.xconfigure.width = width;
  event.xconfigure.height = height;
  event.xconfigure.border_width = border_width;
  event.xconfigure.above = None; /* FIXME */
  event.xconfigure.override_redirect = False;

  XSendEvent (WS_RESOURCE_XDISPLAY (window),
              WS_RESOURCE_XID (window),
              False, StructureNotifyMask, &event);
}
