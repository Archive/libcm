/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include <config.h>
#include "wsint.h"
#include <dlfcn.h>

G_DEFINE_TYPE (WsPixmap, ws_pixmap, WS_TYPE_DRAWABLE);

static WsFormat ws_pixmap_get_format (WsDrawable	     *drawable);
static GObject *ws_pixmap_constructor (GType                  type,
				       guint                  n_construct_properties,
				       GObjectConstructParam *construct_params);

typedef void (*GLXBindTexImageProc)    (Display       *display,
					GLXDrawable   drawable,
					int           buffer,
					int           *attribList);
typedef void (*GLXReleaseTexImageProc) (Display       *display,
					GLXDrawable   drawable,
					int           buffer);

static gpointer
get_func (const char *name)
{
    gpointer f = glXGetProcAddress ((const guchar *)name);

    if (!f)
	g_error ("%s is missing\n", name);

    return f;
}

static void
release_tex_image_ext (Display *dpy,
		       GLXDrawable drawable,
		       int buffer)
{
    static GLXReleaseTexImageProc f;

    if (!f)
	f = get_func ("glXReleaseTexImageEXT");

    f (dpy, drawable, buffer);
}

static void
bind_tex_image (Display     *dpy,
		GLXDrawable  drawable,
		int	     buffer,
		int	    *attribs)
{
    static GLXBindTexImageProc f;

    if (!f)
	f = get_func ("glXBindTexImageEXT");

    f (dpy, drawable, buffer, attribs);
}

static void
ws_pixmap_finalize (GObject *object)
{
    WsPixmap *pixmap = WS_PIXMAP (object);
    
    /* FIXME: Deal with foreign objects correctly */
    
    if (pixmap->texture)
    {
	release_tex_image_ext (WS_RESOURCE_XDISPLAY (pixmap),
			       pixmap->glx_pixmap,
			       GLX_FRONT_LEFT_EXT);
	
	glXDestroyGLXPixmap (WS_RESOURCE_XDISPLAY (pixmap),
			     pixmap->glx_pixmap);
	
	glDeleteTextures (1, &pixmap->texture);
    }
    
    XFreePixmap (WS_RESOURCE_XDISPLAY (object),
		 WS_RESOURCE_XID (object));
    
    G_OBJECT_CLASS (ws_pixmap_parent_class)->finalize (object);
}

static void
ws_pixmap_class_init (WsPixmapClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    WsDrawableClass *drawable_class = WS_DRAWABLE_CLASS (class);
    
    object_class->finalize = ws_pixmap_finalize;
    object_class->constructor = ws_pixmap_constructor;
    drawable_class->get_format = ws_pixmap_get_format;
}

static void
repair_pixmap (WsPixmap *pixmap)
{
    if (pixmap->texture)
    {
	/* FIXME: at this point we are just assuming that texture from
	 * drawable is available. At some point we should reinstate the
	 * XGetImage() code. At that point configure.ac needs to be
	 * changed to not report an error.
	 */
	glBindTexture (GL_TEXTURE_RECTANGLE_ARB, pixmap->texture);
	
	bind_tex_image (WS_RESOURCE_XDISPLAY (pixmap),
			pixmap->glx_pixmap,
			GLX_FRONT_LEFT_EXT, NULL);
    }
}	       

static void
on_damage (WsDisplay *display,
	   gpointer   data)
{
    WsPixmap *pixmap = data;
    
#if 0
    g_print ("processing damage on %lx\n", WS_RESOURCE_XID (pixmap));
#endif
    
    if (pixmap->do_updates)
	repair_pixmap (pixmap);
}

static WsFormat
ws_pixmap_get_format (WsDrawable *drawable)
{
    WsPixmap *pixmap = WS_PIXMAP (drawable);
    
    return pixmap->format;
}

static GObject *
ws_pixmap_constructor (GType		        type,
		       guint		        n_construct_properties,
		       GObjectConstructParam *construct_params)
{
    GObject *object;
    WsPixmap *pixmap;
    
    object = G_OBJECT_CLASS (ws_pixmap_parent_class)->constructor (
	type, n_construct_properties, construct_params);
    
    pixmap = WS_PIXMAP (object);
    
    /* FIXME: We are connecting to ourselves here. Consider having a signal
     * function do it instead
     */
    g_signal_connect (pixmap, "damage_notify_event", G_CALLBACK (on_damage), pixmap);
    
    return object;
}

static void
ws_pixmap_init (WsPixmap *pixmap)
{
    pixmap->do_updates = TRUE;
}

static WsPixmap *
pixmap_new (WsDisplay *display,
	    XID        xid,
	    WsFormat   format,
	    gboolean   foreign)
{
    WsPixmap *pixmap = g_object_new (WS_TYPE_PIXMAP,
				     "display", display,
				     "xid", xid,
				     "foreign", foreign,
				     NULL);
    
#if 0
    g_print ("allocating pixmap %lx\n", WS_RESOURCE_XID (pixmap));
#endif
    
    pixmap->format = format;
    
    g_assert (xid == WS_RESOURCE_XID (pixmap));
    
    return pixmap;
}

WsPixmap *
ws_pixmap_new (WsDrawable *drawable,
	       int	   width,
	       int	   height)
{
    Pixmap pixmap;
    Display *xdisplay;
    WsFormat format;
    
    g_return_val_if_fail (WS_IS_DRAWABLE (drawable), NULL);
    g_return_val_if_fail (
	ws_format_is_viewable (ws_drawable_get_format (drawable)), NULL);
    
    format = ws_drawable_get_format (drawable);
    
    xdisplay = WS_RESOURCE_XDISPLAY (drawable);
    
    pixmap = XCreatePixmap (xdisplay,
			    WS_RESOURCE_XID (drawable),
			    width, height, ws_format_get_depth (format));
    
    return pixmap_new (WS_RESOURCE (drawable)->display,
		       pixmap, format, FALSE);
}

WsPixmap *
_ws_pixmap_ensure (WsDisplay *display,
		   XID        xid,
		   WsFormat   format,
		   gboolean   foreign)
{
    WsPixmap *pixmap =
	g_hash_table_lookup (display->xresources, (gpointer)xid);

    if (!pixmap)
      pixmap = pixmap_new (display, xid, format, foreign);
    
    return pixmap;
}

static GLXPixmap
create_glx_pixmap (WsPixmap *pixmap)
{
    Display *xdisplay = WS_RESOURCE_XDISPLAY (pixmap);
    int screen_no = WS_DRAWABLE_XSCREEN_NO (pixmap);
    WsFormat format = ws_drawable_get_format (WS_DRAWABLE (pixmap));
    XVisualInfo *vis_infos;
    int n_infos;
    XVisualInfo template;
    GLXPixmap glx_pixmap;
    
    template.screen = screen_no;
    template.depth = ws_format_get_depth (format);
    template.class = TrueColor;
    
    ws_format_get_masks (format,
			 &template.red_mask,
			 &template.green_mask,
			 &template.blue_mask);
    
    vis_infos = XGetVisualInfo (xdisplay,
				VisualScreenMask	|
				VisualDepthMask		|
				VisualClassMask		|
				VisualRedMaskMask	|
				VisualGreenMaskMask	|
				VisualBlueMaskMask,
				&template, &n_infos);
    
    if (!vis_infos)
    {
	g_warning ("No XVisualInfos found - will likely crash");
	return None;
    }
    
    /* All of the XVisualInfos returned should be ok, so just pick
     * the first one.
     */
    glx_pixmap = glXCreateGLXPixmap (
	xdisplay, vis_infos, WS_RESOURCE_XID (pixmap));

    XFree (vis_infos);

    return glx_pixmap;
}

static gboolean use_tfp = FALSE;

void
enable_tfp (void)
{
  use_tfp = TRUE;
}

static GLuint
create_tfd_texture (WsPixmap *pixmap)
{
    WsRectangle geometry;
    GLuint texture;

    pixmap->glx_pixmap = create_glx_pixmap (pixmap);
    
    if (!pixmap->glx_pixmap)
    {
	g_warning ("Could not create a GLXpixmap for %lx\n",
		   WS_RESOURCE_XID (pixmap));
	return 0;
    }
    
    glGenTextures (1, &texture);
    
    ws_drawable_query_geometry (WS_DRAWABLE (pixmap), &geometry);
    
#if 0
    g_print ("creating texture %d\n", pixmap->texture);
    g_print ("size; %d %d (%d bytes)\n", geometry.width, geometry.height, geometry.width * geometry.height * 4);
#endif
    
    glBindTexture (GL_TEXTURE_RECTANGLE_ARB, texture);
    
    glTexParameteri (GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri (GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri (GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_R, GL_CLAMP);
    
#if 0
    /* XXX JX experimental stuffs */
    glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
    glTexEnvi (GL_TEXTURE_ENV, GL_COMBINE_ALPHA, GL_REPLACE);
    glTexEnvi (GL_TEXTURE_ENV, GL_SOURCE0_ALPHA, GL_CONSTANT_ARB);
    /* and then need to set a glColor somewhere... */
#endif

    /* FIXME: at this point we are just assuming that texture from
     * drawable is available. At some point we should reinstate the
     * XGetImage() code. At that point configure.ac needs to be
     * changed to not report an error.
     */
    
    if (!use_tfp)
      {
         /* This is the case where they don't support the extension.
          * Based on beryl-core/src/texture.c:417-496. */

         GC gc;
         XGCValues gcv;
         XImage *image = NULL;
         Pixmap temp_pix;
         WsFormat format;
         int width, height, x=0, y=0;

         format = ws_drawable_get_format (WS_DRAWABLE (pixmap));

         gcv.graphics_exposures = FALSE;
         gcv.subwindow_mode = IncludeInferiors;

         gc = XCreateGC(
             WS_RESOURCE_XDISPLAY (pixmap),
             pixmap->glx_pixmap,
             GCGraphicsExposures | GCSubwindowMode,
             &gcv);

         width = geometry.width;
         height = geometry.height;

         if (width<=0 || height<=0)
           /* Bail. */
           return texture;

         temp_pix = XCreatePixmap(
             WS_RESOURCE_XDISPLAY (pixmap),
             pixmap->glx_pixmap,
             width,
             height,
             ws_format_get_depth (format)
             );

         if (temp_pix == None) {
           g_warning ("Could not create pixmap for copy.\n");
           return texture;
         }

         XCopyArea (
             WS_RESOURCE_XDISPLAY (pixmap),
             pixmap->glx_pixmap,
             temp_pix,
             gc,
             x, y,
             width, height,
             0, 0
             );

         XSync (WS_RESOURCE_XDISPLAY (pixmap), FALSE);

         image = XGetImage (
             WS_RESOURCE_XDISPLAY (pixmap),
             temp_pix,
             0, 0,
             width, height,
             AllPlanes,
             ZPixmap
             );
           
         glTexSubImage2D (GL_TEXTURE_RECTANGLE_ARB,
             0,
             x, y,
             width, height,
             GL_BGRA,
#ifdef WORDS_BIGENDIAN
             GL_UNSIGNED_INT_8_8_8_8_REV,
#else
             GL_UNSIGNED_BYTE,
#endif
             image? image->data: NULL
             );

         XFreePixmap (WS_RESOURCE_XDISPLAY (pixmap), temp_pix);
         XFreeGC (WS_RESOURCE_XDISPLAY (pixmap), gc);

         if (image)
           XDestroyImage (image);
      }
    else
      {
        bind_tex_image (WS_RESOURCE_XDISPLAY (pixmap),
    		        pixmap->glx_pixmap,
		        GLX_FRONT_LEFT_EXT, NULL);
      }
    
#if 0
    g_print ("creating damage: %lx on %lx\n", pixmap->damage,
	     WS_RESOURCE_XID (pixmap));
#endif

    return texture;
}

GLuint
ws_pixmap_get_texture (WsPixmap  *pixmap)
{
    g_return_val_if_fail (WS_IS_PIXMAP (pixmap), 0);
    
    if (!pixmap->texture)
    {
	pixmap->texture = create_tfd_texture (pixmap);
	repair_pixmap (pixmap);
    }

    if (!pixmap->texture)
    {
	g_warning ("No texture created - maybe Texture From Pixmap "
		   "extension is not present?");
    }
    
    return pixmap->texture;
}

void
ws_pixmap_set_updates (WsPixmap *pixmap, gboolean updates)
{
    pixmap->do_updates = !!updates;
    
    if (pixmap->do_updates)
	repair_pixmap (pixmap);
}

gboolean
ws_pixmap_get_updates (WsPixmap *pixmap)
{
    return pixmap->do_updates;
}
