/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
/* Terminology
 *
 *     - create/destroy       - the X resource is created/destroyed
 *     - new/free/ref/unref   - the local proxy is created/destroyed
 *
 * Generally local proxies can exist even if the other resource doesn't
 * anymore. In that case they don't have an associated XID.
 *
 * And of course we are being async.
 *
 */

#include <X11/Xresource.h>
#include <X11/X.h>
#include <X11/Xutil.h>
#include <X11/extensions/Xcomposite.h>
#include <X11/extensions/Xdamage.h>
#include <X11/extensions/XTest.h>
#include <X11/extensions/sync.h>

#include <glib-object.h>

#include "ws.h"
#include "wsint.h"
#include "watch.h"

G_DEFINE_TYPE (WsDisplay, ws_display, G_TYPE_OBJECT);

static void pop_obsolete_handlers (WsDisplay *display, int serial);

/*
 * Window system object
 */
struct Trap
{
    int begin_serial;
    int end_serial;
    gboolean caught_something;
    
    Trap *next;
};

static void
ws_display_finalize (GObject *object)
{
    WsDisplay *display = WS_DISPLAY (object);
    
    g_hash_table_destroy (display->damage_table);
    
    G_OBJECT_CLASS (ws_display_parent_class)->finalize (object);
}

static void
ws_display_class_init (WsDisplayClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    object_class->finalize = ws_display_finalize;
}

static void
ws_display_init (WsDisplay *display)
{
    display->damage_table = g_hash_table_new_full (
	g_direct_hash, g_direct_equal, NULL, g_free);
}

typedef struct DamageInfo
{
    XID			damage;
    DamageCallback	cb;
    gpointer		data;
} DamageInfo;

/*
 * These two functions basically exist to work around freedesktop bug 5730
 */
void
_ws_display_register_damage (WsDisplay      *display,
			     XID             damage,
			     DamageCallback  cb,
			     gpointer        data)
{
    DamageInfo *info;
    
    g_return_if_fail (
	g_hash_table_lookup (display->damage_table, (gpointer)damage) == NULL);
    
    info = g_new (DamageInfo, 1);
    
    info->damage = damage;
    info->cb = cb;
    info->data = data;

    g_hash_table_insert (display->damage_table, (gpointer)damage, info);
}

void
_ws_display_unregister_damage (WsDisplay	 *display,
			       XID		  damage)
{
    DamageInfo *damage_info = g_hash_table_lookup (
	display->damage_table, (gpointer)damage);
    
    g_return_if_fail (damage_info != NULL);
    
    g_hash_table_remove (display->damage_table, (gpointer)damage);
}

static void
process_damage_event (WsDisplay		  *display,
		      XDamageNotifyEvent *damage_event)
{
    XID damage = damage_event->damage;
    DamageInfo *info =
	g_hash_table_lookup (display->damage_table, (gpointer)damage);
    
    if (info)
	info->cb (display, damage, info->data);
}

static void
process_event (WsDisplay *display,
	       XEvent    *xevent)
{
    pop_obsolete_handlers (display, xevent->xany.serial);
    
    if (display->damage.available &&
	xevent->type == display->damage.event_base + XDamageNotify)
    {
	XDamageNotifyEvent *de = (XDamageNotifyEvent *)xevent;
	
	process_damage_event (display, de);
    }
    else if (xevent->type == ConfigureNotify)
    {
	Window xwindow = ((XConfigureEvent *)xevent)->window;
	WsWindow *window = g_hash_table_lookup (
	    display->xresources, (gpointer)xwindow);
	
	if (window)
	    _ws_window_process_event (window, xevent);
    }
    else if (xevent->type == SelectionClear)
    {
	Window xwindow = ((XSelectionClearEvent *)xevent)->window;
	WsWindow *window = g_hash_table_lookup (
	    display->xresources, (gpointer)xwindow);

	if (window)
	    _ws_window_process_event (window, xevent);
    }
    else if (display->sync.available &&
	     xevent->type == display->sync.event_base + XSyncAlarmNotify)
    {
	XSyncAlarm xalarm = ((XSyncAlarmNotifyEvent *)xevent)->alarm;
	WsSyncAlarm *alarm = g_hash_table_lookup (
	    display->xresources, (gpointer)xalarm);

	if (alarm)
	    _ws_sync_alarm_process_event (alarm, xevent);
    }
}

static void
process_events (gpointer data)
{
    WsDisplay *display = data;
    Display *xdisplay = display->xdisplay;
    
    while (XPending (display->xdisplay))
    {
	XEvent event;
	
	XNextEvent (xdisplay, &event);

	process_event (display, &event);
    }
}

static ErrorHandlerFunc old_x_error_handler;
static GList *all_displays;

static void
pop_obsolete_handlers (WsDisplay *display,
		       int serial)
{
    Trap *new_top;

    new_top = display->traps;
    while (new_top != NULL &&
	   new_top->end_serial != -1 &&
	   new_top->end_serial <= serial)
    {
	new_top = new_top->next;
    }
    
    while (display->traps != new_top)
    {
	Trap *trap;
	
	trap = display->traps;
	display->traps = trap->next;
	
	g_free (trap);
    }
    
    if (!display->traps)
    {
	if (display->old_handler)
	    XSetErrorHandler (display->old_handler);
	display->old_handler = NULL;
    }
}

static void
process_error (WsDisplay *display,
	       XErrorEvent *error)
{
    Trap *trap;
    char buf[64];

    pop_obsolete_handlers (display, error->serial);
    
    for (trap = display->traps; trap; trap = trap->next)
    {
	if (trap->begin_serial <= error->serial &&
	    (trap->end_serial > error->serial || trap->end_serial == -1))
	{
	    trap->caught_something = TRUE;
	    return;
	}
    }
    
    XGetErrorText (display->xdisplay, error->error_code, buf, 63);  
    
    g_error ("Unexpected X error: %s serial %ld error_code "
	     "%d request_code %d minor_code %d)\n",
	     buf,
	     error->serial, 
	     error->error_code, 
	     error->request_code,
	     error->minor_code);
}

static int
xerror_handler (Display		*xdisplay,
		XErrorEvent	*error_event)
{
    GList *list;
    WsDisplay *error_display = NULL;
    
    for (list = all_displays; list != NULL; list = list->next)
    {
	WsDisplay *display = list->data;
	
	if (display->xdisplay == xdisplay)
	{
	    error_display = display;
	    break;
	}
    }
    
    if (error_display)
    {
	process_error (error_display, error_event);
    }
    else if (old_x_error_handler)
    {
	(* old_x_error_handler) (xdisplay, error_event);
    }
    
    return 42;
}

static WsDisplay *
create_display (Display *xdisplay)
{
    int i;
    WsDisplay *display = g_object_new (WS_TYPE_DISPLAY, NULL);
    
    display->xdisplay = xdisplay;
    display->n_screens = ScreenCount (xdisplay);
    display->screens = g_new0 (WsScreen *, display->n_screens);
    
    for (i = 0; i < display->n_screens; ++i)
	display->screens[i] = ws_display_get_screen_from_number (display, i);
    
    display->xresources = g_hash_table_new (g_direct_hash, g_direct_equal);
    
    display->composite.available = FALSE;
    display->damage.available = FALSE;
    display->test.available = FALSE;
    display->fixes.available = FALSE;
    display->glx.available = FALSE;
    display->sync.available = FALSE;

    return display;
}

WsDisplay *
ws_display_new (const char *dpy)
{
    Display *xdisplay = XOpenDisplay (dpy);
    WsDisplay *display;
    int fd;
    
    if (!xdisplay)
	return NULL;

    display = create_display (xdisplay);
    
    fd = ConnectionNumber (xdisplay);
    
    /* process events that are already read off the wire */
    process_events (display);
    
    fd_add_watch (fd, display);
    fd_set_read_callback (fd, process_events);
    fd_set_poll_callback (fd, process_events);
    
    if (!all_displays)
	old_x_error_handler = XSetErrorHandler (xerror_handler);
    
    all_displays = g_list_prepend (all_displays, display);
    
    return display;
}

WsDisplay *
ws_display_new_from_xdisplay (Display     *xdisplay)
{
    g_return_val_if_fail (xdisplay != NULL, NULL);
    
    return create_display (xdisplay);
}

void
ws_display_process_xevent (WsDisplay *display,
			   XEvent *xevent)
{
    g_return_if_fail (WS_IS_DISPLAY (display));
    
    process_event (display, xevent);
}

void
ws_display_process_xerror (WsDisplay   *display,
			  XErrorEvent *error)
{
    g_return_if_fail (WS_IS_DISPLAY (display));

    process_error (display, error);
}

void
ws_display_begin_error_trap  (WsDisplay *display)
{
    struct Trap *trap = g_new0 (Trap, 1);

    trap->begin_serial = NextRequest (display->xdisplay);
    trap->end_serial = -1;
    trap->next = display->traps;
    trap->caught_something = FALSE;
    
    display->traps = trap;

    display->old_handler = XSetErrorHandler (xerror_handler);
    
#if 0
    g_print ("beginning: %d (%p)\n", trap->begin_serial, display->xdisplay);
#endif
    g_assert (display->traps);
}

static Trap *
ws_display_end_error_trap_internal (WsDisplay *display)
{
    Trap *trap;

    /* Find first open trap */
    trap = display->traps;
    while (trap && trap->end_serial != -1)
	trap = trap->next;
    
    if (!trap)
    {
	g_warning ("ws_end_trap_errors() called without corresponding "
		   "ws_begin_trap_errors()");
	return NULL;
    }
    
    trap->end_serial = NextRequest (display->xdisplay);
#if 0
    g_print ("ending: %d \n", trap->end_serial);
#endif
    
    return trap;
}


void
ws_display_end_error_trap (WsDisplay *display)
{
    ws_display_end_error_trap_internal (display);
}

gboolean
ws_display_end_error_trap_with_return (WsDisplay *display)
{
    Trap *trap = ws_display_end_error_trap_internal (display);
    
    ws_display_sync (display);
    
    return trap->caught_something;
}

gboolean
ws_display_init_composite (WsDisplay *display)
{
    if (XCompositeQueryExtension (
	    display->xdisplay,
	    &display->composite.event_base,
	    &display->composite.error_base))
    {
	display->composite.available = TRUE;
	
	return TRUE;
    }
    
    return FALSE;
}

gboolean
ws_display_init_fixes (WsDisplay *display)
{
    if (XFixesQueryExtension (
	    display->xdisplay,
	    &display->composite.event_base,
	    &display->composite.error_base))
    {
	display->fixes.available = TRUE;
	
	return TRUE;
    }
    
    return FALSE;
}

gboolean
ws_display_init_damage (WsDisplay *display)
{
    if (XDamageQueryExtension (
	    display->xdisplay,
	    &display->damage.event_base,
	    &display->damage.error_base))
    {
	display->damage.available = TRUE;
	return TRUE;
    }
    
    return FALSE;
}

gboolean
ws_display_init_test (WsDisplay *display)
{
    int dummy1, dummy2;
    
    if (XTestQueryExtension (
	    display->xdisplay,
	    &display->test.event_base,
	    &display->test.error_base,
	    &dummy1, &dummy2))
    {
	display->test.available = TRUE;
	return TRUE;
    }
    
    return FALSE;
}

gboolean
ws_display_init_glx (WsDisplay *display)
{
    if (glXQueryExtension (display->xdisplay,
			   &display->glx.event_base,
			   &display->glx.error_base))
    {
	display->glx.available = TRUE;
	return TRUE;
    }
    
    return FALSE;
}

gboolean
ws_display_init_sync (WsDisplay *display)
{
    int dummy1, dummy2;
    
    XSyncInitialize (display->xdisplay, &dummy1, &dummy2);

    if (XSyncQueryExtension (display->xdisplay,
			     &display->sync.event_base,
			     &display->sync.error_base))
    {
	display->sync.available = TRUE;
	return TRUE;
    }

    return FALSE;
}

void
ws_display_flush (WsDisplay *display)
{
    g_return_if_fail (display != NULL);
    
    XFlush (display->xdisplay);
}

void
ws_display_sync (WsDisplay *display)
{
    g_return_if_fail (display != NULL);
    
    XSync (display->xdisplay, 0);
}

void
ws_display_set_synchronize (WsDisplay *display,
			    gboolean sync)
{
    g_return_if_fail (display != NULL);
    
    XSynchronize (display->xdisplay, sync);
}

WsWindow *
_ws_display_lookup_window (WsDisplay *display, XID xid)
{
    g_return_val_if_fail (display != NULL, NULL);
    
    return g_hash_table_lookup (display->xresources, (gpointer)xid);
}

void
ws_display_set_ignore_grabs (WsDisplay *display,
			     gboolean  ignore)
{
    g_return_if_fail (display->test.available);
    
    XTestGrabControl (display->xdisplay, ignore);
}

void
_ws_display_add_resource (WsDisplay *display, XID xid, WsResource *resource)
{
    g_hash_table_insert (display->xresources, (gpointer)xid, resource);
}

void
_ws_display_remove_resource (WsDisplay *display,
			     XID xid)
{
    g_hash_table_remove (display->xresources, (gpointer)xid);
}

WsScreen *
ws_display_get_default_screen (WsDisplay *display)
{
    Screen *xscreen;
    int screen_no;
    int i;
    
    g_return_val_if_fail (display != NULL, NULL);
    
    screen_no = DefaultScreen (display->xdisplay);
    xscreen = ScreenOfDisplay (display->xdisplay, screen_no);
    
    for (i = 0; i < display->n_screens; ++i)
    {
	if (display->screens[i]->xscreen == xscreen)
	    return display->screens[i];
    }
    
    return NULL;
}

WsScreen *
ws_display_get_screen_from_number (WsDisplay *display,
				   int number)
{
    if (!display->screens[number])
    {
	display->screens[number] = _ws_screen_new (
	    display, ScreenOfDisplay (display->xdisplay, number));
    }
    
    return display->screens[number];
    
}

WsScreen *
_ws_display_lookup_screen (WsDisplay    *display,
			   Screen       *xscreen)
{
    int i;
    
    for (i = 0; i < display->n_screens; ++i)
    {
	if (display->screens[i]->xscreen == xscreen)
	    return display->screens[i];
    }
    
    return NULL;
}

void
ws_display_grab (WsDisplay *display)
{
    XGrabServer (display->xdisplay);
}

void
ws_display_ungrab (WsDisplay *display)
{
    XUngrabServer (display->xdisplay);
}

gchar *
_ws_display_get_atom_name (WsDisplay *display,
			   Atom	      atom)
{
    /* FIXME: at some point we want an atom cache */
    gchar *xname = XGetAtomName (display->xdisplay, atom);
    gchar *result = g_strdup (xname);

    XFree (xname);

    return result;
}

Atom
_ws_display_intern_atom (WsDisplay *display,
			 const char *string)
{
    /* FIXME: at some point we want an atom cache -
     * and do some prefetching probably
     */
    return XInternAtom (display->xdisplay, string, False);
}

#if 0
typedef enum
{
    CORE,
    DAMAGE,
    SYNC
} Extension;

typedef struct
{
    guint	xtype;
    const char *name;
    int		drawable_offset;
    int		extenstion_event_offset;
} EventTranslation;

const EventTranslation translation_table[] =
{
    { KeyPress,   "KeyPress",   G_STRUCT_OFFSET (XKeyPressedEvent, window) } ,
    { KeyRelease, "KeyRelease", G_STRUCT_OFFSET (XKeyReleasedEvent, window) } ,
    { ButtonPress, "ButtonPress", G_STRUCT_OFFSET (XButtonEvent, window) } ,
    { ButtonRelease, "ButtonRelease", G_STRUCT_OFFSET (XButtonEvent, window) } ,
    { MotionNotify, "MotionNotify", G_STRUCT_OFFSET (XMotionEvent, window) } ,
    { EnterNotify, "EnterNotify", G_STRUCT_OFFSET (XEnterWindowEvent, window) } ,
    { LeaveNotify, "LeaveNotify", G_STRUCT_OFFSET (XLeaveWindowEvent, window) } ,
    { FocusIn, "FocusIn", G_STRUCT_OFFSET (XFocusInEvent, window) } ,
    { FocusOut, "FocusOut", G_STRUCT_OFFSET (XFocusOutEvent, window) } ,
    { KeymapNotify, "KeymapNotify", G_STRUCT_OFFSET (XKeymapEvent, window) } ,
    { Expose,           "Expose", G_STRUCT_OFFSET (XExposeEvent, window) } ,
    { GraphicsExpose,   "GraphicsExpose", G_STRUCT_OFFSET (XGraphicsExposeEvent, drawable) } ,
    { NoExpose,         "NoExpose", G_STRUCT_OFFSET (XNoExposeEvent, drawable) } ,
    { VisibilityNotify, "VisibilityNotify", G_STRUCT_OFFSET (XVisibilityEvent, window) } ,
    { CreateNotify, "CreateNotify", G_STRUCT_OFFSET (XCreateWindowEvent, window) } ,
    { DestroyNotify, "DestroyNotify", G_STRUCT_OFFSET (XDestroyWindowEvent, window) } ,
    { UnmapNotify, "UnmapNotify", G_STRUCT_OFFSET (XUnmapEvent, window) } ,
    { MapNotify, "MapNotify", G_STRUCT_OFFSET (XMapEvent, window) } ,
    { MapRequest, "MapRequest", G_STRUCT_OFFSET (XMapRequestEvent, window) } ,
    { ReparentNotify, "ReparentNotify", G_STRUCT_OFFSET (XReparentEvent, window) } ,
    { ConfigureNotify, "ConfigureNotify", G_STRUCT_OFFSET (XConfigureEvent, window) } ,
    { ConfigureRequest, "ConfigureRequest", G_STRUCT_OFFSET (XConfigureRequestEvent, window) } ,
    { GravityNotify, "GravityNotify", G_STRUCT_OFFSET (XGravityEvent, window) } ,
    { ResizeRequest, "ResizeRequest", G_STRUCT_OFFSET (XResizeRequestEvent, window) } ,
    { CirculateNotify, "CirculateNotify", G_STRUCT_OFFSET (XCirculateEvent, window) } ,
    { CirculateRequest, "CirculateRequest", G_STRUCT_OFFSET (XCirculateRequestEvent, window) } ,
    { PropertyNotify, "PropertyNotify", G_STRUCT_OFFSET (XPropertyEvent, window) } ,
    { SelectionClear, "SelectionClear", G_STRUCT_OFFSET (XSelectionClearEvent, window) } ,
    { SelectionRequest, "SelectionRequest", G_STRUCT_OFFSET (XSelectionRequestEvent, window) } ,
    { SelectionNotify, "SelectionNotify", G_STRUCT_OFFSET (XSelectionEvent, window) } ,
    { ColormapNotify, "ColormapNotify", G_STRUCT_OFFSET (XColormapEvent, window) } ,
    { ClientMessage, "ClientMessage", G_STRUCT_OFFSET (XClientMessageEvent, window) } ,
    { MappingNotify, "MappingNotify", G_STRUCT_OFFSET (XMappingNotifyEvent, window) },
};
#endif

