/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "node.h"

#define CM_TYPE_EXPLOSION            (cm_explosion_get_type ())
#define CM_EXPLOSION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CM_TYPE_EXPLOSION, CmExplosion))
#define CM_EXPLOSION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CM_TYPE_EXPLOSION, CmExplosionClass))
#define CM_IS_EXPLOSION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CM_TYPE_EXPLOSION))
#define CM_IS_EXPLOSION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CM_TYPE_EXPLOSION))
#define CM_EXPLOSION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CM_TYPE_EXPLOSION, CmExplosionClass))

typedef struct _CmExplosion CmExplosion;
typedef struct _CmExplosionClass CmExplosionClass;

struct _CmExplosion
{
    CmNode parent_instance;
    
    WsRegion *region;
    GList *polygons;

    gdouble level;
};

struct _CmExplosionClass
{
    CmNodeClass parent_class;
};

GType        cm_explosion_get_type     (void);
CmExplosion *cm_explosion_new          (int          x,
					int          y,
					int          w,
					int          h);
void         cm_explosion_set_geometry (CmExplosion *explosion,
					int          x,
					int          y,
					int          w,
					int          h);
void         cm_explosion_set_region   (CmExplosion *explosion,
					WsRegion    *region);

void	     cm_explosion_set_level    (CmExplosion *explosion,
					double	     level);
