/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "ws.h"

int
main ()
{
    g_type_init ();
    
    WsDisplay *display = ws_display_new (NULL);
    WsScreen *screen = ws_display_get_default_screen (display);
    WsWindow *root = ws_screen_get_root_window (screen);
    WsWindow *overlay;

    ws_display_init_composite (display);
    
    ws_window_redirect_subwindows (root);

    overlay = ws_screen_get_gl_window (screen);
    
    g_print ("Hello world\n");
    return 0;
}
