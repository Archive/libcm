/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "ortho.h"

#include <GL/glu.h>

static void cm_ortho_render (CmNode *node, CmState *state);

G_DEFINE_TYPE (CmOrtho, cm_ortho, CM_TYPE_NODE);

static void
cm_ortho_finalize (GObject *object)
{
    CmOrtho *ortho = CM_ORTHO (object);
    
    G_OBJECT_CLASS (cm_ortho_parent_class)->finalize (object);

    cm_node_disown_child (CM_NODE (ortho), &ortho->child);
}

static void
cm_ortho_class_init (CmOrthoClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = CM_NODE_CLASS (class);
    
    object_class->finalize = cm_ortho_finalize;
    node_class->render = cm_ortho_render;
}

static void
cm_ortho_init (CmOrtho *ortho)
{
    
}

static void
cm_ortho_render (CmNode *node,
		 CmState *state)
{
    CmOrtho *ortho = CM_ORTHO (node);
    
    glPushMatrix ();

    gluOrtho2D (ortho->x, ortho->x + ortho->width,
		ortho->y + ortho->height, ortho->y);
    
    cm_node_render (ortho->child, state);

    glPopMatrix();
}

CmOrtho *
cm_ortho_new (CmNode *child,
	      int x, int y,
	      int width, int height)
{
    CmOrtho *ortho = g_object_new (CM_TYPE_ORTHO, NULL);

    ortho->x = x;
    ortho->y = y;
    ortho->width = width;
    ortho->height = height;

    cm_node_own_child (CM_NODE (ortho), &ortho->child, child);
    
    return ortho;
}

void
cm_ortho_set_axes (CmOrtho *ortho,
		   int x, int y,
		   int width, int height)
{
    g_return_if_fail (CM_IS_ORTHO (ortho));

    ortho->x = x;
    ortho->y = y;
    ortho->width = width;
    ortho->height = height;

    cm_node_queue_repaint (CM_NODE (ortho));
}

