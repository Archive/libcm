/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "node.h"

#define CM_TYPE_ORTHO            (cm_ortho_get_type ())
#define CM_ORTHO(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CM_TYPE_ORTHO, CmOrtho))
#define CM_ORTHO_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CM_TYPE_ORTHO, CmOrthoClass))
#define CM_IS_ORTHO(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CM_TYPE_ORTHO))
#define CM_IS_ORTHO_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CM_TYPE_ORTHO))
#define CM_ORTHO_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CM_TYPE_ORTHO, CmOrthoClass))

typedef struct _CmOrtho CmOrtho;
typedef struct _CmOrthoClass CmOrthoClass;

struct _CmOrtho
{
    CmNode	parent_instance;
    
    int		x;
    int		y;
    int		width;
    int		height;

    CmNode *	child;
};

struct _CmOrthoClass
{
    CmNodeClass parent_class;
};

GType cm_ortho_get_type (void);

CmOrtho *cm_ortho_new      (CmNode  *child,
			    int      x,
			    int      y,
			    int      width,
			    int      height);
void     cm_ortho_set_axes (CmOrtho *ortho,
			    int      x,
			    int      y,
			    int      width,
			    int      height);

