/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "clip.h"

#include <GL/gl.h>
#include <GL/glu.h>

#include <glib.h>

G_DEFINE_TYPE (CmClipNode, cm_clip_node, CM_TYPE_NODE);

static void clip_node_render (CmNode *node,
			      CmState *state);

static void
cm_clip_node_finalize (GObject *object)
{
    G_OBJECT_CLASS (cm_clip_node_parent_class)->finalize (object);

    cm_node_disown_child (CM_NODE (object),
			  &CM_CLIP_NODE (object)->child);
}

static void
cm_clip_node_class_init (CmClipNodeClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = (CmNodeClass *)class;
    
    object_class->finalize = cm_clip_node_finalize;

    node_class->render = clip_node_render;
}

static void
cm_clip_node_init (CmClipNode *clip_node)
{

}

static void
clip_node_render (CmNode *node,
		  CmState *state)
{
    CmClipNode *clip_node = (CmClipNode *)node;

    /* coordinate system */
    
#if 0
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glLineWidth (10.0);
    
    glBegin (GL_LINES);
    
    glColor4f (1.0, 0.0, 1.0, 1.0);
    
    glVertex2f (0.0, 0.0);
    glVertex2f (1.0, 0.0);
    
    glColor4f (0.0, 1.0, 0.0, 1.0);
    
    glVertex2f (0.0, 0.0);
    glVertex2f (0.0, 1.0);

    glEnd();
#endif
    
    /* We can do this more efficiently by adding a
     *
     *   node_get_texture (Node, x, y, w, h, *texture, *tx, *ty, *tw, *th)
     *
     * for items that return a texture we can then do clipping by just
     * setting texture coordinates.
     *
     * We could even have the possibility of returning multiple textures.
     */
    
    double eqn0[4] = {  1.0, 0.0, 0.0,  -clip_node->x };
    double eqn1[4] = {  -1.0, 0.0, 0.0,  (clip_node->x + clip_node->w) };
    double eqn2[4] = { 0.0,  1.0, 0.0, -clip_node->y };
    double eqn3[4] = { 0.0,  -1.0, 0.0, (clip_node->y + clip_node->h) };

    glClipPlane(GL_CLIP_PLANE0, eqn0);
    glClipPlane(GL_CLIP_PLANE1, eqn1);
    glClipPlane(GL_CLIP_PLANE2, eqn2);
    glClipPlane(GL_CLIP_PLANE3, eqn3); 

    glPushMatrix ();
    
    glEnable (GL_CLIP_PLANE0);
    glEnable (GL_CLIP_PLANE1);
    glEnable (GL_CLIP_PLANE2);
    glEnable (GL_CLIP_PLANE3);
    
    CM_NODE_GET_CLASS (clip_node->child)->render (clip_node->child, state);

    glPopMatrix();
    
    glDisable (GL_CLIP_PLANE0);
    glDisable (GL_CLIP_PLANE1);
    glDisable (GL_CLIP_PLANE2);
    glDisable (GL_CLIP_PLANE3);
}

CmClipNode *
cm_clip_node_new (double x, double y,
		  double width, double height,
		  CmNode *child)
{
    CmClipNode *clip_node;

    g_return_val_if_fail (CM_IS_NODE (child), NULL);
    
    clip_node = g_object_new (CM_TYPE_CLIP_NODE, NULL);

    clip_node->x = x;
    clip_node->y = y;
    clip_node->w = width;
    clip_node->h = height;

    cm_node_own_child (CM_NODE (clip_node), &clip_node->child, child);

    return clip_node;
}
