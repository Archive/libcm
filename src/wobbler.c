/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "wobbler.h"
#include "clip.h"

#include <GL/gl.h>
#include <GL/glu.h>

#include <glib.h>
#include "node.h"
#include "cube.h"
#include <math.h>

#define N_TILES 4

static void wobbler_node_render (CmNode  *node,
				 CmState *state);

G_DEFINE_TYPE (CmWobblerNode, cm_wobbler_node, CM_TYPE_NODE);

static void
cm_wobbler_node_finalize (GObject *object)
{
    G_OBJECT_CLASS (cm_wobbler_node_parent_class)->finalize (object);
}

static void
cm_wobbler_node_class_init (CmWobblerNodeClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = CM_NODE_CLASS (class);
    
    object_class->finalize = cm_wobbler_node_finalize;
    node_class->render = wobbler_node_render;
}

static void
cm_wobbler_node_init (CmWobblerNode *wobbler_node)
{

}

static void
wobbler_node_render (CmNode *node,
		     CmState *state)
{
    CmWobblerNode *wobbler_node = (CmWobblerNode *)node;
    GList *list;
    int i, j, k;
    double x, y;
    
    /* coordinate system */
    
    i = 0;
    j = 0;
    k = 0;
    
    for (list = wobbler_node->clips; list != NULL; list = list->next)
    {
	CmNode *child = list->data;
	
	x = (1.0 / N_TILES) * i + (1.0 / N_TILES) / 2;
	y = (1.0 / N_TILES) * j + (1.0 / N_TILES) / 2;
	
	glEnable (GL_TEXTURE_2D);
	
	glPushMatrix();
	
	if (i == 1 && j == 1)
	{
	    glTranslatef (x, y, 0);

	    glScalef (0.25, 0.25, 0.25);

	    glColor4f (1 * 0.5 * sin (cm_node_get_time (node)) + 1, 1 * 0.3, 1 * 0.3, 0.9);
	
#if 0
	    glDisable (GL_DEPTH_TEST);
	    glEnable (GL_BLEND);
	    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	
	    
	    glBegin (GL_QUADS);
	    
	    glVertex2f (0.0, 1.0);
	    glVertex2f (1.0, 1.0);
	    glVertex2f (1.0, 0.0);
	    glVertex2f (0.0, 0.0);
	    
	    glEnd ();
#endif

	    glTranslatef (-0.5, -0.5, 0);

	    glTranslatef (0.0, 0.0, 0.2 * sin (5 * cm_node_get_time (node)));
	    /* coordinate system now has origo in the corner */
	    
	    glTranslatef (0.5, 0.5, 0.5);
	    
	    glRotatef (180 * cm_node_get_time (node), 0.0, 0.0, 1.0);

	    glRotatef (60 * cm_node_get_time (node), 0.0, 1.0, 0.0);
	    glRotatef (90 * cm_node_get_time (node), 1.0, 0.0, 0.0);

	    glTranslatef (-0.5, -0.5, -0.5);
	    
#if 0
#endif
#if 0
	    glTranslatef (-0.5, -0.5, -0.5);
#endif
	    
#if 0
	    glTranslatef (-0.0, -1.0, 0);
#endif
	    
#if 0
	    glTranslatef (-1.5, -1.5, 0);
#endif
	    

#if 0
#endif
	    
	    cm_node_render (child, state);
#if 0
	    glRotatef (60 * cm_node_get_time (node), 0.0, 1.0, 0.0);
#endif
	
	    glTranslatef (-x, -y, 0.0);
	}
	else
	    cm_node_render (child, state);
	
	glPopMatrix();
	
	glColor4f (k * 0.3, k * 0.3, k * 0.3, 0.3);
	
	glDisable (GL_TEXTURE_2D);
	
	glDisable (GL_DEPTH_TEST);
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	
	
#if 0
	glBegin (GL_QUADS);
	
	glVertex2f (x - 0.1, y - 0.1);
	glVertex2f (x - 0.1, y + 0.1);
	glVertex2f (x + 0.1, y + 0.1);
	glVertex2f (x + 0.1, y - 0.1);
	
	glEnd ();
#endif
	
	glEnable (GL_DEPTH_TEST);
	
	j++;
	if (j == N_TILES)
	{
	    j = 0;
	    i++;
	}
	k++;
    }
    
#if 0
    glDisable (GL_DEPTH_TEST);
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	
    
    glBegin (GL_QUADS);
    
    glVertex2f (x - 0.1, y - 0.1);
    glVertex2f (x - 0.1, y + 0.1);
    glVertex2f (x + 0.1, y + 0.1);
    glVertex2f (x + 0.1, y - 0.1);
    
    glEnd ();
#endif
    
#if 0
    g_print ("asdf\n");
#endif
}

CmWobblerNode *
wobbler_node_new (CmNode *child)
{
    CmWobblerNode *wobbler = g_object_new (CM_TYPE_WOBBLER_NODE, NULL);
    int i;
    int j;
    
    wobbler->child = child;
    
    for (i = 0; i < N_TILES; ++i)
	for (j = 0; j < N_TILES; ++j)
	{
	    CmNode *node;

	    if (i == 1 && j == 1)
	    {
		CmClipNode *clip_node = cm_clip_node_new (
		    (1.0 / N_TILES) * i, (1.0 / N_TILES) * j,
		    (1.0 / N_TILES), (1.0 / N_TILES), child);
		CmCube *cube;

		cube = cm_cube_new ();

		clip_node = (CmClipNode *)child;
		
		cm_cube_set_face (cube, 0, child);
		cm_cube_set_face (cube, 1, child);
		cm_cube_set_face (cube, 2, child);
		cm_cube_set_face (cube, 3, child);
		cm_cube_set_face (cube, 4, child);
		cm_cube_set_face (cube, 5, child);

		node = (CmNode *)cube;
	    }
	    else
	    {
		node = (CmNode *)cm_clip_node_new (
		    (1.0 / N_TILES) * i, (1.0 / N_TILES) * j,
		    (1.0 / N_TILES), (1.0 / N_TILES), child);
	    }
	    
	    wobbler->clips = g_list_append (wobbler->clips, node);
	}
    
    return wobbler;
}
