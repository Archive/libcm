/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */

#ifndef _CM_DEFORM_H
#define _CM_DEFORM_H

#include "ws.h"
#include "node.h"

#define CM_TYPE_DEFORM            (cm_deform_get_type ())
#define CM_DEFORM(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CM_TYPE_DEFORM, CmDeform))
#define CM_DEFORM_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CM_TYPE_DEFORM, CmDeformClass))
#define CM_IS_DEFORM(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CM_TYPE_DEFORM))
#define CM_IS_DEFORM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CM_TYPE_DEFORM))
#define CM_DEFORM_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CM_TYPE_DEFORM, CmDeformClass))

typedef struct _CmDeform CmDeform;
typedef struct _CmDeformClass CmDeformClass;
typedef void (*CmDeformFunc) (int u, int v,
			      int x, int y, int width, int height,
			      int *deformed_x, int *deformed_y,
			      gpointer data);

typedef struct _CmPoint CmPoint;
struct _CmPoint
{
  double x, y;
};

struct _CmDeform
{
    CmNode parent_instance;
    
    int x;
    int y;
    int width;
    int height;
    
    CmDeformFunc	func;
    gpointer		data;
    
    CmNode	       *child;
    CmPoint		patch_points[4][4];

    WsRectangle	        rectangle;
};

struct _CmDeformClass
{
    CmNodeClass parent_class;
};

GType cm_deform_get_type (void);


CmDeform *cm_deform_new        (CmNode *child,
				int x,
				int y,
				int width,
				int height);

void	 cm_deform_set_patch (CmDeform *deform,
			      CmPoint   points[][4]);
void	  cm_deform_set_rectangle (CmDeform *deform,
				   WsRectangle *rect);
void	  cm_deform_set_deform (CmDeform     *deform,
				CmDeformFunc  func,
				gpointer      data);
void	  cm_deform_set_geometry (CmDeform   *deform,
				  int x, int y, int width, int height);

#endif
