/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "node.h"

#define CM_TYPE_TRANSLATION            (cm_translation_get_type ())
#define CM_TRANSLATION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CM_TYPE_TRANSLATION, CmTranslation))
#define CM_TRANSLATION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CM_TYPE_TRANSLATION, CmTranslationClass))
#define CM_IS_TRANSLATION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CM_TYPE_TRANSLATION))
#define CM_IS_TRANSLATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CM_TYPE_TRANSLATION))
#define CM_TRANSLATION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CM_TYPE_TRANSLATION, CmTranslationClass))

typedef struct _CmTranslation CmTranslation;
typedef struct _CmTranslationClass CmTranslationClass;

struct _CmTranslation
{
    CmNode	parent_instance;
    
    CmNode *	child;
    
    gdouble	x;
    gdouble	y;
    gdouble	z;
};

struct _CmTranslationClass
{
    CmNodeClass parent_class;
};

GType cm_translation_get_type (void);

CmTranslation * cm_translation_new (CmNode *child);
void	      cm_translation_set_translation (CmTranslation *translation,
					      gdouble	x,
					      gdouble	y,
					      gdouble      z);

