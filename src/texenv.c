/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */

#include "texenv.h"

G_DEFINE_TYPE (CmTexEnv, cm_tex_env, CM_TYPE_NODE);

static void cm_tex_env_render (CmNode *node,
			       CmState *state);

static void
cm_tex_env_finalize (GObject *object)
{
    G_OBJECT_CLASS (cm_tex_env_parent_class)->finalize (object);
}

static void
cm_tex_env_class_init (CmTexEnvClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = CM_NODE_CLASS (class);
    
    object_class->finalize = cm_tex_env_finalize;
    node_class->render = cm_tex_env_render;
}

static void
cm_tex_env_init (CmTexEnv *tex_env)
{
    
}

static void
cm_tex_env_render (CmNode *node,
		   CmState *state)
{
    CmTexEnv *env = CM_TEX_ENV (node);

    if (env->func == CM_REPLACE)
    {
	glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    }
    else
    {
	glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	if (env->has_modulate_color)
	{
	    glColor4f (env->modulate_color.red / (double)0xffff,
		       env->modulate_color.green / (double)0xffff,
		       env->modulate_color.blue / (double)0xffff,
		       env->modulate_color.alpha / (double)0xffff);
	}
    }
    
    cm_node_render (env->child, state);
}

CmTexEnv *
cm_tex_env_new (CmNode *child)
{
    CmTexEnv *env = g_object_new (CM_TYPE_TEX_ENV, NULL);

    cm_node_own_child (CM_NODE (env), &(env->child), child);

    env->func = CM_REPLACE;
    
    return env;
}

/* FIXME: Maybe there should be a mode where color comes from the actual fragment,
 * instead of being set explicitly here. How do we generally deal with colors?
 * 
 */
void
cm_tex_env_set_modulate (CmTexEnv *env,
			 WsColor *color)
{
    if (!color)
    {
	env->has_modulate_color = FALSE;
    }
    else
    {
	env->has_modulate_color = TRUE;

	env->modulate_color = *color;
    }

    env->func = CM_MODULATE;

    cm_node_queue_repaint (CM_NODE (env));
}

void
cm_tex_env_set_replace (CmTexEnv *env)
{
    env->func = CM_REPLACE;

    cm_node_queue_repaint (CM_NODE (env));
}
