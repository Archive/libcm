/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>
#include <math.h>
#include <string.h>

#include "ws.h"

#include "node.h"
#include "drawable-node.h"
#include "cube.h"
#include "clip.h"
#include "wobbler.h"
#include "stacker.h"
#include "rotation.h"
#include "translation.h"

#if 0
static void
draw_background (CmNode *node)
{
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f (0.2, 0.3, 0.8, g_random_double());
    
    glBegin (GL_QUADS);
    glVertex2f (0.0, 0.0);
    glVertex2f (0.0, 1.0);
    glVertex2f (1.0, 1.0);
    glVertex2f (1.0, 0.0);
    glEnd ();
}
#endif

#if 0
static CmNode *
create_background ()
{
    CmNode *node = g_new0 (CmNode, 1);
    
    node->render = draw_background;
    
    return node;
}
#endif

#if 0
static void
draw_coordinate_system (CmNode *node)
{
    int i;
    
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f (1.0, 1.0, 1.0, 1.0);
    
    glBegin (GL_LINES);
    
    glVertex2f (0.0, 0.0);
    glVertex2f (1.0, 0.0);
    
    glVertex2f (0.0, 0.0);
    glVertex2f (0.0, 1.0);
    
    glVertex2f (0.0, 0.0);
    glVertex3f (0.0, 0.0, 1.0);
    
    glColor4f (0.0, 1.0, 0.0, 0.8);
    
    for (i = -10; i < 10; ++i)
    {
	double x = i / 10.0;
	double z = i / 10.0;
	
	glVertex3f (x, 0.0, -1.0);
	glVertex3f (x, 0.0, 1.0);
	
	glVertex3f (-1.0, 0.0, z);
	glVertex3f (1.0, 0.0, z);
    }
    
    glEnd ();
}
#endif

#if 0
static CmNode *
create_coordinate_system ()
{
    CmNode *node = g_new0 (CmNode, 1);
    
    node->render = draw_coordinate_system;
    
    return node;
}
#endif

#if 0
static gdouble cube_time;
#endif

#if 0
static void
draw_cube (CmNode *node)
{
    double wt = 1.0;
    double ht = 1.0;
    
    glBegin (GL_QUADS);
    glColor3f (1.0, 0.0, 0.0);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (0.0f, 0.0f, 0.0f);
    glTexCoord2f (  wt, 0.0f); glVertex3f (1.0f, 0.0f, 0.0f);
    glTexCoord2f (  wt,   ht); glVertex3f (1.0f, 1.0f, 0.0f);
    glTexCoord2f (0.0f,   ht); glVertex3f (0.0f, 1.0f, 0.0f);
    
    glColor3f (0.0, 1.0, 0.0);
    glTexCoord2f (  wt, 0.0f); glVertex3f (0.0f, 0.0f, -1.0f);
    glTexCoord2f (  wt,   ht); glVertex3f (0.0f, 1.0f, -1.0f);
    glTexCoord2f (0.0f,   ht); glVertex3f (1.0f, 1.0f, -1.0f);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (1.0f, 0.0f, -1.0f);
    
    glColor3f (0.0, 0.0, 1.0);
    glTexCoord2f (0.0f,   ht); glVertex3f (0.0f, 1.0f, -1.0f);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (0.0f, 1.0f,  0.0f);
    glTexCoord2f (  wt, 0.0f); glVertex3f (1.0f, 1.0f,  0.0f);
    glTexCoord2f (  wt,   ht); glVertex3f (1.0f, 1.0f, -1.0f);
    
    glColor3f (1.0, 1.0, 0.0);
    glTexCoord2f (  wt,   ht); glVertex3f (0.0f, 0.0f, -1.0f);
    glTexCoord2f (0.0f,   ht); glVertex3f (1.0f, 0.0f, -1.0f);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (1.0f, 0.0f,  0.0f);
    glTexCoord2f (  wt, 0.0f); glVertex3f (0.0f, 0.0f,  0.0f);
    
    glColor3f (0.0, 1.0, 1.0);
    glTexCoord2f (  wt, 0.0f); glVertex3f (1.0f, 0.0f, -1.0f);
    glTexCoord2f (  wt,   ht); glVertex3f (1.0f, 1.0f, -1.0f);
    glTexCoord2f (0.0f,   ht); glVertex3f (1.0f, 1.0f,  0.0f);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (1.0f, 0.0f,  0.0f);
    
    glColor3f (1.0, 0.0, 1.0);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (0.0f, 0.0f, -1.0f);
    glTexCoord2f (  wt, 0.0f); glVertex3f (0.0f, 0.0f,  0.0f);
    glTexCoord2f (  wt,   ht); glVertex3f (0.0f, 1.0f,  0.0f);
    glTexCoord2f (0.0f,   ht); glVertex3f (0.0f, 1.0f, -1.0f);
    
    glEnd ();
}
#endif

static void
print_current (void)
{
#if 0
    double 
    glGetDoublev (
#endif
}

static GTimer *timer;
#if 0
static Node *coord;
static Node *cube;
#endif

typedef struct PaintInfo
{
    WsWindow *gl_window;
    GTimer *timer;
    CmNode *scene;
    CmNode *pixbufs;
    CmNode *nautilus;
    
    double start_nautilus;
    double start_pixbufs;
} PaintInfo;

#if 0
static GList *drawables;
#endif

static gboolean
do_paint (gpointer data)
{
    PaintInfo *info = data;
    
    static double xa = 0.0;
    static double ya = 0.0;
    static double za = 0.0;
    
    double f = 1.0;

    gdouble time = 3 * g_timer_elapsed (timer, NULL);
    
    xa = time * 90 * f;
    ya = time * 45 * f;
    za = time * 30 * f;
    
    ws_window_raise (info->gl_window);

    glClearColor (1.0, 0.5, 0.5, 1.0);
    glClear (GL_COLOR_BUFFER_BIT);
    glClear (GL_DEPTH_BUFFER_BIT);
    
    glEnable (GL_DEPTH_TEST);
    
    /* Set up default coordinate system */
    glMatrixMode (GL_MODELVIEW);

    glLoadIdentity ();
    gluOrtho2D (0.0, 1.0, 0.0, 1.0);
    glTranslatef (0.0, 0.0, 1.0);
#if 0
    glOrtho (0.0, 1.0, 1.0, 0.0, 0.0, 2.0);
#endif

    print_current();
    
#if 0
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glFrustum (0.0, 1.0, 1.0, 0.0, 1.0, 7);
#endif

    
    cm_node_render (info->scene, NULL);
    
    
#if 0
    /* draw cube a little further back */
    double f = 0.5;
    
    xa = time * 90 * f;
    ya = time * 45 * f;
    za = time * 30 * f;
    
    /* Move the visual coordinate system a little back */
    glTranslatef (0.0, 0.0, sin (time));
    
    glRotatef (10 * time, 0.0, 1.0, 0.0);
    glRotatef (2 * time, 0.0, 0.0, 1.0);
    
    glPushMatrix();
    
    glRotatef (xa, 1.0, 0.0, 0.0);
    glRotatef (ya, 0.0, 1.0, 0.0);
    glRotatef (za, 0.0, 0.0, 1.0);
    
    glEnable (GL_DEPTH_TEST);
    
    cube_time = 30 * time;
    cube->render(cube);
    
    glPopMatrix();
    
#if 0
    glDisable (GL_DEPTH_TEST);
#endif
    
    coord->render(coord);
#endif
    
    ws_window_gl_swap_buffers (info->gl_window);

    glFinish ();
    
    ws_display_flush (ws_drawable_get_display ((WsDrawable *)info->gl_window));
    
    return TRUE;
}

#if 0
static gboolean
do_quit (gpointer data)
{
    GMainLoop *loop = data;
    
    g_main_loop_quit (loop);
    
    return FALSE;
}
#endif

#if 0
static WsWindow *
get_desktop_window (WsWindow *root)
{
    WsWindow *result = NULL;
    GList *toplevels = ws_window_query_subwindows (root);
    GList *list;
    
    for (list = toplevels; list != NULL; list = list->next)
    {
	WsWindow *window = list->data;
	WsRectangle rect;
	
	ws_drawable_query_geometry ((WsDrawable *)window, &rect);
	
	if (!ws_window_query_input_only (window))
	{
	    if (ws_window_query_viewable (window))
	    {
		char *title = ws_window_query_title (window);
		
		if (title && strcmp (title, "Desktop") == 0)
		{
		    result = window;
		    break;
		}
	    }
	}
    }
    
    return result;
}
#endif

#if 0
static void
add_windows (WsWindow *root, CubeNode *cube_node)
{
    WsWindow *result = NULL;
    GList *list;
    CubeNode *cube2;
    GList *toplevels = ws_window_query_subwindows (root);
    
    face = 0;
    for (list = toplevels; list != NULL; list = list->next)
    {
	WsWindow *window = list->data;
	WsRectangle rect;
	
	ws_drawable_query_geometry (window, &rect);
	
	if (window != gl_window && !ws_window_query_input_only (window))
	{
	    if (ws_window_query_viewable (window))
	    {
		g_print ("title %s\n", ws_window_query_title (window));
		cube_set_face (cube_node, face++,
			       cm_drawable_node_new (window));
	    }
	    
	    if (face == 6)
		break;
#if 0
	    if (face == 2)
		break;
#endif
	}
    }
}
#endif

static gboolean
compositable (WsWindow *window)
{
    if (ws_window_query_viewable (window) &&
	!ws_window_query_input_only (window))
    {
	return TRUE;
    }
    
    return FALSE;
    
}

static gboolean
is_nautilus (WsWindow *window)
{
    char *title;
    
    title = ws_window_query_title (window);
    
    if (compositable (window) && title && strcmp (title, "Desktop") == 0)
	return TRUE;
    else
	return FALSE;
}

static gboolean
has_child_named (WsWindow *window,
		 char *child_title)
{
    GList *list;
    GList *children = ws_window_query_subwindows (window);
    
    for (list = children; list ; list = list->next)
    {
	char *title = ws_window_query_title (list->data);
	
	if (title && strcmp (child_title, title) == 0)
	    return TRUE;
    }
    
    return FALSE;
}

static WsWindow *
find_nautilus_window (WsWindow *root)
{
    GList *list;
    GList *toplevels = ws_window_query_subwindows (root);
    
    for (list = toplevels; list != NULL; list = list->next)
    {
	char *title;
	WsWindow *window = list->data;
	
	title = ws_window_query_title (window);
	if (is_nautilus (window))
	    return window;
    }
    
    return NULL;
}

static WsWindow *
find_pixbufs_window (WsWindow *root)
{
    GList *list;
    GList *toplevels = ws_window_query_subwindows (root);
    
    for (list = toplevels; list != NULL; list = list->next)
    {
	if (has_child_named (list->data, "lt-pixbuf-demo"))
	    return list->data;
    }
    
    return NULL;
}

static CmStacker *
create_windows (WsWindow *root, PaintInfo *info)
{
    CmStacker *stacker;
    GList *toplevels = ws_window_query_subwindows (root);
    GList *list;
#if 0
    int f;
#endif
    
    stacker = cm_stacker_new ();
    
    for (list = toplevels; list != NULL; list = list->next)
    {
	WsWindow *window = list->data;
	
	if (compositable (window) && window != info->gl_window &&
	    !is_nautilus (window) && !has_child_named (window, "lt-pixbuf-demo"))
	{
	    WsRectangle rect;
	    CmDrawableNode *drawable;

	    ws_drawable_query_geometry (WS_DRAWABLE (window), &rect);
	    
	    drawable = cm_drawable_node_new ((WsDrawable *)window, &rect);
	    
	    cm_stacker_add_child (stacker, (CmNode *)drawable);
	}
    }
    
    return stacker;
}

static void
setup_for_compositing (WsScreen *screen, PaintInfo *info)
{
    info->gl_window = ws_screen_get_gl_window (screen);
    
    /* redirect all toplevels */
    ws_window_redirect_subwindows (ws_screen_get_root_window (screen));
    
    /* map and unredirect the gl window */
    ws_window_set_override_redirect (info->gl_window, TRUE);
    ws_window_map (info->gl_window);
    ws_window_unredirect (info->gl_window);
    
    ws_display_sync (ws_drawable_get_display ((WsDrawable *)info->gl_window));
    
#if 0
    glMatrixMode (GL_MODELVIEW);
    
    glTranslatef (-1.0, -1.0, 0.0);
    
    glMatrixMode (GL_PROJECTION);
    gluPerspective( 45.0f, 1.0, 0.1f, 10.0f );
#endif
}

CmTranslation *pixbuf_node;

#if 0
static gboolean
do_transform (gpointer data)
{
    Rotation *trans = data;
    double time = 4 * cm_node_get_time (data);
    
    rotation_set_rotation (trans, (45 * sin (time) + 45),
			   1.0, 0.0, 0.0);
    
#if 0
    time /= 3.0;
#endif
    
    translation_set_translation ((Translation *)pixbuf_node, 0.0, 0.0, ( sin (time) ));
    
    return TRUE;
}
#endif

static gboolean
do_nautilus2 (gpointer data)
{
#define NAUTILUS2_TIME 0.225
    
    PaintInfo *info = data;
    double time = cm_node_get_time (info->nautilus) - info->start_nautilus;
    double angle = 0.0;
    
    if (time < NAUTILUS2_TIME)
	angle = 90 - (90 * (time / NAUTILUS2_TIME));
    else
	angle = 0.0;
    
    cm_rotation_set_rotation ((CmRotation *)info->nautilus, angle,
			      1.0, 0.0, 0.0);
    
    if (time > NAUTILUS2_TIME)
	return FALSE;
    else
	return TRUE;
}

static gboolean
start_nautilus2 (gpointer data)
{
    PaintInfo *info = data;
    
    info->start_nautilus = cm_node_get_time (info->pixbufs);
    
    g_idle_add (do_nautilus2, info);
    
    return FALSE;
    
}

static gboolean
do_pixbufs (gpointer data)
{
#define PIXBUF_TIME 0.2
    
    PaintInfo *info = data;
    double time = cm_node_get_time (info->pixbufs) - info->start_pixbufs;
    
    cm_translation_set_translation ((CmTranslation *)info->pixbufs,
				    0.0, 0.0, 3.5 * (time / PIXBUF_TIME));
    
    if (time > PIXBUF_TIME)
    {
	g_timeout_add (1000, start_nautilus2, info);
	
	return FALSE;
    }
    else
	return TRUE;
}

static gboolean
start_pixbufs (gpointer data)
{
    PaintInfo *info = data;
    
    if (info->pixbufs)
    {
	info->start_pixbufs = cm_node_get_time (info->pixbufs);
	
	g_idle_add (do_pixbufs, info);
    }
    
    return FALSE;
    
}

static gboolean
do_nautilus (gpointer data)
{
#define NAUTILUS_TIME 0.225
    
    PaintInfo *info = data;
    double time = cm_node_get_time (info->nautilus) - info->start_nautilus;
    
#if 0
    g_print ("time %f\n", time);
#endif

#if 0
    g_print ("setting rotation\n");
#endif
    
    cm_rotation_set_rotation ((CmRotation *)info->nautilus, (90 * (time / NAUTILUS_TIME)),
			      1.0, 0.0, 0.0);
    
#if 0
    g_print ("nautil\n");
#endif
    
    if (time > NAUTILUS_TIME)
    {
	g_timeout_add (1000, start_pixbufs, info);
	
	return FALSE;
    }
    else
	return TRUE;
    
}

static gboolean
do_animation (gpointer data)
{
    PaintInfo *info = data;

    if (!info->nautilus)
	return FALSE;
    
    info->start_nautilus = cm_node_get_time (info->nautilus);
    
    g_idle_add (do_nautilus, info);
    
    return FALSE;
}

static gboolean option_sync;
static GOptionEntry demo_options[] = {
    { "sync", 0, 0, G_OPTION_ARG_NONE, &option_sync, "Make X calls synchronous", NULL },
    { NULL }
};

int
main (int argc, char *argv[])
{
    GOptionContext *ctx;

    ctx = g_option_context_new("libcm options");
    g_option_context_add_main_entries(ctx, demo_options, "demo");
    g_option_context_parse(ctx, &argc, &argv, NULL);
    g_option_context_free(ctx);

    g_type_init ();
    
    GMainLoop *loop = g_main_loop_new (NULL, 0);
    WsDisplay *display = ws_display_new (NULL);
    WsScreen *screen = ws_display_get_default_screen (display);
    g_assert (screen);
    
    WsWindow *root = ws_screen_get_root_window (screen);

    PaintInfo info;
    CmStacker *stacker1;
    CmStacker *stacker2;
    
#if 0
    CmNode *desktop;
    CmNode *cube;
    CmNode *clipper;
#endif
    WsWindow *nautilus;
    CmDrawableNode *nautilus_node;
    CmRotation *rotation;
    WsWindow *pixbufs;

    ws_display_set_synchronize (display, option_sync);
    
    ws_display_init_composite (display);
    
    setup_for_compositing (screen, &info);
    
    stacker2 = create_windows (root, &info);
    
    stacker1 = cm_stacker_new ();
    
    nautilus = find_nautilus_window (root);
    if (nautilus)
    {
	WsRectangle rect;

	ws_drawable_query_geometry (WS_DRAWABLE (nautilus), &rect);
	
	nautilus_node = cm_drawable_node_new (WS_DRAWABLE (nautilus), &rect);
    }
    
    pixbufs = find_pixbufs_window (root);
    if (pixbufs)
    {
#if 0
	CubeNode *cube = cube_node_new ();
#endif
	CmDrawableNode *drawable;
	WsRectangle rect;
	
	ws_drawable_query_geometry ((WsDrawable *) pixbufs, &rect);
	drawable = cm_drawable_node_new ((WsDrawable *)pixbufs, &rect);
	
#if 0
	cube_set_face (cube, 0, pixbuf_node);
	cube_set_face (cube, 1, pixbuf_node);
	cube_set_face (cube, 2, pixbuf_node);
	cube_set_face (cube, 3, pixbuf_node);
	cube_set_face (cube, 4, pixbuf_node);
	cube_set_face (cube, 5, pixbuf_node);
#endif
	
	pixbuf_node = cm_translation_new ((CmNode *)drawable);
    }
    else
    {
	g_print ("no\n");
	pixbuf_node = NULL;
    }
    
    info.pixbufs = (CmNode *)pixbuf_node;
#if 0
    rotation = cm_rotation_new ((CmNode *)nautilus_node);
#endif
    
    cm_stacker_add_child (stacker1, CM_NODE (stacker2));
    if (pixbuf_node)
	cm_stacker_add_child (stacker1, CM_NODE (pixbuf_node));
#if 0
    cm_stacker_add_child (stacker1, CM_NODE (rotation));
#endif
    
    info.scene = (CmNode *)stacker1;
    
    info.nautilus = (CmNode *)rotation;
    
#if 0
    desktop = create_desktop (root, &info);
    
    info.scene = cube_node_new ();
    
    clipper = wobbler_node_new (desktop);
    
    cube_set_face (info.scene, 0, clipper);
    cube_set_face (info.scene, 1, clipper);
    cube_set_face (info.scene, 2, clipper);
    cube_set_face (info.scene, 3, clipper);
    cube_set_face (info.scene, 4, clipper);
    cube_set_face (info.scene, 5, clipper);
    
    info.scene = clipper;
#endif
    
#if 0
    g_timeout_add (4000, do_animation, &info);
#endif
    
    g_idle_add (do_paint, &info);
    
    timer = g_timer_new ();
    
    g_main_loop_run (loop);
    
    return 0;
}
