/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "square.h"
#include "node.h"
#include "state.h"

static void cm_square_render (CmNode *node, CmState *state);

G_DEFINE_TYPE (CmSquare, cm_square, CM_TYPE_NODE);

static void
cm_square_finalize (GObject *object)
{
    G_OBJECT_CLASS (cm_square_parent_class)->finalize (object);
}

static void
cm_square_class_init (CmSquareClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = CM_NODE_CLASS (class);
    object_class->finalize = cm_square_finalize;
    node_class->render = cm_square_render;
}

static void
cm_square_render (CmNode *node,
		  CmState *state)
{
    CmSquare *square = CM_SQUARE (node);
    WsRegion *region = cm_state_get_visible_region (state);
    WsRectangle *rects;
    int n_rects, i;

    ws_region_get_rectangles (region, &rects, &n_rects);

    glPushMatrix ();

    cm_state_set_screen_coords (state);
    
    glScalef (square->scale, square->scale, 1.0);

    glColor4f (square->r, square->g, square->b, square->a);

    glDisable (GL_TEXTURE_2D);
    glDisable (GL_TEXTURE_RECTANGLE_ARB);

    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
    
#if 0
    g_print ("printing %d rectangles (%f %f %f %f)\n", n_rects,
	     square->r, square->g, square->b, square->a);
#endif
    
    for (i = 0; i < n_rects; ++i)
    {
	WsRectangle *rect = &(rects[i]);

	glBegin (GL_QUADS);
	glVertex2f (rect->x, rect->y);
	glVertex2f (rect->x, rect->y + rect->height);
	glVertex2f (rect->x + rect->width, rect->y + rect->height);
	glVertex2f (rect->x + rect->width, rect->y);
	glEnd ();

	glFinish ();
    }

    if (rects)
	g_free (rects);

    ws_region_destroy (region);
    
    glPopMatrix();
}

static void
cm_square_init (CmSquare *square)
{
    
}

CmSquare *
cm_square_new (gdouble r,
	       gdouble g,
	       gdouble b,
	       gdouble a)
{
    CmSquare *square = g_object_new (CM_TYPE_SQUARE, NULL);

    square->r = r;
    square->g = g;
    square->b = b;
    square->a = a;
    
    square->scale = 1.0;

    return square;
}

void
cm_square_set_scale (CmSquare *square,
		     gdouble scale)
{
    square->scale = scale;

    cm_node_queue_repaint (CM_NODE (square));
}
