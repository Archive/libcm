/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "ws.h"
#include "wsint.h"
#include "grid.h"
#include "cube.h"
#include "stacker.h"
#include "square.h"
#include "deform.h"
#include <GL/glu.h>
#include <math.h>

static CmNode *
build_stacker (gdouble r, gdouble g, gdouble b)
{
#define N_NESTED 10
    CmStacker *stacker = cm_stacker_new ();
    CmSquare *s;
    int i;

    for (i = 0; i < N_NESTED; ++i)
    {
	gdouble blend = 1 - (i / (double)N_NESTED);
	
	s = cm_square_new (blend * r + (1 - blend),
			   blend * g + (1 - blend),
			   blend * b + (1 - blend),
			   0.3);

	cm_square_set_scale (s, blend);
	
	cm_stacker_add_child (stacker, CM_NODE (s));
    }

    return CM_NODE (stacker);
}

int
main ()
{
    g_type_init ();
    
    WsDisplay *display = ws_display_new (NULL);
    WsScreen *screen = ws_display_get_default_screen (display);
    WsWindow *gl_window = ws_screen_get_gl_window (screen);
    GMainLoop *loop = g_main_loop_new (NULL, TRUE);
    GTimer *timer = g_timer_new ();
    CmCube *cube;
    double time;
	
    ws_display_set_synchronize (display, TRUE);
    
    ws_window_map (gl_window);

    ws_display_sync (display);
    
    cube = cm_cube_new ();
    
    cm_cube_set_face (cube, 0, build_stacker (1.0, 0.0, 0.0));	
    cm_cube_set_face (cube, 1, build_stacker (0.0, 1.0, 0.0));
    cm_cube_set_face (cube, 2, build_stacker (0.0, 0.0, 1.0));
    cm_cube_set_face (cube, 3, build_stacker (1.0, 0.0, 1.0));	
    cm_cube_set_face (cube, 4, build_stacker (0.0, 1.0, 1.0));
    cm_cube_set_face (cube, 5, build_stacker (1.0, 1.0, 0.0));	

#if 0
    glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
#endif
#if 0
    glEnable (GL_POLYGON_SMOOTH);
    glEnable (GL_LINE_SMOOTH);
#endif
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint (GL_POLYGON_SMOOTH_HINT, GL_NICEST);

    glMatrixMode (GL_PROJECTION);
    gluPerspective (60.0, 4/3.0, 1.0, 9.0);
    glMatrixMode (GL_MODELVIEW);
    glTranslatef (0.0, 0.0, -6.0);

    glEnable (GL_DEPTH_TEST);
    
    for (;;)
    {
	time = g_timer_elapsed (timer, NULL);
	glClearColor (1.0, 1.0, 1.0, 1.0);
	
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glPushMatrix ();
	glRotatef (10 * time, 0.0, 0.0, 1.0);
	
	glPushMatrix ();
	glTranslatef (0.0, 0.0, -2.0);
	glScalef (3.0, 3.0, 1.0);
	cm_node_render (build_stacker (0.5, 0.5, 0.5), NULL);
	glPopMatrix ();
	
	glRotatef (- 20 * time, 0.0, 0.0, 1.0);
	
	glPushMatrix ();
	glRotatef (time * 180, -1.0, 1.0, 1.0);
	
	cm_node_render (CM_NODE (cube), NULL);
	glPopMatrix ();

	ws_window_gl_swap_buffers (gl_window);
	ws_window_gl_finish (gl_window);
    }    
    g_main_loop_run (loop);
    return 0;
}
