/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include <string.h>

#include "wsint.h"
#include <glib-object.h>

enum
{
    PROP_0,
    PROP_DISPLAY,
    PROP_XID,
    PROP_FOREIGN
};

G_DEFINE_TYPE (WsResource, ws_resource, G_TYPE_OBJECT);

static void
ws_resource_finalize (GObject *object)
{
    WsResource *resource = WS_RESOURCE (object);
    
    _ws_display_remove_resource (resource->display, resource->xid);
    
    G_OBJECT_CLASS (ws_resource_parent_class)->finalize (object);
}

static void
ws_resource_set_property (GObject         *object,
			  guint            prop_id,
			  const GValue    *value,
			  GParamSpec      *pspec)
{
    WsResource *resource = WS_RESOURCE (object);
    
    switch (prop_id)
    {
    case PROP_DISPLAY:
	resource->display = g_value_get_object (value);
	break;
    case PROP_XID:
	resource->xid = g_value_get_ulong (value);
	break;
    case PROP_FOREIGN:
	resource->foreign = g_value_get_boolean (value);
	break;
    default:
	break;
    }
}

static void
ws_resource_get_property (GObject         *object,
			  guint            prop_id,
			  GValue          *value,
			  GParamSpec      *pspec)
{
    WsResource *resource = WS_RESOURCE (object);
    
    switch (prop_id)
    {
    case PROP_DISPLAY:
	g_value_set_object (value, resource->display);
	break;
    case PROP_XID:
	g_value_set_ulong (value, resource->xid);
	break;
    case PROP_FOREIGN:
	g_value_set_boolean (value, resource->foreign);
	break;
    default:
	break;
    }
}

static GObject *
ws_resource_constructor (GType		        type,
			 guint		        n_construct_properties,
			 GObjectConstructParam *construct_params)
{
    GObject *object;
    WsResource *resource;
    gboolean found_foreign_property;
    int i;
    
    object = G_OBJECT_CLASS (ws_resource_parent_class)->constructor (
	type, n_construct_properties, construct_params);
    
    resource = WS_RESOURCE (object);

    /* Check that the "foreign" property has been set */
    found_foreign_property = FALSE;
    for (i = 0; i < n_construct_properties; ++i)
    {
	GObjectConstructParam *c_p = &(construct_params[i]);
	
	if (strcmp (c_p->pspec->name, "foreign") == 0)
	    found_foreign_property = TRUE;
    }
    
    if (resource->xid == None || resource->display == NULL || !found_foreign_property)
    {
	g_error ("Display, XID and 'foreign' must be specified when creating "
		 "a resource");
    }
    
    _ws_display_add_resource (resource->display, resource->xid, resource);
    
    return object;
}

static void
ws_resource_class_init (WsResourceClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    
    object_class->finalize = ws_resource_finalize;
    object_class->constructor = ws_resource_constructor;
    object_class->get_property = ws_resource_get_property;
    object_class->set_property = ws_resource_set_property;
    
    g_object_class_install_property (
	object_class, PROP_DISPLAY,
	g_param_spec_object ("display",
			     "Display",
			     "Display of the resource",
			     WS_TYPE_DISPLAY,
			     G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
    
    g_object_class_install_property (
	object_class, PROP_XID,
	g_param_spec_ulong ("xid",
			    "XID",
			    "XID of resource",
			    0, G_MAXLONG, None,
			    G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));

    g_object_class_install_property (
	object_class, PROP_FOREIGN,
	g_param_spec_boolean ("foreign",
			      "Foreign",
			      "Whether the resource is owned someone else",
			      FALSE,
			      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
}

static void
ws_resource_init (WsResource *resource)
{
    
}
