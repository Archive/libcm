/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "node.h"

#define CM_TYPE_SHADOW            (cm_shadow_get_type ())
#define CM_SHADOW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CM_TYPE_SHADOW, CmShadow))
#define CM_SHADOW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CM_TYPE_SHADOW, CmShadowClass))
#define CM_IS_SHADOW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CM_TYPE_SHADOW))
#define CM_IS_SHADOW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CM_TYPE_SHADOW))
#define CM_SHADOW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CM_TYPE_SHADOW, CmShadowClass))

typedef struct _CmShadow CmShadow;
typedef struct _CmShadowClass CmShadowClass;

struct _CmShadow
{
    CmNode parent_instance;

    int		x;
    int		y;
    int		width;
    int		height;
};

struct _CmShadowClass
{
    CmNodeClass parent_class;
};

GType cm_shadow_get_type (void);

CmShadow *cm_shadow_new (int x, int y,
			 int width, int height);
