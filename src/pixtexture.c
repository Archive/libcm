/* libcm - A library with a xlib wrapper and a gl based scene graph
 * Copyright (C) 2005, 2006  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * Author: Soren Sandmann (sandmann@redhat.com)
 */
#include "node.h"
#include "state.h"
#include "pixtexture.h"
#include "wsint.h"
#include <math.h>

static void cm_pix_texture_render (CmNode *node,
				   CmState *state);

G_DEFINE_TYPE (CmPixTexture, cm_pix_texture, CM_TYPE_NODE);

static void
cm_pix_texture_finalize (GObject *object)
{
    CmPixTexture *pix_texture = CM_PIX_TEXTURE (object);

    cm_pix_texture_set_pixmap (pix_texture, NULL);
    
    cm_node_disown_child (CM_NODE (pix_texture), &pix_texture->child);
	
    G_OBJECT_CLASS (cm_pix_texture_parent_class)->finalize (object);
}

static void
pix_texture_vertex (gdouble *x,
		    gdouble *y,
		    gdouble *z,
		    gdouble  u,
		    gdouble  v,
		    gpointer data)
{
    CmPixTexture *node = data;

    u = round (u * (node->geometry.width));
    v = round (v * (node->geometry.height));

#if 0
    g_print ("coords: %d %d\n", (int)u, (int)v);
#endif
    
    glTexCoord2i ((int)u, (int)v);
}

static void
cm_pix_texture_render (CmNode *node,
		       CmState *state)
{
    CmPixTexture *pix_texture = CM_PIX_TEXTURE (node);
    GLuint texture;

    if (!pix_texture->pixmap)
	return;

    glPushMatrix ();
    
#if 0
    cm_state_set_screen_coords (state);
#endif
    
    texture = ws_pixmap_get_texture (pix_texture->pixmap);
    
    glBindTexture (GL_TEXTURE_RECTANGLE_ARB, texture);
    glEnable (GL_TEXTURE_RECTANGLE_ARB);

    cm_state_push_vertex_func (state, pix_texture_vertex, pix_texture);

    if (cm_pix_texture_has_alpha (node))
    {
	glBlendFunc (GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	glEnable (GL_BLEND);
    }
    
#if 0
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glColor4f (1.0, 1.0, 0.98, 1.0);
#endif

    cm_node_render (pix_texture->child, state);

    cm_state_pop_vertex_func (state);

    glBindTexture (GL_TEXTURE_RECTANGLE_ARB, 0);

    glPopMatrix ();
}

static void
cm_pix_texture_class_init (CmPixTextureClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS (class);
    CmNodeClass *node_class = CM_NODE_CLASS (class);
    
    object_class->finalize = cm_pix_texture_finalize;
    node_class->render = cm_pix_texture_render;
}

static void
cm_pix_texture_init (CmPixTexture *pix_texture)
{
    pix_texture->updates = TRUE;
}

void
cm_pix_texture_set_pixmap (CmPixTexture *pix_texture,
			   WsPixmap *pixmap)
{
    if (pix_texture->pixmap)
	g_object_unref (pix_texture->pixmap);
    
    if (pixmap)
    {
	WsDisplay *display = WS_RESOURCE (pixmap)->display;

	g_object_ref (pixmap);

	ws_display_begin_error_trap (display);
	
	ws_drawable_query_geometry (pixmap,
				    &pix_texture->geometry);
	
	ws_display_end_error_trap (display);
    }

    pix_texture->pixmap = pixmap;

    cm_node_queue_repaint (CM_NODE (pix_texture));
}

CmPixTexture *
cm_pix_texture_new (WsPixmap *pixmap,
		    CmNode   *child)
{
    CmPixTexture *pix_texture = g_object_new (CM_TYPE_PIX_TEXTURE, NULL);

    cm_pix_texture_set_pixmap (pix_texture, pixmap);

    cm_node_own_child (CM_NODE (pix_texture), &pix_texture->child, child);
    
    return pix_texture;
}

void
cm_pix_texture_set_child (CmPixTexture *pix_texture,
			  CmNode       *child)
{
    if (pix_texture->child == child)
	return;

    cm_node_disown_child (CM_NODE (pix_texture), &pix_texture->child);
    cm_node_own_child (CM_NODE (pix_texture), &pix_texture->child, child);
}

void
cm_pix_texture_set_updates (CmPixTexture *pix_texture,
			    gboolean	  updates)
{
    pix_texture->updates = !!updates;

    if (pix_texture->pixmap)
	ws_pixmap_set_updates (pix_texture->pixmap, pix_texture->updates);
}

gboolean
cm_pix_textures_get_updates (CmPixTexture *pix_texture)
{
    return pix_texture->updates;
}

gboolean
cm_pix_texture_has_alpha (CmPixTexture *pix_texture)
{
    return (pix_texture->pixmap) &&
	ws_format_has_alpha (ws_drawable_get_format (WS_DRAWABLE (pix_texture->pixmap)));
}

